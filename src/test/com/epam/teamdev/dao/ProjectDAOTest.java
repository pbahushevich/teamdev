package test.com.epam.teamdev.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.Role;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.CustomerDAO;
import com.epam.teamdev.dao.intf.DeveloperDAO;
import com.epam.teamdev.dao.intf.ManagerDAO;
import com.epam.teamdev.dao.intf.ProjectDAO;
import com.epam.teamdev.dao.intf.QualificationDAO;
import com.epam.teamdev.dao.intf.TechTaskDAO;
import com.epam.teamdev.dao.intf.WorkTypeDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class ProjectDAOTest {

	private static Project project;
	private static Developer dev1;
	private static Developer dev2;
	private static Developer dev3;
	private static Customer customer;
	private static Manager manager;
	private static Qualification qualification;
	private static WorkType workType;
	private static TechTask task;

	// private static final
	@BeforeClass
	public static void initTest() {

		ConnectionPool.initPool();

		customer = new Customer();
		customer.setName("Vasilij Ahmetzianov");
		customer.setCompany("Roga i Kopyta");
		customer.setEmail("vasa@mail.ru");
		customer.setLogin("VAhmetzianov");
		customer.setPassword("23212");
		customer.setRole(Role.CUSTOMER);
		customer.setTelephone("2313123");

		manager = new Manager();
		manager.setName("Das manager");
		manager.setDepartment("Marketing office");
		manager.setEmail("manaeger@mail.ru");
		manager.setLogin("dManager");
		manager.setPassword("1234");
		manager.setRole(Role.MANAGER);
		manager.setTelephone("2313123");

		qualification = new Qualification();
		qualification.setTitle("Developer");
		workType = new WorkType();
		workType.setTitle("Soft development");

		dev1 = new Developer();
		dev1.setName("Das developer1");
		dev1.setQualification(qualification);
		dev1.setEmail("developer1@mail.ru");
		dev1.setLogin("developer1");
		dev1.setPassword("1234");
		dev1.setRole(Role.DEVELOPER);
		dev1.setTelephone("2313123");

		dev2 = new Developer();
		dev2.setName("Das developer2");
		dev2.setQualification(qualification);
		dev2.setEmail("developer2@mail.ru");
		dev2.setLogin("developer2");
		dev2.setPassword("1234");
		dev2.setRole(Role.DEVELOPER);
		dev2.setTelephone("2313123");

		dev3 = new Developer();
		dev3.setName("Das developer3");
		dev3.setQualification(qualification);
		dev3.setEmail("developer3@mail.ru");
		dev3.setLogin("developer3");
		dev3.setPassword("1234");
		dev3.setRole(Role.DEVELOPER);
		dev3.setTelephone("2313123");
		task = new TechTask();
		task.setCustomer(customer);
		task.setTitle("Test task");
		task.setDate(new Date(2016,Calendar.JANUARY,01));

		List<TaskWork> taskWorks = new ArrayList<>();
		TaskWork work1 = task.new TaskWork();
		work1.setDevCount(3);
		work1.setQualification(qualification);
		work1.setWorkType(workType);
		taskWorks.add(work1);
		task.setWorkList(taskWorks);

		insertInitDataInDatabase();
		createNewProject();
		insertProjectIntoDatabase();

	}

	private static void insertInitDataInDatabase() {

		CustomerDAO cDao = JDBCDAOFactory.getInstance().getCustomerDAO();
		customer.setId(cDao.insert(customer));
		ManagerDAO mDao = JDBCDAOFactory.getInstance().getManagerDAO();
		manager.setId(mDao.insert(manager));
		QualificationDAO qDao = JDBCDAOFactory.getInstance().getQualificationDAO();
		qualification.setId(qDao.insert(qualification));
		WorkTypeDAO wDao = JDBCDAOFactory.getInstance().getWorkTypeDAO();
		workType.setId(wDao.insert(workType));
		
		DeveloperDAO dDao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		dev1.setId(dDao.insert(dev1));
		dev2.setId(dDao.insert(dev2));
		dev3.setId(dDao.insert(dev3));
		TechTaskDAO tDao = JDBCDAOFactory.getInstance().getTechTaskDAO();
		task.setId(tDao.insert(task));

	}

	private static void insertProjectIntoDatabase() {
		ProjectDAO pDao = JDBCDAOFactory.getInstance().getProjectDAO();
		project.setId(pDao.insert(project));
		assertNotEquals(0, project.getId());
	}

	@AfterClass
	public static void destroyTest() {
		if (project != null) {
			ProjectDAO pDao = JDBCDAOFactory.getInstance().getProjectDAO();
			pDao.deleteById(project.getId());
		}

		ManagerDAO mDao = JDBCDAOFactory.getInstance().getManagerDAO();
		mDao.deleteById(manager.getId());

		DeveloperDAO dDao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		dDao.deleteById(dev1.getId());
		dDao.deleteById(dev2.getId());
		dDao.deleteById(dev3.getId());

		TechTaskDAO tDao = JDBCDAOFactory.getInstance().getTechTaskDAO();
		tDao.deleteById(task.getId());

		CustomerDAO cDao = JDBCDAOFactory.getInstance().getCustomerDAO();
		cDao.deleteById(customer.getId());

		QualificationDAO qDao = JDBCDAOFactory.getInstance().getQualificationDAO();
		qDao.deleteById(qualification.getId());

		WorkTypeDAO wDao = JDBCDAOFactory.getInstance().getWorkTypeDAO();
		wDao.deleteById(workType.getId());

	}

	@Test
	public void readTest() {
		Project projectInDB = new Project();
		ProjectDAO pDao = JDBCDAOFactory.getInstance().getProjectDAO();

		projectInDB = pDao.getHeaderById(project.getId());
		assertNotEquals(null, projectInDB);
		assertEquals(project.getDate(), projectInDB.getDate());
		assertEquals(project.getDateEnd(), projectInDB.getDateEnd());
		assertEquals(project.getDateStart(), projectInDB.getDateStart());
		assertEquals(project.getTitle(), projectInDB.getTitle());
		Customer cus1 = project.getTechTask().getCustomer();
		Customer cus2 = projectInDB.getTechTask().getCustomer();
		
		assertEquals(cus1.getCompany(), cus2.getCompany());
		assertEquals(cus1.getEmail(), cus2.getEmail());
		assertEquals(cus1.getLogin(), cus2.getLogin());
		assertEquals(cus1.getName(), cus2.getName());
		assertEquals(cus1.getTelephone(), cus2.getTelephone());
		assertEquals(cus1.getUserId(), cus2.getUserId());
		
		assertEquals(project.getTechTask(), projectInDB.getTechTask());
		
		assertEquals(project.getManager(), projectInDB.getManager());
		projectInDB.setWorkList(pDao.getWorkList(projectInDB));
		assertEquals(project, projectInDB);
	}

	@Test
	public void updateTest() {

		project.setTitle("New test project title");
		ProjectDAO pDao = JDBCDAOFactory.getInstance().getProjectDAO();
		pDao.update(project);

		Project projectInDB = new Project();
		projectInDB = pDao.getHeaderById(project.getId());
		assertEquals(project.getTitle(), projectInDB.getTitle());

	}

	@Test
	public void deleteTest() {
		ProjectDAO pDao = JDBCDAOFactory.getInstance().getProjectDAO();
		pDao.deleteById(project.getId());
		assertEquals(null, pDao.getHeaderById(project.getId()));
		insertProjectIntoDatabase();
	}

	private static void createNewProject() {
		project = new Project();
		project.setDate(new Date(2016,Calendar.JANUARY,01));
		project.setDateEnd(new Date(2016,Calendar.JANUARY,01));
		project.setDateStart(new Date(2016,Calendar.JANUARY,01));
		project.setManager(manager);
		project.setTechTask(task);
		project.setTitle("Test project");

		TechTaskDAO tDao = JDBCDAOFactory.getInstance().getTechTaskDAO();
		List<ProjectWork> projectWorks = new ArrayList<>();
		List<TaskWork> createdTaskWorks = tDao.getWorkList(task);
		for (TaskWork taskWork : createdTaskWorks) {
			ProjectWork pWork1 = project.new ProjectWork();
			pWork1.setDateEnd(new Date());
			pWork1.setDateStart(new Date());
			pWork1.setDeveloper(dev1);
			pWork1.setTaskWork(taskWork);

			projectWorks.add(pWork1);

			ProjectWork pWork2 = project.new ProjectWork();
			pWork2.setDateEnd(new Date(2016,Calendar.JANUARY,01));
			pWork2.setDateStart(new Date(2016,Calendar.JANUARY,01));
			pWork2.setDeveloper(dev2);
			pWork2.setTaskWork(taskWork);

			projectWorks.add(pWork2);

			ProjectWork pWork3 = project.new ProjectWork();
			pWork3.setDateEnd(new Date());
			pWork3.setDateStart(new Date());
			pWork3.setDeveloper(dev3);
			pWork3.setTaskWork(taskWork);

			projectWorks.add(pWork1);
		}
		project.setWorkList(projectWorks);

	}

}
