package com.epam.teamdev.dao.pool;
import java.util.ResourceBundle;

/**
 * Class that loads connection parameters from file, and helps to operate with them for connection pool.
 *
 */
public class DBResourceManager {
	private static final String DBCONFIG_ADRRESS = "com.epam.teamdev.configuration.dbconfig.db";
	private final static DBResourceManager instance = new DBResourceManager();
	
	private ResourceBundle bundle = ResourceBundle.getBundle(DBCONFIG_ADRRESS);

	public static DBResourceManager getInstance() {
		return instance;
	}

	public String getValue(String key) {
		return bundle.getString(key);
	}
}