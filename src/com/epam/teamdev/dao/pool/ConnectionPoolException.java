package com.epam.teamdev.dao.pool;

/**
 * Connection thrown by connection pool methods in case of SQL, or database connection errors.
 * @author Uzbyakov
 *
 */
public class ConnectionPoolException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ConnectionPoolException(String message, Exception e) {
		super(message, e);
	}

	public ConnectionPoolException(String message) {
		super(message);
	}
	
}