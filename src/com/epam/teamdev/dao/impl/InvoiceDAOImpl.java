package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.InvoiceDAO;
import com.epam.teamdev.dao.intf.ProjectDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class InvoiceDAOImpl implements InvoiceDAO {

	private static final String PAID_COLUMN = "paid";

	private static final InvoiceDAO instance = new InvoiceDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_ALL_SQL = "SELECT * FROM invoice ORDER BY date";
	private final static String GET_BY_ID_SQL = "SELECT * FROM invoice WHERE id = ? ";
	private final static String GET_BY_PROJECT_ID_SQL = "SELECT * FROM invoice WHERE project_id = ? ";
	private final static String GET_BY_CUSTOMER_ID_SQL = "SELECT * FROM invoice WHERE paid = false AND project_id IN "
			+ " (SELECT id FROM project WHERE techtask_id IN " + " (SELECT id FROM techtask WHERE customer_id =?))";
	private final static String INSERT_SQL = "INSERT  INTO invoice (number,date,project_id,price) VALUES (?,?,?,?)";
	private final static String UPDATE_SQL = "UPDATE invoice SET number=?,date=?,project_id=?,price=? WHERE id=?";
	private final static String SET_PAID_FLAG_SQL = "UPDATE invoice SET paid=? WHERE id=?";
	private final static String DELETE_SQL = "DELETE FROM invoice WHERE id = ?";

	private static final String NUMBER_COLUMN = "number";
	private static final String ID_COLUMN = "id";
	private static final String DATE_COLUMN = "date";
	private static final String PRICE_COLUMN = "price";
	private static final String PROJECT_ID_COLUMN = "project_id";

	public static InvoiceDAO getInstance() {
		return instance;
	}

	@Override
	public Invoice getById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {

				Invoice invoice = getInvoiceFromRs(rs);
				return invoice;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting invoice by id", e);
			throw new DAOException("SQL Exception while getting invoice by id", e);
		}

	}

	@Override
	public Invoice getByProjectId(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_PROJECT_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {

				Invoice invoice = getInvoiceFromRs(rs);
				return invoice;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting invoice by project id", e);
			throw new DAOException("SQL Exception while getting invoice by project id", e);
		}
	}

	@Override
	public List<Invoice> getAll() throws DAOException {

		List<Invoice> result = new ArrayList<>();

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {

			ResultSet rs = stm.executeQuery(GET_ALL_SQL);

			while (rs.next()) {

				Invoice invoice = getInvoiceFromRs(rs);

				result.add(invoice);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting invoice list", e);
			throw new DAOException("SQL Exception while getting invoice list", e);
		}
	}

	@Override
	public int insert(Invoice invoice) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, invoice.getNumber());
			java.sql.Date date = new java.sql.Date(invoice.getDate().getTime());
			statement.setDate(2, date);
			statement.setInt(3, invoice.getProject().getId());
			statement.setDouble(4, invoice.getPrice());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();

			if (resultSet.next()) {
				return (resultSet.getInt(1));
			} else {
				throw new DAOException("Unable to insert invoice into Database");
			}

		} catch (SQLException e) {
			LOG.error("SQL Exception while writing invoice in database", e);
			throw new DAOException("SQL Exception writing invoice in database", e);
		}
	}

	@Override
	public void setPaidFlag(int invoiceId, boolean paidFlag) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(SET_PAID_FLAG_SQL)) {
			statement.setBoolean(1, paidFlag);
			statement.setInt(2, invoiceId);
			statement.executeUpdate();

		} catch (SQLException e) {
			LOG.error("SQL Exception while writing invoice in database", e);
			throw new DAOException("SQL Exception writing invoice in database", e);
		}
	}

	@Override
	public void deleteById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_SQL)) {

			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			LOG.error("SQL Exception deleting invoice", e);
			throw new DAOException("SQL Exception deleting invoice", e);
		}

	}

	@Override
	public void update(Invoice invoice) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_SQL)) {
			statement.setString(1, invoice.getNumber());

			java.sql.Date date = new java.sql.Date(invoice.getDate().getTime());
			statement.setDate(2, date);
			statement.setInt(3, invoice.getProject().getId());
			statement.setDouble(4, invoice.getPrice());
			statement.setInt(5, invoice.getId());
			statement.executeUpdate();

		} catch (SQLException e) {
			LOG.error("SQL Exception while writing invoice in database", e);
			throw new DAOException("SQL Exception writing invoice in database", e);
		}
	}

	@Override
	public List<Invoice> getUnpaid(Customer customer) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_CUSTOMER_ID_SQL);) {

			stm.setInt(1, customer.getId());
			ResultSet rs = stm.executeQuery();

			List<Invoice> result = new ArrayList<>();
			while (rs.next()) {
				result.add(getInvoiceFromRs(rs));
			}

			return result;
		} catch (SQLException e) {
			LOG.error("SQL Exception while getting invoice for customer", e);
			throw new DAOException("SQL Exception writing  invoice for customer", e);
		}

	}
	/**
	 * Method builds invoice from result set
	 * @param rs			result set
	 * @return				instance of invoice
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Invoice getInvoiceFromRs(ResultSet rs) throws SQLException {
		Invoice invoice = new Invoice();

		invoice.setId(rs.getInt(ID_COLUMN));
		invoice.setNumber(rs.getString(NUMBER_COLUMN));
		invoice.setDate(rs.getDate(DATE_COLUMN));
		invoice.setPrice(rs.getDouble(PRICE_COLUMN));
		invoice.setPaid(rs.getBoolean(PAID_COLUMN));

		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		Project project = dao.getHeaderById(rs.getInt(PROJECT_ID_COLUMN));
		invoice.setProject(project);
		return invoice;

	}

}
