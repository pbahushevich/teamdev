package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.dao.intf.UserDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class UserDAOImpl implements UserDAO {

	private static final UserDAO instance = new UserDAOImpl();
	
	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private static final String GET_BY_LOGIN_PASS_SQL = "SELECT * FROM user where login = ? AND password = ?";
	private static final String GET_BY_LOGIN_SQL = "SELECT * FROM user where login = ?";
	private static final String GET_BY_ID_SQL = "SELECT * FROM user where id = ?";
	private static final String INSERT_SQL = "INSERT INTO `user`(`name`, telephone, email,login, password, role_id) VALUES(?,?,?,?,?,?)";
	private static final String DELETE_BY_ID_SQL = "DELETE FROM user where id = ?";

	private static final int LOGIN_PARAM = 1;
	private static final int PASSWORD_PARAM = 2;

	private static final String ID_COLUMN = "id";
	private static final String LOGIN_COLUMN = "login";
	private static final String NAME_COLUMN = "name";
	private static final String TELEPHONE_COLUMN = "telephone";
	private static final String EMAIL_COLUMN = "email";
	private static final String ROLE_ID_COLUMN = "role_id";

	
	public static UserDAO getInstance(){
		return instance;
	}

	@Override
	public User getById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {

				User user = getUserFromRs(rs);
				return user;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting user by Id", e);
			throw new DAOException("SQL error while getting user by Id", e);
		}

	}

	@Override
	public User getByLogin(String login) throws DAOException {

		User user = null;

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_LOGIN_SQL);) {
	
			stm.setString(LOGIN_PARAM, login);
			ResultSet result = stm.executeQuery();
			if (result.next()) {
				user = getUserFromRs(result);
			}
			return user;

		} catch (SQLException e) {
			LOG.error("SQL error while getting user by login and password", e);
			throw new DAOException("SQL error while getting user by login and password", e);
		}

	}

	@Override
	public User getByLoginPass(String login, String pass) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_LOGIN_PASS_SQL);) {

			stm.setString(LOGIN_PARAM, login);
			stm.setString(PASSWORD_PARAM, String.valueOf(pass.hashCode()));

			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				User user = getUserFromRs(rs);
				return user;
			} else {
				return null;
			}
		} catch (SQLException e) {
			LOG.error("SQL error while getting user by login and password", e);
			throw new DAOException("SQL error while getting user by login and password", e);
		}

	}

	@Override
	public int insert(User user) throws DAOException {

		if (getByLogin(user.getLogin()) != null) {
			throw new DAOException("User with such login already exist." + user.getLogin());
		}

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS)) {

			return executeInsertStatement(user, statement);

		} catch (SQLException e) {
			LOG.error("SQL error while getting writing user to database", e);
			throw new DAOException("SQL error while getting writing user to database", e);
		}

	}

	@Override
	public int insertInTransaction(User user, Connection connection) throws DAOException {

		if (getByLogin(user.getLogin()) != null) {
			throw new DAOException("User with such login already exist." + user.getLogin());
		}

		try (PreparedStatement statement = connection.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS)) {
			return executeInsertStatement(user, statement);
		} catch (SQLException e) {
			LOG.error("SQL error while getting writing user to database in transaction", e);
			throw new DAOException("SQL error while getting writing user to database in transaction", e);
		}

	}

	@Override
	public void deleteByIdInTransaction(Integer id, Connection connection) throws DAOException {

		try (PreparedStatement stm = connection.prepareStatement(DELETE_BY_ID_SQL, Statement.RETURN_GENERATED_KEYS)) {
			
			stm.setInt(1, id);
			stm.executeUpdate();
			return;
		} catch (SQLException e) {
			LOG.error("SQL error while getting deleting user in transaction", e);
			throw new DAOException("SQL error while getting deleting user in transaction", e);
		}

	}

	/**
	 * Method builds User from result set
	 * @param rs			result set
	 * @return				instance of User
	 * @throws SQLException
	 * @throws DAOException
	 */
	private User getUserFromRs(ResultSet rs) throws SQLException, DAOException {
		
		User user;
		user = new User();
		user.setId(rs.getInt(ID_COLUMN));
		user.setName(rs.getString(NAME_COLUMN));
		user.setEmail(rs.getString(EMAIL_COLUMN));
		user.setTelephone(rs.getString(TELEPHONE_COLUMN));
		user.setLogin(rs.getString(LOGIN_COLUMN));

		RoleDAO roleDao = new RoleDAOImpl();
		user.setRole(roleDao.getById(rs.getInt(ROLE_ID_COLUMN)));
		return user;
	}
	
	/**
	 * Method that used by others to insert user into database.
	 * @param user			user to insert into database
	 * @param statement		prepared statement for insertion
	 * @return				id of the new entry
	 * @throws SQLException
	 * @throws DAOException
	 */
	private int executeInsertStatement(User user, PreparedStatement statement) throws SQLException, DAOException {

		statement.setString(1, user.getName());
		statement.setString(2, user.getTelephone());
		statement.setString(3, user.getEmail());
		statement.setString(4, user.getLogin());
		statement.setString(5, String.valueOf(user.getPassword().hashCode()));

		RoleDAO roleDAO = JDBCDAOFactory.getInstance().getRoleDAO();
		int roleId = roleDAO.getId(user.getRole());
		statement.setInt(6, roleId);

		statement.executeUpdate();
		ResultSet resultSet = statement.getGeneratedKeys();
		if (resultSet.next()) {
			return (resultSet.getInt(1));
		} else {
			throw new DAOException("Unable to insert user into Database");
		}
	}

}
