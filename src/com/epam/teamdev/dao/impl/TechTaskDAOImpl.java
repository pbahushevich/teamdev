package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.CustomerDAO;
import com.epam.teamdev.dao.intf.TechTaskDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class TechTaskDAOImpl implements TechTaskDAO {

	private static final TechTaskDAO instance = new TechTaskDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_BY_ID_SQL = "SELECT * FROM techtask WHERE id =?";
	private final static String GET_BY_CUSTOMER_ID_SQL = "SELECT * FROM techtask WHERE customer_id =?";
	private final static String GET_TASK_WORK_SQL = "SELECT tt.id id,wt.title wt_title, work_type_id, q.title q_title,qualification_id, 	devcount  "
			+ "FROM techtask_spec tt " + "LEFT JOIN qualification q ON tt.qualification_id = q.id "
			+ "LEFT JOIN work_type wt ON tt.work_type_id = wt.id " + "WHERE techtask_id=? order by tt.id";
	private final static String GET_NEW_SQL = "SELECT * FROM techtask WHERE id NOT in (SELECT techtask_id FROM project) ORDER BY title";
	private final static String INSERT_TASK_SQL = "INSERT  INTO techtask (title,date,customer_id) VALUES (?,?,?)";
	private final static String INSERT_TASK_WORK_SQL = "INSERT  INTO techtask_spec (techtask_id,work_type_id,qualification_id,devcount) VALUES (?,?,?,?)";
	private final static String DELETE_WORK_BY_TECHTASK_SQL = "DELETE FROM techtask_spec WHERE techtask_id=?";
	private final static String DELETE_TECHTASK_SQL = "DELETE FROM techtask WHERE id=?";

	private static final String ID_COLUMN = "id";
	private static final String CUSTOMER_ID_COLUMN = "customer_id";
	private static final String WORKTYPE_ID_COLUMN = "work_type_id";
	private static final String WORKTYPE_TITLE_COLUMN = "wt_title";
	private static final String QUALIFICATION_ID_COLUMN = "qualification_id";
	private static final String QUALIFICATION_TITLE_COLUMN = "q_title";
	private static final String DEVCOUNT_COLUMN = "devcount";
	private static final String TITLE_COLUMN = "title";
	private static final String DATE_COLUMN = "date";

	public static TechTaskDAO getInstance() {
		return instance;
	}

	@Override
	public TechTask getHeaderById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {

				TechTask task = getTaskFromRS(rs);

				return task;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting techtask by Id", e);
			throw new DAOException("SQL error while getting techtask by Id", e);
		}

	}

	@Override
	public List<TechTask> getUnprocessed() throws DAOException {

		List<TechTask> result = new ArrayList<>();

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {

			ResultSet rs = stm.executeQuery(GET_NEW_SQL);

			while (rs.next()) {

				TechTask task = getTaskFromRS(rs);
				result.add(task);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL error while getting unprocessed techtask list", e);
			throw new DAOException("SQL error while getting unprocessed techtask list", e);
		}
	}

	@Override
	public List<TechTask> getByCustomer(Customer customer) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_CUSTOMER_ID_SQL);) {

			stm.setInt(1, customer.getId());
			ResultSet rs = stm.executeQuery();
			List<TechTask> result = new ArrayList<>();

			while (rs.next()) {

				TechTask task = getTaskFromRS(rs);

				List<TaskWork> list = getWorkList(task);
				task.setWorkList(list);

				result.add(task);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL error while getting techtask list for customer.", e);
			throw new DAOException("SQL error while getting techtask list for customer.", e);
		}

	}

	@Override
	public List<TaskWork> getWorkList(TechTask task) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement pstm = connection.prepareStatement(GET_TASK_WORK_SQL);) {
			List<TaskWork> list = new ArrayList<>();

			pstm.setInt(1, task.getId());

			ResultSet rs = pstm.executeQuery();
			while (rs.next()) {

				TaskWork techTaskWork = task.new TaskWork();

				techTaskWork.setId(rs.getInt(ID_COLUMN));

				WorkType workType = new WorkType();
				workType.setId(rs.getInt(WORKTYPE_ID_COLUMN));
				workType.setTitle(rs.getString(WORKTYPE_TITLE_COLUMN));

				Qualification qualification = new Qualification();
				qualification.setId(rs.getInt(QUALIFICATION_ID_COLUMN));
				qualification.setTitle(rs.getString(QUALIFICATION_TITLE_COLUMN));

				techTaskWork.setWorkType(workType);
				techTaskWork.setQualification(qualification);
				techTaskWork.setDevCount(rs.getInt(DEVCOUNT_COLUMN));

				list.add(techTaskWork);

			}
			return list;
		} catch (SQLException e) {
			LOG.error("SQL error while getting techtask work list", e);
			throw new DAOException("SQL error while getting techtask work list", e);
		}
	}

	@Override
	public int insert(TechTask task) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement taskStm = connection.prepareStatement(INSERT_TASK_SQL,
						Statement.RETURN_GENERATED_KEYS);
				PreparedStatement workStm = connection.prepareStatement(INSERT_TASK_WORK_SQL,
						Statement.RETURN_GENERATED_KEYS)) {

			connection.setAutoCommit(false);

			taskStm.setString(1, task.getTitle());
			java.sql.Date date = new java.sql.Date(task.getDate().getTime());
			taskStm.setDate(2, date);
			taskStm.setInt(3, task.getCustomer().getId());

			taskStm.executeUpdate();
			ResultSet resultSet = taskStm.getGeneratedKeys();

			if (!resultSet.next()) {
				connection.rollback();
				throw new DAOException("Unable to write techtask to database");
			}
			int taskId = resultSet.getInt(1);
			for (TaskWork work : task.getWorkList()) {
				workStm.setInt(1, taskId);
				workStm.setInt(2, work.getWorkType().getId());
				workStm.setInt(3, work.getQualification().getId());
				workStm.setInt(4, work.getDevCount());
				workStm.executeUpdate();
			}

			connection.commit();
			return taskId;

		} catch (SQLException e) {
			LOG.error("SQL error while writing techtask to database", e);
			throw new DAOException("SQL error while writing techtask to database", e);
		}

	}

	@Override
	public void update(TechTask task) {
		deleteById(task.getId());
		insert(task);
	}

	@Override
	public void deleteById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement workStm = connection.prepareStatement(DELETE_WORK_BY_TECHTASK_SQL);
				PreparedStatement taskStm = connection.prepareStatement(DELETE_TECHTASK_SQL);) {

			connection.setAutoCommit(false);

			workStm.setInt(1, id);
			workStm.executeUpdate();

			taskStm.setInt(1, id);
			taskStm.executeUpdate();

			connection.commit();

		} catch (SQLException e) {
			LOG.error("SQL error while deleting techtask", e);
			throw new DAOException("SQL error while deleting techtask", e);
		}
	}

	private TechTask getTaskFromRS(ResultSet rs) throws SQLException, DAOException {

		TechTask techTask = new TechTask();

		techTask.setId(rs.getInt(ID_COLUMN));
		techTask.setTitle(rs.getString(TITLE_COLUMN));
		techTask.setDate(rs.getDate(DATE_COLUMN));
		CustomerDAO customerDAO = JDBCDAOFactory.getInstance().getCustomerDAO();
		Customer customer = customerDAO.getById(rs.getInt(CUSTOMER_ID_COLUMN));
		techTask.setCustomer(customer);

		return techTask;
	}
}
