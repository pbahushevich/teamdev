package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.DeveloperDAO;
import com.epam.teamdev.dao.intf.ManagerDAO;
import com.epam.teamdev.dao.intf.ProjectDAO;
import com.epam.teamdev.dao.intf.TechTaskDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class ProjectDAOImpl implements ProjectDAO {
	private static final String GET_WORK_BY_ID_SQL = "SELECT ps.id id, ps.project_id,ps.developer_id, ps.time_spent, ps.date_start, "
			+ " ps.date_end, ts.id ts_id, q.title qualification_title,q.id qualification_id, w.title worktype_title, w.id worktype_id, p.id project_id, p.title title "
			+ " FROM project_spec ps  " + "INNER JOIN techtask_spec ts  ON ps.techtask_spec_id = ts.id  AND ps.id =? "
			+ " LEFT JOIN qualification q  ON ts.qualification_id = q.id "
			+ " LEFT JOIN work_type w ON ts.work_type_id = w.id " + " LEFT JOIN project p ON ps.project_id = p.id ";

	private static final ProjectDAO instance = new ProjectDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");
	private static final String MANAGER_ID_COLUMN = "manager_id";

	private final static String GET_BY_ID_SQL = "SELECT * FROM project WHERE id =?";
	private final static String GET_BY_MANAGER_ID_SQL = "SELECT * FROM project WHERE manager_id =?";
	private final static String GET_BY_TECHTASK_ID_SQL = "SELECT * FROM project WHERE techtask_id =?";
	private final static String GET_PROJECT_WORK_SQL = "SELECT ps.id id, ps.project_id,ps.developer_id, ps.time_spent, ps.date_start, "
			+ "ps.date_end, ts.id ts_id, q.title qualification_title,q.id qualification_id, w.title worktype_title,w.id worktype_id "
			+ " FROM project_spec ps  "
			+ "INNER JOIN techtask_spec ts  ON ps.techtask_spec_id = ts.id  AND project_id =? "
			+ "LEFT JOIN qualification q  ON ts.qualification_id = q.id "
			+ "LEFT JOIN work_type w ON ts.work_type_id = w.id";
	private final static String INSERT_PROJECT_SQL = "INSERT  INTO project (title,date,date_start,date_end, techtask_id,manager_id) "
			+ "VALUES (?,?,?,?,?,?)";
	private final static String INSERT_WORK_SQL = "INSERT  INTO project_spec (project_id,techtask_spec_id,date_start,date_end,developer_id)"
			+ " VALUES (?,?,?,?,?)";
	private final static String DELETE_WORK_BY_PROJECT_SQL = "DELETE FROM project_spec WHERE project_id=?";
	private final static String DELETE_PROJECT_SQL = "DELETE FROM project WHERE id=?";
	private final static String UPDATE_PROJECT_SQL = "UPDATE project SET title=?,date=?,date_start=?,date_end=?, techtask_id=?,manager_id=? WHERE id=?";
	private final static String UPDATE_WORK_SQL = "UPDATE project_spec SET date_start=?,date_end=?, developer_id=?,time_spent=? WHERE id=?";

	private static final String ID_COLUMN = "id";
	private static final String PROJECT_ID_COLUMN = "project_id";
	private static final String TECHTASK_ID_COLUMN = "techtask_id";
	private static final String DEVELOPER_ID_COLUMN = "developer_id";
	private static final String TITLE_COLUMN = "title";
	private static final String DATE_COLUMN = "date";
	private static final String DATE_START_COLUMN = "date_start";
	private static final String DATE_END_COLUMN = "date_end";
	private static final String QUALIFICATION_ID_COLUMN = "qualification_id";
	private static final String QUALIFICATION_TITLE_COLUMN = "qualification_title";
	private static final String WORKTYPE_TITLE_COLUMN = "worktype_title";
	private static final String WORKTYPE_ID_COLUMN = "worktype_id";
	private static final String TASK_WORK_ID_COLUMN = "ts_id";
	private static final String TIME_SPENT_COLUMN = "time_spent";

	public static ProjectDAO getInstance() {
		return instance;
	}

	public Project getHeaderById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement projectStm = connection.prepareStatement(GET_BY_ID_SQL);
				PreparedStatement workStm = connection.prepareStatement(GET_PROJECT_WORK_SQL);) {

			projectStm.setInt(1, id);
			ResultSet rs = projectStm.executeQuery();

			if (!rs.next()) {
				return null;
			}
			Project project = getProjectFromRs(rs);

			return project;

		} catch (SQLException e) {
			LOG.error("SQL error while getting project by Id", e);
			throw new DAOException("SQL error while getting project by Id", e);
		}

	}

	@Override
	public Project getByTechTaskId(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_TECHTASK_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {

				Project project = getProjectFromRs(rs);

				return project;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting project by TechTask", e);
			throw new DAOException("SQL error while getting project by TechTask", e);
		}

	}

	@Override
	public List<ProjectWork> getWorkList(Project project) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_PROJECT_WORK_SQL);) {

			List<ProjectWork> list = new ArrayList<>();
			stm.setInt(1, project.getId());
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {

				ProjectWork work = getProjectWorkFromRs(project, rs);
				list.add(work);

			}
			return list;
		} catch (SQLException e) {
			LOG.error("SQL error while getting work list for project", e);
			throw new DAOException("SQL error while getting work list for project", e);
		}
	}

	/**
	 * Method builds project work from result set
	 * @param project	project, project work is build for
	 * @param rs		result set
	 * @return			instance of project work
	 * @throws SQLException
	 */
	private ProjectWork getProjectWorkFromRs(Project project, ResultSet rs) throws SQLException {

		ProjectWork work = project.new ProjectWork();
		work.setId(rs.getInt(ID_COLUMN));

		work.setDateStart(rs.getDate(DATE_START_COLUMN));
		work.setDateEnd(rs.getDate(DATE_END_COLUMN));

		DeveloperDAO dDao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		Developer developer = dDao.getById(rs.getInt(DEVELOPER_ID_COLUMN));
		work.setDeveloper(developer);

		work.setTimeSpent(rs.getDouble(TIME_SPENT_COLUMN));

		TaskWork taskWork = new TechTask().new TaskWork();
		taskWork.setId(Integer.valueOf(rs.getInt(TASK_WORK_ID_COLUMN)));

		Qualification qualification = new Qualification();
		qualification.setId(rs.getInt(QUALIFICATION_ID_COLUMN));
		qualification.setTitle(rs.getString(QUALIFICATION_TITLE_COLUMN));
		taskWork.setQualification(qualification);

		WorkType workType = new WorkType();
		workType.setId(rs.getInt(WORKTYPE_ID_COLUMN));
		workType.setTitle(rs.getString(WORKTYPE_TITLE_COLUMN));
		taskWork.setWorkType(workType);

		work.setTaskWork(taskWork);
		return work;
	}

	@Override
	public List<Project> getByManager(Manager manager) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_MANAGER_ID_SQL);) {

			stm.setInt(1, manager.getId());
			ResultSet rs = stm.executeQuery();
			List<Project> result = new ArrayList<>();

			while (rs.next()) {

				Project project = getProjectFromRs(rs);

				result.add(project);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL error while getting project list for manager", e);
			throw new DAOException("SQL error while getting project list for manager", e);
		}

	}

	public int insert(Project project) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement projectStm = connection.prepareStatement(INSERT_PROJECT_SQL,
						Statement.RETURN_GENERATED_KEYS);
				PreparedStatement workStm = connection.prepareStatement(INSERT_WORK_SQL,
						Statement.RETURN_GENERATED_KEYS)) {

			connection.setAutoCommit(false);

			projectStm.setString(1, project.getTitle());
			projectStm.setDate(2, getSqlDate(project.getDate()));
			projectStm.setDate(3, getSqlDate(project.getDateStart()));
			projectStm.setDate(4, getSqlDate(project.getDateEnd()));
			projectStm.setInt(5, project.getTechTask().getId());
			projectStm.setInt(6, project.getManager().getId());

			projectStm.executeUpdate();
			ResultSet resultSet = projectStm.getGeneratedKeys();

			if (!resultSet.next()) {
				connection.rollback();
				throw new DAOException("Unable to write project to database");
			}
			int projectId = resultSet.getInt(1);
			for (ProjectWork work : project.getWorkList()) {
				workStm.setInt(1, projectId);
				workStm.setInt(2, work.getTaskWork().getId());
				workStm.setDate(3, getSqlDate(work.getDateStart()));
				workStm.setDate(4, getSqlDate(work.getDateEnd()));
				workStm.setInt(5, work.getDeveloper().getId());
				workStm.executeUpdate();
			}

			connection.commit();
			return projectId;

		} catch (SQLException e) {
			LOG.error("SQL error while writing project to database", e);
			throw new DAOException("SQL error while writing project to database", e);
		}

	}

	@Override
	public void deleteById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement workStm = connection.prepareStatement(DELETE_WORK_BY_PROJECT_SQL);
				PreparedStatement projectStm = connection.prepareStatement(DELETE_PROJECT_SQL);) {

			Project project = getHeaderById(id);
			if (project == null) {
				throw new DAOException("Cann't delete project. There is no project with id = "+id);
			}
			connection.setAutoCommit(false);

			workStm.setInt(1, id);
			workStm.executeUpdate();

			projectStm.setInt(1, id);
			projectStm.executeUpdate();

			connection.commit();

		} catch (SQLException e) {
			LOG.error("SQL error while deleting project", e);
			throw new DAOException("SQL error while deleting project", e);
		}
	}


	@Override
	public void update(Project project) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement projectStm = connection.prepareStatement(UPDATE_PROJECT_SQL);
				PreparedStatement workStm = connection.prepareStatement(UPDATE_WORK_SQL);) {

			connection.setAutoCommit(false);

			projectStm.setString(1, project.getTitle());
			projectStm.setDate(2, getSqlDate(project.getDate()));
			projectStm.setDate(3, getSqlDate(project.getDateStart()));
			projectStm.setDate(4, getSqlDate(project.getDateEnd()));
			projectStm.setInt(5, project.getTechTask().getId());
			projectStm.setInt(6, project.getManager().getId());
			projectStm.setInt(7, project.getId());

			projectStm.executeUpdate();

			for (ProjectWork work : project.getWorkList()) {
				workStm.setDate(1, getSqlDate(work.getDateStart()));
				workStm.setDate(2, getSqlDate(work.getDateEnd()));
				workStm.setInt(3, work.getDeveloper().getId());
				workStm.setDouble(4, work.getTimeSpent());
				workStm.setInt(5, work.getId());
				workStm.executeUpdate();
			}

			connection.commit();

		} catch (SQLException e) {
			LOG.error("SQL error while updating project", e);
			throw new DAOException("SQL error while updating project", e);
		}

	}

	@Override
	public ProjectWork getWorkById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement workStm = connection.prepareStatement(GET_WORK_BY_ID_SQL)) {

			workStm.setInt(1, id);
			ResultSet rs = workStm.executeQuery();
			if (rs.next()) {
				int projectId = rs.getInt(PROJECT_ID_COLUMN);
				Project project = getHeaderById(projectId);
				ProjectWork work = getProjectWorkFromRs(project, rs);
				return work;
			} else {
				throw new DAOException("There is no project work with id = " + id);
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting project work information for id " + id, e);
			throw new DAOException("SQL error while getting project work information for id " + id, e);
		}
	}

	/**
	 * Method builds project from result set
	 * @param rs			result set
	 * @return				instance of invoice
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Project getProjectFromRs(ResultSet rs) throws SQLException, DAOException {
		Project project = new Project();

		project.setId(rs.getInt(ID_COLUMN));
		project.setTitle(rs.getString(TITLE_COLUMN));
		project.setDate(rs.getDate(DATE_COLUMN));
		project.setDateStart(rs.getDate(DATE_START_COLUMN));
		project.setDateEnd(rs.getDate(DATE_END_COLUMN));

		TechTaskDAO taskDAO = JDBCDAOFactory.getInstance().getTechTaskDAO();
		TechTask task = taskDAO.getHeaderById(rs.getInt(TECHTASK_ID_COLUMN));

		project.setTechTask(task);
		project.setManager(getManagerFromRS(rs));
		return project;
	}

	/**
	 * Method gets manager from result set by its manager id column
	 * @param rs			result set
	 * @return				instance of manager
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Manager getManagerFromRS(ResultSet rs) throws DAOException, SQLException {

		ManagerDAO dao = JDBCDAOFactory.getInstance().getManagerDAO();
		Manager manager = dao.getById(rs.getInt(MANAGER_ID_COLUMN));
		return manager;
	}

	/**
	 * Method converts java.util.Date to java.sql.Date
	 * @param date	date to convert
	 * @return
	 */
	private java.sql.Date getSqlDate(Date date) {
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}


}
