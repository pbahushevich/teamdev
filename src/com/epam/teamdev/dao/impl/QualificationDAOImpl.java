package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.intf.QualificationDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class QualificationDAOImpl implements QualificationDAO {

	private static final QualificationDAO instance = new QualificationDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_ALL_SQL = "SELECT * FROM qualification ORDER BY title";
	private final static String GET_BY_ID_SQL = "SELECT * FROM qualification WHERE id = ?";
	private final static String INSERT_SQL = "INSERT  INTO qualification (title) VALUES (?)";
	private final static String DELETE_SQL = "DELETE FROM qualification WHERE id = ?";

	private static final String TITLE_COLUMN = "title";
	private static final String ID_COLUMN = "id";

	public static QualificationDAO getInstance() {
		return instance;
	}

	@Override
	public Qualification getById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {

				Qualification qualification = getQualificationFromRS(rs);
				return qualification;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting qualification by Id", e);
			throw new DAOException("SQL error while getting qualification by Id", e);
		}
	}

	@Override
	public List<Qualification> getAll() throws DAOException {

		List<Qualification> result = new ArrayList<>();

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {

			ResultSet rs = stm.executeQuery(GET_ALL_SQL);

			while (rs.next()) {

				Qualification qualification = getQualificationFromRS(rs);

				result.add(qualification);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL error while getting qualification list", e);
			throw new DAOException("SQL error while getting qualification list", e);
		}

	}

	@Override
	public int insert(Qualification qualification) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, qualification.getTitle());
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();

			if (resultSet.next()) {
				return (resultSet.getInt(1));
			} else {
				throw new DAOException("Unable to insert qualification into Database");
			}

		} catch (SQLException e) {
			LOG.error("SQL error while inserting qualification", e);
			throw new DAOException("SQL error while inserting qualification", e);
		}
	}

	@Override
	public void deleteById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_SQL)) {

			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			LOG.error("SQL error while getting deleting qualification by Id", e);
			throw new DAOException("SQL error while getting deleting qualification by Id", e);
		}

	}

	/**
	 * Method builds Qualification from result set
	 * @param rs			result set
	 * @return				instance of Qualification
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Qualification getQualificationFromRS(ResultSet rs) throws SQLException {
		Qualification qualification = new Qualification();

		qualification.setId(rs.getInt(ID_COLUMN));
		qualification.setTitle(rs.getString(TITLE_COLUMN));
		return qualification;
	}

}
