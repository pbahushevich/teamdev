package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.DeveloperDAO;
import com.epam.teamdev.dao.intf.ProjectDAO;
import com.epam.teamdev.dao.intf.QualificationDAO;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.dao.intf.UserDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class DeveloperDAOImpl implements DeveloperDAO {

	private static final String PROJECT_SPEC_ID_COLUMND = "project_spec_id";

	private static final DeveloperDAO instance = new DeveloperDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_BY_ID_SQL = "SELECT d.qualification_id, d.user_id,d.id, name, telephone, email,login, password, role_id "
			+ "FROM developer d LEFT JOIN user u ON u.id = d.user_id WHERE d.id =?";
	private final static String GET_BY_USER_ID_SQL = "SELECT d.qualification_id, d.user_id,d.id, name, telephone, email,login, password, role_id "
			+ "FROM developer d LEFT JOIN user u ON u.id = d.user_id WHERE d.user_id =?";
	private final static String GET_ALL_SQL = "SELECT d.qualification_id, d.user_id,d.id, name, telephone, email,login, password, role_id "
			+ "FROM developer d LEFT JOIN user u ON u.id = d.user_id";
	private final static String GET_DEVELOPER_WORKLIST_SQL = "SELECT * FROM project_spec ps LEFT JOIN invoice iv ON ps.project_id = iv.project_id WHERE iv.id IS NULL AND developer_id = ?";
	private final static String GET_AVAILABLE_SQL = "SELECT d.qualification_id, d.user_id,d.id, u.name, u.telephone, u.email, u.login, u.password, u.role_id "
			+ " FROM developer d LEFT JOIN user u ON d.user_id = u.id   WHERE qualification_id = ?  "
			+ " AND d.id NOT IN (SELECT developer_id id FROM project_spec "
			+ "WHERE  (date_start <= ? AND date_end >=?) AND project_id<>?)";
	private static final String GET_DEVELOPERS_SCHEDULE = "SELECT d.qualification_id, d.user_id,d.id, name, telephone, email,login, password, role_id, ps.id project_spec_id "
			+ " FROM developer d LEFT JOIN project_spec ps ON d.id=ps.developer_id "
			+ " LEFT  JOIN user u ON u.id = d.user_id ORDER BY d.qualification_id, d.id";
	private static final String INSERT_SQL = "INSERT INTO `developer`( qualification_id, user_id) VALUES(?,?)";
	private static final String DELETE_BY_ID_SQL = "DELETE FROM developer WHERE id =?";
	private static final String ADD_TIME_SPENT_SQL = "UPDATE project_spec SET time_spent = time_spent +? WHERE id = ?";

	private static final String ID_COLUMN = "id";
	private static final String LOGIN_COLUMN = "login";
	private static final String NAME_COLUMN = "name";
	private static final String TELEPHONE_COLUMN = "telephone";
	private static final String EMAIL_COLUMN = "email";
	private static final String ROLE_ID_COLUMN = "role_id";
	private static final String USER_ID_COLUMN = "user_id";
	private static final String QUALIFICATION_ID_COLUMN = "qualification_id";

	public static DeveloperDAO getInstance() {
		return instance;
	}

	@Override
	public Developer getById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Developer developer = getDeveloperFromRS(rs);
				return developer;

			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting developer by id", e);
			throw new DAOException("SQL Exception while getting developer by id", e);
		}
	}

	@Override
	public Developer getByUser(User user) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_USER_ID_SQL);) {
			stm.setInt(1, user.getId());
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Developer developer = getDeveloperFromRS(rs);
				return developer;

			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting developer for user", e);
			throw new DAOException("SQL Exception while getting developer for user", e);
		}
	}

	@Override
	public List<Developer> getAll() throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {
			ResultSet rs = stm.executeQuery(GET_ALL_SQL);
			List<Developer> result = new ArrayList<>();

			while (rs.next()) {
				Developer developer = getDeveloperFromRS(rs);
				result.add(developer);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting developer list", e);
			throw new DAOException("SQL Exception while getting developer list", e);
		}
	}

	public int insert(Developer developer) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS)) {

			connection.setAutoCommit(false);

			UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();

			int userId = userDAO.insertInTransaction(developer, connection);
			developer.setUserId(userId);

			statement.setInt(1, developer.getQualification().getId());
			statement.setString(2, String.valueOf(userId));
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();

			if (resultSet.next()) {
				connection.commit();
				return (resultSet.getInt(1));
			} else {
				connection.rollback();
				throw new DAOException("Unable to insert customer into Database. Haven't got customer id in return");
			}
		} catch (SQLException e) {
			LOG.error("SQL Exception while writing developer to database", e);
			throw new DAOException("SQL Exception while writing developer to database", e);
		}
	}

	@Override
	public void deleteById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(DELETE_BY_ID_SQL)) {

			connection.setAutoCommit(false);

			Developer developer = getById(id);

			if (developer == null) {
				LOG.error("Unable to delete developer by id. There is no developer with id " + id);
				throw new DAOException("Unable to delete developer by id. There is no developer with id " + id);
			}

			stm.setInt(1, id);
			stm.executeUpdate();

			UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();
			userDAO.deleteByIdInTransaction(developer.getUserId(), connection);

			connection.commit();

		} catch (SQLException e) {
			LOG.error("SQL Exception while deleting developer", e);
			throw new DAOException("SQL Exception while deleting developer", e);
		}
	}

	@Override
	public List<Developer> getAvailable(Qualification qualification, Date dateStart, Date dateEnd, int projectId)
			throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_AVAILABLE_SQL);) {

			stm.setInt(1, qualification.getId());

			java.sql.Date dateS = new java.sql.Date(dateStart.getTime());
			java.sql.Date dateE = new java.sql.Date(dateEnd.getTime());

			stm.setDate(2, dateE);
			stm.setDate(3, dateS);
			stm.setInt(4, projectId);

			ResultSet rs = stm.executeQuery();
			List<Developer> result = new ArrayList<>();

			while (rs.next()) {
				Developer developer = getDeveloperFromRS(rs);
				result.add(developer);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting available developer list", e);
			throw new DAOException("SQL Exception while getting available developer list", e);
		}
	}

	@Override
	public List<ProjectWork> getDeveloperWorkList(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_DEVELOPER_WORKLIST_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			List<ProjectWork> result = new ArrayList<>();

			ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
			while (rs.next()) {
				int workId = rs.getInt(ID_COLUMN);

				ProjectWork work = dao.getWorkById(workId);
				result.add(work);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting available developer list", e);
			throw new DAOException("SQL Exception while getting available developer list", e);
		}
	}

	@Override
	public List<ProjectWork> getDevelopersSchedule() throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {

			ResultSet rs = stm.executeQuery(GET_DEVELOPERS_SCHEDULE);
			List<ProjectWork> result = new ArrayList<>();

			while (rs.next()) {
				int workId = rs.getInt(PROJECT_SPEC_ID_COLUMND);
				ProjectWork work = null;
				if (workId != 0) {
					ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
					work = dao.getWorkById(workId);
				} else {
					work = new Project().new ProjectWork();
					Developer developer = getDeveloperFromRS(rs);
					work.setDeveloper(developer);
				}
				result.add(work);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL Exception while getting available developer list", e);
			throw new DAOException("SQL Exception while getting available developer list", e);
		}
	}

	@Override
	public void addWorkSpentTime(int workId, double timeSpent) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(ADD_TIME_SPENT_SQL);) {

			stm.setDouble(1, timeSpent);
			stm.setInt(2, workId);
			int rowsNumber = stm.executeUpdate();

			if (rowsNumber == 0) {
				throw new DAOException("Error addding spent time to database. No entry was applied");
			}
		} catch (SQLException e) {
			LOG.error("SQL Exception while addding spent time to database", e);
			throw new DAOException("SQL Exception while addding spent time to database", e);
		}
	}

	/**
	 * Method builds Developer from result set
	 * @param rs			result set
	 * @return				instance of Developer
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Developer getDeveloperFromRS(ResultSet rs) throws SQLException, DAOException {
		Developer developer = new Developer();

		developer.setId(rs.getInt(ID_COLUMN));
		developer.setUserId(rs.getInt(USER_ID_COLUMN));

		QualificationDAO qDAO = JDBCDAOFactory.getInstance().getQualificationDAO();
		Qualification qualification = qDAO.getById(rs.getInt(QUALIFICATION_ID_COLUMN));
		developer.setQualification(qualification);

		developer.setName(rs.getString(NAME_COLUMN));
		developer.setEmail(rs.getString(EMAIL_COLUMN));
		developer.setTelephone(rs.getString(TELEPHONE_COLUMN));
		developer.setLogin(rs.getString(LOGIN_COLUMN));
		RoleDAO roleDao = new RoleDAOImpl();
		developer.setRole(roleDao.getById(rs.getInt(ROLE_ID_COLUMN)));

		return developer;

	}

}
