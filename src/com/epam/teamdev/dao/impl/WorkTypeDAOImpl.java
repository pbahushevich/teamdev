package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.intf.WorkTypeDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class WorkTypeDAOImpl implements WorkTypeDAO{

	private static final WorkTypeDAO instance = new WorkTypeDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_BY_ID_SQL = "SELECT * FROM work_type WHERE id = ?";
	private final static String GET_ALL_SQL ="SELECT * FROM work_type ORDER BY title";
	private final static String INSERT_SQL ="INSERT  INTO work_type (title) VALUES (?)";
	private final static String DELETE_SQL ="DELETE FROM work_type WHERE id = ?";
	
	private static final String TITLE_COLUMN = "title";
	private static final String ID_COLUMN = "id";
	
	
	public static WorkTypeDAO getInstance(){
		return instance;
	}

	@Override
	public WorkType getById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				WorkType workType = getWorkTypeFromRS(rs);
				return workType;
			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting workType by Id", e);
			throw new DAOException("SQL error while getting workType by Id", e);
		}
	}


	@Override
	public List<WorkType> getAll() throws DAOException {

		List<WorkType> result = new ArrayList<>();

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
			Statement stm = connection.createStatement();)
		{

			ResultSet rs= stm.executeQuery(GET_ALL_SQL);
			while (rs.next()) {
				
				WorkType workType = new WorkType();
				workType.setId(rs.getInt(ID_COLUMN));
				workType.setTitle(rs.getString(TITLE_COLUMN));
				
				result.add(workType);
			}
			return result;
			
		} catch (SQLException e) {
			LOG.error("SQL error while getting workType list", e);
			throw new DAOException("SQL error while getting workType list", e);
		}
	}

	@Override
	public int insert(WorkType workType) throws DAOException {

		try (	Connection connection = ConnectionPool.getInstance().takeConnection();
    			PreparedStatement statement = connection.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS)) {
	    		statement.setString(1, workType.getTitle());
	        	statement.executeUpdate();
	        	
	        	ResultSet resultSet = statement.getGeneratedKeys();
	        	
	        	if(resultSet.next()) {
	        		return(resultSet.getInt(1));
	        	}else{
	        		throw new DAOException("Unable to insert worktype into Database");
	        	}
 
		} catch (SQLException e) {
			LOG.error("SQL error while writing workType to database", e);
			throw new DAOException("SQL error while writing workType to database", e);
		}
	}

	@Override
	public void deleteById(int id) throws DAOException {

		try (	Connection connection = ConnectionPool.getInstance().takeConnection();
    			PreparedStatement statement = connection.prepareStatement(DELETE_SQL)) {
	    	
			statement.setInt(1, id);
	        statement.executeUpdate();
	        	
		} catch (SQLException e) {
			LOG.error("SQL error while deleting workType", e);
			throw new DAOException("SQL error while deleting workType", e);
		}
		
	}

	/**
	 * Method builds WorkType from result set
	 * @param rs			result set
	 * @return				instance of WorkType
	 * @throws SQLException
	 * @throws DAOException
	 */

	private WorkType getWorkTypeFromRS(ResultSet rs) throws SQLException {
		WorkType workType = new WorkType();

		workType.setId(rs.getInt(ID_COLUMN));
		workType.setTitle(rs.getString(TITLE_COLUMN));
		return workType;
	}

}
