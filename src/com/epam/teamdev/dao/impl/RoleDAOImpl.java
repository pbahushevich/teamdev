package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Role;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class RoleDAOImpl implements RoleDAO {

	private static final RoleDAO instance = new RoleDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_BY_ID_SQL = "SELECT * FROM role WHERE id =?";
	private final static String GET_BY_NAME_SQL = "SELECT * FROM role WHERE name =?";

	private static final String NAME_COLUMN = "name";
	private static final String ID_COLUMN = "id";

	public static RoleDAO getInstance() {
		return instance;
	}

	@Override
	public Role getById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {

			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Role role = Role.valueOf(rs.getString(NAME_COLUMN));
				return role;
			}
			return null;

		} catch (SQLException e) {
			LOG.error("SQL error while getting role by Id", e);
			throw new DAOException("SQL error while getting role by Id", e);
		}
	}

	@Override
	public int getId(Role role) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_NAME_SQL);) {

			stm.setString(1, role.toString());
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				int id = rs.getInt(ID_COLUMN);
				return id;
			} else {
				throw new DAOException("Unable to get role id");
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting role Id", e);
			throw new DAOException("SQL error while getting role Id", e);
		}
	}
}
