package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.CustomerDAO;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.dao.intf.UserDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class CustomerDAOImpl implements CustomerDAO {

	private static final CustomerDAO instance = new CustomerDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	private final static String GET_BY_ID_SQL = "SELECT c.company, c.user_id, c.id ,name, telephone, email,login, password, role_id  "
			+ "FROM customer c LEFT JOIN user u ON u.id = c.user_id WHERE c.id =?";
	private final static String GET_BY_USER_ID_SQL = "SELECT c.company, c.user_id, c.id ,name, telephone, email,login, password, role_id  "
			+ "FROM customer c LEFT JOIN user u ON u.id = c.user_id WHERE c.user_id =?";
	private final static String GET_ALL_SQL = "SELECT c.company, c.user_id, c.id ,name, telephone, email,login, password, role_id  "
			+ "FROM customer c LEFT JOIN user u ON u.id = c.user_id";
	private static final String INSERT_SQL = "INSERT INTO customer( company, user_id) VALUES(?,?)";
	private static final String DELETE_BY_ID_SQL = "DELETE FROM customer WHERE id =?";

	private static final String ID_COLUMN = "id";
	private static final String LOGIN_COLUMN = "login";
	private static final String NAME_COLUMN = "name";
	private static final String TELEPHONE_COLUMN = "telephone";
	private static final String EMAIL_COLUMN = "email";
	private static final String ROLE_ID_COLUMN = "role_id";
	private static final String USER_ID_COLUMN = "user_id";
	private static final String COMPANY_COLUMN = "company";

	private static final int ID_PARAM = 1;
	private static final int USER_ID_PARAM = 2;
	private static final int COMPANY_PARAM = 1;

	public static CustomerDAO getInstance() {
		return instance;
	}

	@Override
	public Customer getById(int id) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {
			stm.setInt(ID_PARAM, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Customer customer = getCustomerFromRS(rs);
				return customer;

			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting customer by Id", e);
			throw new DAOException("SQL error while getting customer by Id", e);
		}
	}

	@Override
	public Customer getByUser(User user) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_USER_ID_SQL);) {

			stm.setInt(ID_PARAM, user.getId());
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Customer customer = getCustomerFromRS(rs);
				return customer;

			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting customer by for user", e);
			throw new DAOException("SQL error while getting customer for user", e);
		}
	}

	@Override
	public List<Customer> getAll() throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {
			ResultSet rs = stm.executeQuery(GET_ALL_SQL);
			List<Customer> result = new ArrayList<>();

			while (rs.next()) {
				Customer customer = getCustomerFromRS(rs);
				result.add(customer);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL error while getting customer list", e);
			throw new DAOException("SQL error while getting customer list", e);
		}
	}

	@Override
	public int insert(Customer customer) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement customerStm = connection.prepareStatement(INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS);) {

			connection.setAutoCommit(false);

			UserDAO dao = JDBCDAOFactory.getInstance().getUserDAO();
			int userId = dao.insertInTransaction(customer, connection);
			customer.setUserId(userId);
			customerStm.setString(COMPANY_PARAM, customer.getCompany());
			customerStm.setInt(USER_ID_PARAM, userId);
			customerStm.executeUpdate();

			ResultSet resultSet = customerStm.getGeneratedKeys();

			if (resultSet.next()) {
				connection.commit();
				connection.setAutoCommit(true);
				return (resultSet.getInt(ID_PARAM));
			} else {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DAOException("Unable to insert customer into Database. Haven't got customer id in return");
			}
			
		} catch (SQLException e) {
			LOG.error("SQL error while writing customer in db", e);
			throw new DAOException("SQL error while writing customer in db", e);
		}
	}

	@Override
	public void deleteById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(DELETE_BY_ID_SQL)) {
			connection.setAutoCommit(false);

			Customer customer = getById(id);

			if (customer == null) {
				LOG.error("Unable to delete customer by id. There is no customer with id " + id);
				throw new DAOException("Unable to delete customer by id. There is no customer with id " + id);
			}

			stm.setInt(ID_PARAM, id);
			stm.executeUpdate();

			UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();
			userDAO.deleteByIdInTransaction(customer.getUserId(), connection);

			connection.commit();
			connection.setAutoCommit(true);

		} catch (SQLException e) {
			LOG.error("SQL error while deleting customer in db", e);
			throw new DAOException("SQL error while deleting customer in db", e);
		}
	}

	/**
	 * Method builds customer from result set
	 * 
	 * @param rs
	 *            result set
	 * @return instance of Customer
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Customer getCustomerFromRS(ResultSet rs) throws SQLException, DAOException {
		Customer customer = new Customer();

		customer.setId(rs.getInt(ID_COLUMN));
		customer.setUserId(rs.getInt(USER_ID_COLUMN));
		customer.setCompany(rs.getString(COMPANY_COLUMN));
		customer.setName(rs.getString(NAME_COLUMN));
		customer.setEmail(rs.getString(EMAIL_COLUMN));
		customer.setTelephone(rs.getString(TELEPHONE_COLUMN));
		customer.setLogin(rs.getString(LOGIN_COLUMN));
		RoleDAO roleDao = new RoleDAOImpl();
		customer.setRole(roleDao.getById(rs.getInt(ROLE_ID_COLUMN)));

		return customer;

	}

}
