package com.epam.teamdev.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.ManagerDAO;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.dao.intf.UserDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;

public class ManagerDAOImpl implements ManagerDAO {

	private static final ManagerDAO instance = new ManagerDAOImpl();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	
	private final static String GET_BY_ID_SQL = "SELECT m.department, m.user_id, m.id ,name, telephone, email,login, password, role_id  "
			+ " FROM manager m LEFT JOIN user u ON u.id = m.user_id WHERE m.id =?";
	private final static String GET_BY_USER_ID_SQL = "SELECT m.department, m.user_id, m.id ,name, telephone, email,login, password, role_id  "
			+ " FROM manager m LEFT JOIN user u ON u.id = m.user_id WHERE user_id =?";
	private final static String GET_ALL_SQL = "SELECT m.department, m.user_id, m.id ,name, telephone, email,login, password, role_id  "
			+ " FROM manager m LEFT JOIN user u ON u.id = m.user_id";
	private static final String INSERT_SQL = "INSERT INTO `manager`( department, user_id) VALUES(?,?)";
	private static final String DELETE_BY_ID_SQL = "DELETE FROM manager WHERE id =?";

	private static final String ID_COLUMN = "id";
	private static final String LOGIN_COLUMN = "login";
	private static final String NAME_COLUMN = "name";
	private static final String TELEPHONE_COLUMN = "telephone";
	private static final String EMAIL_COLUMN = "email";
	private static final String ROLE_ID_COLUMN = "role_id";
	private static final String USER_ID_COLUMN = "user_id";
	private static final String DEPARTMENT_COLUMN = "department";

	public static ManagerDAO getInstance() {
		return instance;
	}

	@Override
	public Manager getById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_ID_SQL);) {
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Manager manager = getManagerFromRS(rs);
				return manager;

			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting manager by Id", e);
			throw new DAOException("SQL error while getting manager by Id", e);
		}
	}

	@Override
	public Manager getByUser(User user) throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(GET_BY_USER_ID_SQL);) {
			stm.setInt(1, user.getId());
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				Manager manager = getManagerFromRS(rs);
				return manager;

			} else {
				return null;
			}

		} catch (SQLException e) {
			LOG.error("SQL error while getting manager for user", e);
			throw new DAOException("SQL error while getting manager for user", e);
		}
	}

	@Override
	public List<Manager> getAll() throws DAOException {

		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				Statement stm = connection.createStatement();) {
			ResultSet rs = stm.executeQuery(GET_ALL_SQL);
			List<Manager> result = new ArrayList<>();

			while (rs.next()) {
				Manager manager = getManagerFromRS(rs);
				result.add(manager);
			}
			return result;

		} catch (SQLException e) {
			LOG.error("SQL error while getting manager list", e);
			throw new DAOException("SQL error while getting  manager list", e);
		}
	}

	@Override
	public int insert(Manager manager) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS)) {

			connection.setAutoCommit(false);

			UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();

			int userId = userDAO.insertInTransaction(manager, connection);
			manager.setUserId(userId);

			statement.setString(1, manager.getDepartment());
			statement.setString(2, String.valueOf(userId));
			statement.executeUpdate();

			ResultSet resultSet = statement.getGeneratedKeys();

			if (resultSet.next()) {
				connection.commit();
				return (resultSet.getInt(1));
			} else {
				connection.rollback();
				throw new DAOException("Unable to insert manager into Database. Haven't got manager id in return");
			}
		} catch (SQLException e) {
			LOG.error("SQL error while inserting manager into database", e);
			throw new DAOException("SQL error while inserting manager into database", e);
		}

	}

	@Override
	public void deleteById(int id) throws DAOException {
		try (Connection connection = ConnectionPool.getInstance().takeConnection();
				PreparedStatement stm = connection.prepareStatement(DELETE_BY_ID_SQL)) {

			connection.setAutoCommit(false);

			Manager manager = getById(id);

			if (manager == null) {
				LOG.error("Error deleting manger from database. No manager with such id.");
				throw new DAOException("Error deleting manger from database. No manager with such id.");
			}
			stm.setInt(1, id);
			stm.executeUpdate();

			UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();
			userDAO.deleteByIdInTransaction(manager.getUserId(), connection);

			connection.commit();

		} catch (SQLException e) {
			LOG.error("SQL error while deleting manager", e);
			throw new DAOException("SQL error while deleting manager", e);
		}
	}

	/**
	 * Method builds manager from result set
	 * @param rs			result set
	 * @return				instance of manager
	 * @throws SQLException
	 * @throws DAOException
	 */
	private Manager getManagerFromRS(ResultSet rs) throws SQLException, DAOException {
		Manager manager = new Manager();

		manager.setId(rs.getInt(ID_COLUMN));
		manager.setDepartment(rs.getString(DEPARTMENT_COLUMN));
		manager.setUserId(rs.getInt(USER_ID_COLUMN));
		manager.setName(rs.getString(NAME_COLUMN));
		manager.setEmail(rs.getString(EMAIL_COLUMN));
		manager.setTelephone(rs.getString(TELEPHONE_COLUMN));
		manager.setLogin(rs.getString(LOGIN_COLUMN));
		RoleDAO roleDao = new RoleDAOImpl();
		manager.setRole(roleDao.getById(rs.getInt(ROLE_ID_COLUMN)));

		return manager;

	}

}
