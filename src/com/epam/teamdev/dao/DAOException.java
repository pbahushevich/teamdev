package com.epam.teamdev.dao;

/**
 * Exception thrown by DAO class methods.
 *
 */
public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 1650149788582625358L;

	public DAOException(String message, Exception e) {
		super(message, e);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

}
