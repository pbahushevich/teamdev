package com.epam.teamdev.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.dao.impl.CustomerDAOImpl;
import com.epam.teamdev.dao.impl.DeveloperDAOImpl;
import com.epam.teamdev.dao.impl.InvoiceDAOImpl;
import com.epam.teamdev.dao.impl.ManagerDAOImpl;
import com.epam.teamdev.dao.impl.ProjectDAOImpl;
import com.epam.teamdev.dao.impl.QualificationDAOImpl;
import com.epam.teamdev.dao.impl.RoleDAOImpl;
import com.epam.teamdev.dao.impl.TechTaskDAOImpl;
import com.epam.teamdev.dao.impl.UserDAOImpl;
import com.epam.teamdev.dao.impl.WorkTypeDAOImpl;
import com.epam.teamdev.dao.intf.CustomerDAO;
import com.epam.teamdev.dao.intf.DeveloperDAO;
import com.epam.teamdev.dao.intf.InvoiceDAO;
import com.epam.teamdev.dao.intf.ManagerDAO;
import com.epam.teamdev.dao.intf.ProjectDAO;
import com.epam.teamdev.dao.intf.QualificationDAO;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.dao.intf.TechTaskDAO;
import com.epam.teamdev.dao.intf.UserDAO;
import com.epam.teamdev.dao.intf.WorkTypeDAO;
import com.epam.teamdev.dao.pool.ConnectionPool;
import com.epam.teamdev.dao.pool.ConnectionPoolException;

/**
 * Factory that provides implementations for DAO interfaces.
 */
public class JDBCDAOFactory {
	

		private static final JDBCDAOFactory instance =new JDBCDAOFactory();
		
		
		private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.dbLogger");

	public static JDBCDAOFactory getInstance() { 
		return instance; 
	}
	
	private JDBCDAOFactory()  {
	}

	
	public void destroyFactory() {
		
		try {
			ConnectionPool.getInstance().dispose();
		} catch (ConnectionPoolException e) {
			LOG.error("Error while closing connetion pool",e);
		}
	}

	public RoleDAO getRoleDAO() throws DAOException {
		return RoleDAOImpl.getInstance();
	}

	public CustomerDAO getCustomerDAO() throws DAOException{
		return CustomerDAOImpl.getInstance();
	}

	public DeveloperDAO getDeveloperDAO() throws DAOException{
		return DeveloperDAOImpl.getInstance();
	}

	public InvoiceDAO getInvoiceDAO() throws DAOException{
			return InvoiceDAOImpl.getInstance();
		}

	public ManagerDAO getManagerDAO() throws DAOException{
		return ManagerDAOImpl.getInstance();
	}

	public ProjectDAO getProjectDAO() throws DAOException{
		return ProjectDAOImpl.getInstance();
	}

	public TechTaskDAO getTechTaskDAO() throws DAOException{
		return TechTaskDAOImpl.getInstance();
	}

	public UserDAO getUserDAO() throws DAOException{
		return UserDAOImpl.getInstance();
	}

	public WorkTypeDAO getWorkTypeDAO() throws DAOException{
		return WorkTypeDAOImpl.getInstance();
	}

	public QualificationDAO getQualificationDAO() throws DAOException{
		return QualificationDAOImpl.getInstance();
	}


}
