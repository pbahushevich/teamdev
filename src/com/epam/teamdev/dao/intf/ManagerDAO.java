package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.DAOException;

public interface ManagerDAO {

	public Manager getById(int id) throws DAOException;
	
	/**
	 * Method that gets Manager by its user. Mostly used during login process.
	 * 
	 * @param user
	 *            relevant user of the manager
	 * @return instance of Manager
	 * @throws DAOException
	 *             in case of SQL error
	 */
	public Manager getByUser(User user) throws DAOException;
	
	public List<Manager>getAll() throws DAOException;

	public int insert(Manager customer) throws DAOException;

	public void deleteById(int id) throws DAOException;
}
