package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.DAOException;

public interface CustomerDAO {

	public Customer getById(int id) throws DAOException;

	/**
	 * Method that gets Customer by its user. Mostly used during login process.
	 * 
	 * @param user
	 *            relevant user of the Customer
	 * @return instance of Customer
	 * @throws DAOException
	 *             in case of SQL error
	 */
	public Customer getByUser(User user) throws DAOException;

	public List<Customer> getAll() throws DAOException;

	public int insert(Customer customer) throws DAOException;

	public void deleteById(int id) throws DAOException;
}
