package com.epam.teamdev.dao.intf;

import java.util.Date;
import java.util.List;

import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.dao.DAOException;

public interface DeveloperDAO {

	public Developer getById(int id) throws DAOException;
	
	/**
	 * Method that gets Developer by its user. Mostly used during login process.
	 * 
	 * @param user
	 *            relevant user of the Developer
	 * @return instance of Customer
	 * @throws DAOException
	 *             in case of SQL error
	 */
	public Developer getByUser(User user) throws DAOException;
	
	public List<Developer> getAll() throws DAOException;
	
	/**
	 * Method to get the list of developers that are not busy on any other work for specified period.
	 * @param qualification		qualification of the developers
	 * @param dateStart			start of the period
	 * @param dateEnd			end of the period
	 * @param projectId			id of the project, that will be ignored during search process, in case method used while editing existing project.
	 * 							If list of the developers is specified for new project parameter should be 0;
	 * @return					List of developers that are not busy on any project other than specified in parameter projectId.
	 * @throws DAOException
	 */
	public List<Developer> getAvailable(Qualification qualification, Date dateStart,Date dateEnd,int projectId) throws DAOException;
	
	/**
	 * Method for getting all the developers with their worklist. 
	 * In case developer has no work in any project, 
	 * return ProjectWork filled only with developer
	 * @return				List of Developers with their work.
	 * @throws DAOException
	 */
	public List<ProjectWork> getDevelopersSchedule() throws DAOException;
	
	/**
	 * Method gets actual work for the developer.
	 * @param id			id of the developer
	 * @return				list of project work, that developer is doing.
	 * @throws DAOException 
	 */
	public List<ProjectWork> getDeveloperWorkList(int id) throws DAOException;
	
	public int insert(Developer developer) throws DAOException;
	
	/**
	 * Add by developer time to project work.
	 * @param workId		- id of the project work
	 * @param timeSpent		- time developer has spent
	 * @throws DAOException
	 */
	public  void addWorkSpentTime(int workId, double timeSpent) throws DAOException;
	
	public void deleteById(int id) throws DAOException;
}
