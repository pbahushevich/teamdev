package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.dao.DAOException;

public interface QualificationDAO {
	
	public Qualification getById(int id) throws DAOException;
	
	public List<Qualification> getAll() throws DAOException;
	
	public int insert(Qualification qualification) throws DAOException;
	
	public void deleteById(int id) throws DAOException;

}
