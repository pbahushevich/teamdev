package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.dao.DAOException;

public interface ProjectDAO {

	/**
	 * Gets project header (date, title etc.. by its id)
	 * @param id			id of the project
	 * @return				instance of project
	 * @throws DAOException
	 */
	public Project getHeaderById(int id) throws DAOException;

	/**
	 * Gets relevant project for the techtask
	 * 
	 * @param id
	 *            id of the techtask
	 * @return instance of project
	 * @throws DAOException
	 */
	public Project getByTechTaskId(int id) throws DAOException;

	/**
	 * Gets list of the work for the project
	 * @param project		Project, work list is get for
	 * @return				list of the work for the project
	 * @throws DAOException
	 */
	public List<ProjectWork> getWorkList(Project project) throws DAOException;

	/**
	 * Methods gets list of the projects that are made by manager
	 * @param manager		manager of the projects
	 * @return				list of projects
	 * @throws DAOException
	 */
	public List<Project> getByManager(Manager manager) throws DAOException;

	/**
	 * Method gets Project Work by its id
	 * @param id			id of the project work
	 * @return				instance of project work
	 * @throws DAOException
	 */
	public ProjectWork getWorkById(int id) throws DAOException;

	public int insert(Project project) throws DAOException;

	public void update(Project project) throws DAOException;

	public void deleteById(int id) throws DAOException;

}
