package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.dao.DAOException;

public interface TechTaskDAO {
	
	/**
	 * Gets techtask header(title, date, customer, etc..)
	 * @param id			id of the techtask
	 * @return				instance of techtask
	 * @throws DAOException
	 */
	public TechTask getHeaderById(int id) throws DAOException;

	/**
	 * Gets all techtasks that don't have projects yet
	 * @return 				list of techtasks
	 * @throws DAOException
	 */
	public List<TechTask> getUnprocessed() throws DAOException;
	
	/**
	 * Gets techtasks created by customer.
	 * @param customer		customer of the techtasks	
	 * @return				list of techtasks
	 * @throws DAOException
	 */
	public List<TechTask> getByCustomer(Customer customer) throws DAOException;
	
	/**
	 * Gets list of the work for the specified techtask
	 * @param techTask		techtask, work list is get for.
	 * @return				list of the work for techtask.
	 * @throws DAOException
	 */
	public List<TaskWork> getWorkList(TechTask techTask) throws DAOException;
	
	public int insert(TechTask techtask) throws DAOException;

	public void update(TechTask techtask) throws DAOException;
	
	public void deleteById(int id)throws DAOException;
	
}
