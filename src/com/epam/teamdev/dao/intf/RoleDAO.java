package com.epam.teamdev.dao.intf;

import com.epam.teamdev.bean.Role;
import com.epam.teamdev.dao.DAOException;

public interface RoleDAO {

	public Role getById(int id) throws DAOException;
	
	public int getId(Role role) throws DAOException;
	
}
