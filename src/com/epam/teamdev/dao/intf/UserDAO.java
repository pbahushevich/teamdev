package com.epam.teamdev.dao.intf;

import java.sql.Connection;

import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.DAOException;

public interface UserDAO {

	public User getById(int id) throws DAOException;
	
	public User getByLogin(String login) throws DAOException;
	
	public User getByLoginPass(String login, String pass) throws DAOException;
	
	public int insert(User user) throws DAOException;

	/**
	 * Inserts user in database during already opened transaction
	 * @param user			user, to insert into database
	 * @param connection	connection to database
	 * @return				id of the inserted entry
	 * @throws DAOException
	 */
	public int insertInTransaction(User user, Connection connection) throws DAOException;
	
	/**
	 * Deletes user from database during already opened transaction
	 * @param id			id of the deleted user 
	 * @param connection	connection to database
	 * @throws DAOException
	 */
	public void deleteByIdInTransaction(Integer id, Connection connection) throws DAOException;
}
