package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.dao.DAOException;

public interface InvoiceDAO {

	public Invoice getById(int id) throws DAOException;

	
	public Invoice getByProjectId(int id) throws DAOException;
	
	public List<Invoice> getAll() throws DAOException;

	/**
	 * Method gets unpaid invoiced for customer
	 * @param customer      customer
	 * @return				list of unpaid invoices
	 * @throws DAOException
	 */
	public List<Invoice> getUnpaid(Customer customer) throws DAOException;
	
	public int insert(Invoice invoice) throws DAOException;
	
	/**
	 * Method change paid flag for the invoice.
	 * @param invoiceId		invoice id
	 * @param paidFlag		paid flag
	 * @throws DAOException
	 */
	public void setPaidFlag(int invoiceId, boolean paidFlag) throws DAOException;
	
	public void update(Invoice invoice) throws DAOException;
	
	public void deleteById(int id) throws DAOException;

}
