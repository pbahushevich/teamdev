package com.epam.teamdev.dao.intf;

import java.util.List;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.dao.DAOException;

public interface WorkTypeDAO {
	
	public WorkType getById(int id) throws DAOException;

	public List<WorkType> getAll() throws DAOException;

	public int insert(WorkType workType) throws DAOException;
	
	public void deleteById(int id) throws DAOException;

}
