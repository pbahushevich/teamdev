package com.epam.teamdev.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.CommandFactory;
import com.epam.teamdev.command.TeamDevCommand;

/**
 * 
 * Main servlet of the application. Almost exclusive enter point for the application.
 *
 */
public final class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.gLogger");
	private static final String FORWARD_ERROR_MESSAGE = "Unable forward request to command result";

	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);

	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);

	}

	/**
	 * Method calls relevant commands for each request. Command name is taken from request parameter 
	 * and passed to CommandFactory.
	 * All exceptions are passed to exception handler. 
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logRequest(request);
		try {
			String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);

			TeamDevCommand command = CommandFactory.getInstance().getCommand(commandName);
			LOG.info(command);

			String page = command.execute(request);
			RequestDispatcher dispatcher = request.getRequestDispatcher(page);
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			} else {
				throw new CommandException(FORWARD_ERROR_MESSAGE);
			}

		} catch (Exception e) {
			ExceptionHandler.getInstance().handleException(request, response, e);
		}

	}

	/**
	 * method used to log all incoming requests with its attributes.
	 */
	private void logRequest(HttpServletRequest request) {

		String requestString = "Got new request";

		LOG.info(requestString + request.getRequestURI());
		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramName = params.nextElement();
			LOG.info(paramName + "  " + request.getParameter(paramName));
		}
	}

}