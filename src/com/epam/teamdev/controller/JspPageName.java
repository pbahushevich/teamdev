package com.epam.teamdev.controller;

/**
 * List of jsp pages
 */
public final class JspPageName {
	private JspPageName() {
	}

	public static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	public static final String LOGIN_PAGE = "index.jsp";
	public static final String MANAGER_WELCOME_PAGE = "/WEB-INF/jsp/manager.jsp";
	public static final String CUSTOMER_WELCOME_PAGE = "/WEB-INF/jsp/customer.jsp";
	public static final String DEVELOPER_WELCOME_PAGE = "/WEB-INF/jsp/developer.jsp";

	public static final String USER_PAGE = "/WEB-INF/jsp/user.jsp";
	public static final String NEW_USER_PAGE = "/WEB-INF/jsp/newuser.jsp";
	public static final String WORKTYPE_LIST_PAGE = "/WEB-INF/jsp/worktype.jsp";
	public static final String CUSTOMER_LIST_PAGE = "/WEB-INF/jsp/customerlist.jsp";
	public static final String NEW_CUSTOMER_PAGE = "/WEB-INF/jsp/newcustomer.jsp";
	public static final String TECHTASK_PAGE = "/WEB-INF/jsp/techtask.jsp";
	public static final String PROJECT_PAGE = "/WEB-INF/jsp/project.jsp";
	public static final String NEW_PROJECT_PAGE = "/WEB-INF/jsp/newproject.jsp";
	public static final String EDIT_PROJECT_PAGE = "/WEB-INF/jsp/editproject.jsp";
	public static final String DEVELOPER_LIST = "/WEB-INF/jsp/developerlist.jsp";
	public static final String AVAILABLE_DEVS_LIST = "/WEB-INF/jsp/devsavailable.jsp";
	public static final String INVOICE = "/WEB-INF/jsp/invoice.jsp";
	
}