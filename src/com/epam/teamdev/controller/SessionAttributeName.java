package com.epam.teamdev.controller;

/**
 * List of session attributes names.
 */
public class SessionAttributeName {

	public static final String USER = "user";
	public static final String LOCALE = "locale";
}
