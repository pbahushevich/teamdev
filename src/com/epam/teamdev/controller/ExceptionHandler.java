package com.epam.teamdev.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.command.AuthenticationException;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.pool.ConnectionPoolException;
import com.epam.teamdev.service.ServiceException;

/**
 * 
 * Class handles all exceptions that come from application.
 *
 */
public class ExceptionHandler {

	private static final String DAOEXCEPTION_MESSAGE = "Request couldn't be processed due to database restrictions.";

	private static final String CONNECTION_POON_EXCEPTION_MESSAGE = "Sorry but right now we are "
			+ "experiencing some technical problems. Try again later.";

	private static ExceptionHandler instance = new ExceptionHandler();

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.gLogger");

	public static ExceptionHandler getInstance() {
		return instance;
	}

	/**
	 * Method checks the type of exception and takes according decision. If
	 * exception is not fatal, user if forwarded to his start page, or login
	 * page if he is not logged in. Otherwise the error page is show.
	 * 
	 * @param request
	 *            http request sent by user
	 * @param response
	 *            http response that would be sent to user
	 * @param e
	 *            exception that occurred
	 * @throws ServletException
	 *             from RequestDispatcher forward method
	 * @throws IOException
	 *             from RequestDispatcher forward method
	 */
	public void handleException(HttpServletRequest request, HttpServletResponse response, Exception e)
			throws ServletException, IOException {

		LOG.error(e.getMessage(), e);
		String forwardAddress = "";
		if (e.getClass() == ConnectionPoolException.class) {
			forwardAddress = CommandUtil.getErrorPageWithMessage(request, CONNECTION_POON_EXCEPTION_MESSAGE);
		} else if (e instanceof AuthenticationException) {
			forwardAddress = CommandUtil.getLoginPageWithMessage(request, e.getMessage());
		} else if (e instanceof DAOException) {
			forwardAddress = CommandUtil.getWelcomePageWithMessage(request, DAOEXCEPTION_MESSAGE, true);
		} else if (e instanceof ServiceException || e instanceof CommandException) {
			forwardAddress = CommandUtil.getWelcomePageWithMessage(request, e.getMessage(), true);
		} else {
			forwardAddress = CommandUtil.getErrorPageWithMessage(request, e.getMessage());
		}
		request.getRequestDispatcher(forwardAddress).forward(request, response);

	}

}
