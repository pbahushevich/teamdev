package com.epam.teamdev.controller;

/**
 * 
 * List of request parameters or attributes that occur more that once in commands.
 *
 */
public final class RequestParameterName {

	private RequestParameterName() {
	}

	public static final String COMMAND_NAME = "command";
	public static final String TARGET_PAGE= "page";
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String LOCALE = "locale";
	public static final String ERROR_INFO= "errorMessage";
	public static final String USER_MESSAGE= "userMessage";
	public static final String ERROR_FLAG= "error";
	public static final String ID= "id";
	public static final String TECHTASK_LIST= "techTaskList";
	public static final String PROJECT_TASK_LIST= "projectTaskList";
	public static final String TECHTASK= "techTask";
	public static final String PROJECT= "project";
	public static final String INVOICE= "invoice";
	public static final String WORK_LIST= "workList";
	public static final String READ_ONLY= "readonly";
	public static final String QUALIFICATION_LIST= "qualificationList";
	public static final String WORKTYPE_LIST= "worktypeList";
	public static final String TASK_ID= "task_id";
	public static final String USER = "user";

}