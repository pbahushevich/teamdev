package com.epam.teamdev.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.epam.teamdev.dao.pool.ConnectionPool;

/**
 * Listener that initializes pool when application starts, and calls pool to
 * close all its connections when application is closed.
 * 
 */
public class DatabaseStartEndListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ConnectionPool.initPool();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ConnectionPool.getInstance().dispose();
	}

}
