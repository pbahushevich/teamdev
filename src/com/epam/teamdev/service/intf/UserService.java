package com.epam.teamdev.service.intf;

import java.util.Map;

import com.epam.teamdev.bean.User;
import com.epam.teamdev.service.ServiceException;

public interface UserService {

	public  User getById(int id) throws ServiceException;

	public  User getByLoginPass(String login, String password) throws ServiceException;

	/**
	 * Builds user from map of parameters. 
	 * Parameters should include:
	 * <ul>
	 * <li> name
	 * <li> role
	 * <li> telephone
	 * <li> email
	 * <li> password
	 * <li> login	
	 *  </ul>
	 * @param values  	map of values to build user
	 * @return				created user object filled with values from parameter map
	 */
	public  User buildUser(Map<String, String> values);

}
