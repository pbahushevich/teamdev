package com.epam.teamdev.service.intf;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.ProjectTaskInvoice;
import com.epam.teamdev.service.ServiceException;

public interface ProjectService {

	/**
	 * Gets relevant project for the techtask
	 * 
	 * @param id
	 *            id of the techtask
	 * @return instance of project
	 * @throws ServiceException
	 */
	public Project getByTechTaskId(int id) throws ServiceException;

	public Project getById(int id) throws ServiceException;

	/**
	 * Gets list of the work for the project
	 * 
	 * @param project
	 *            Project, work list is get for
	 * @return list of the work for the project
	 * @throws ServiceException
	 */
	public List<ProjectWork> getWorkList(Project project) throws ServiceException;

	/**
	 * Method that gets list of projects, techtasks and invoices for the
	 * Customer. Used to show gathered information on customer welcome page.
	 * 
	 * @param customer
	 *            customer information is getting for.
	 * @return
	 * @throws ServiceException
	 */
	public List<ProjectTaskInvoice> getProjectListForCustomer(Customer customer) throws ServiceException;

	/**
	 * Method that gets list of projects, techtasks and invoices for the
	 * Customer. Used to show gathered information on customer welcome page.
	 * 
	 * @param manager
	 *            manager of the projects
	 * @return list of projects
	 * @throws ServiceException
	 */
	public List<ProjectTaskInvoice> getProjectListForManager(Manager manager) throws ServiceException;

	public void createNew(Map<String, String[]> values) throws ServiceException;

	public void update(Map<String, String[]> values) throws ServiceException;

	public void deleteById(int id) throws ServiceException;

}
