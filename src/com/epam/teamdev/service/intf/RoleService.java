package com.epam.teamdev.service.intf;

import com.epam.teamdev.bean.Role;
import com.epam.teamdev.service.ServiceException;

public interface RoleService {

	public Role getById(int id) throws ServiceException ;
	public int  getId(Role role) throws ServiceException ;

}
