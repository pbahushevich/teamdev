package com.epam.teamdev.service.intf;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.service.ServiceException;

public interface InvoiceService {
	
	public Invoice getById(int id) throws ServiceException;

	/**
	 * Method gets unpaid invoices for customer
	 * @param customer      customer
	 * @return				list of unpaid invoices
	 * @throws ServiceException
	 */
	public  List<Invoice> getForCustomer(Customer customer) throws ServiceException ;
	
	public  Invoice getForProject(Project project) throws ServiceException ;

	public  void deleteById(int id) throws ServiceException ;
	
	public  void update(Map<String, String> values) throws ServiceException ;
	
	/**
	 * Method change paid flag for the invoice.
	 * @param invoiceId		invoice id
	 * @param paidFlag		paid flag
	 * @throws ServiceException
	 */
	public  void setPaidFlag(int invoiceId, boolean paidFlag) throws ServiceException ;
	
	public Invoice createNew(Map<String, String> values) throws ServiceException;
}
