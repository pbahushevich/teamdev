package com.epam.teamdev.service.intf;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.service.ServiceException;

public interface DeveloperService {

	public Developer getById(int id) throws ServiceException;

	/**
	 * Method that gets Developer after user was defined. For example after the
	 * login.
	 * 
	 * @param user
	 *            user that we search Developer for
	 * @return Developer of the specified user
	 * @throws ServiceException
	 */
	public Developer getByUser(User user) throws ServiceException;

	public List<Developer> getAll() throws ServiceException;

	public Developer createNew(Map<String, String> values) throws ServiceException;

	/**
	 * Method to get the list of developers that are not busy on any other work
	 * for specified period.
	 * 
	 * @param params
	 *            Map of parameters to get the available developer list should
	 *            include:
	 *            <ul>
	 *            <li>qualification qualification of the developers
	 *            <li>dateStart start of the period
	 *            <li>dateEnd end of the period
	 *            <li>projectId id of the project, that will be ignored during
	 *            search process, in case method used while editing existing
	 *            project. If list of the developers is specified for new
	 *            project parameter should be 0;
	 *            </ul>
	 * @return List of developers that are not busy on any project other than
	 *         specified in parameter projectId.
	 * @throws ServiceException
	 */
	public List<Developer> getAvailable(Map<String, String> params) throws ServiceException;

	/**
	 * Method for getting all the developers with their worklist. 
	 * In case developer has no work in any project, 
	 * return ProjectWork filled only with developer
	 * @return				List of Developers with their work.
	 * @throws ServiceException
	 */
	public List<ProjectWork> getDeveloperSchedule() throws ServiceException;

	/**
	 * Method gets actual work for the developer.
	 * @param id			id of the developer
	 * @return				list of project work, that developer is doing.
	 * @throws ServiceException 
	 */
	public List<ProjectWork> getDeveloperWorkList(int id) throws ServiceException;

	/**
	 * Add by developer time to project work.
	 * @param workId		- id of the project work
	 * @param timeSpent		- time developer has spent
	 * @throws ServiceException
	 */
	public void addWorkSpentTime(int workId, double timeSpent) throws ServiceException;

	public void deleteById(int id) throws ServiceException;

}
