package com.epam.teamdev.service.intf;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.service.ServiceException;

public interface ManagerService {

	public Manager getById(int id) throws ServiceException;

	/**
	 * Method that gets Manager by its user. Mostly used during login process.
	 * 
	 * @param user
	 *            relevant user of the manager
	 * @return instance of Manager
	 * @throws ServiceException
	 * 
	 */
	public Manager getByUser(User user) throws ServiceException;

	public List<Manager> getAll() throws ServiceException;

	public Manager createNew(Map<String, String> values) throws ServiceException;

	public void deleteById(int id) throws ServiceException;

}
