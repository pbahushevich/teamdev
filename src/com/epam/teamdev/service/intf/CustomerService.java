package com.epam.teamdev.service.intf;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.service.ServiceException;

public interface CustomerService {

	public Customer getById(int id) throws ServiceException;

	/**
	 * Method that gets Customer after user was defined. 
	 * For example after the login.
	 * @param user						user that we search Customer for
	 * @return								Customer of the specified user
	 * @throws ServiceException	
	 */
	public Customer getByUser(User user) throws ServiceException;

	public List<Customer> getAll() throws ServiceException;

	public Customer createNew(Map<String, String> values) throws ServiceException;

	public void deleteById(int id) throws ServiceException;

}
