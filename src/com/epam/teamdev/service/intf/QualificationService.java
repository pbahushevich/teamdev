package com.epam.teamdev.service.intf;

import java.util.List;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.service.ServiceException;

public interface QualificationService {

	public Qualification getById(int id) throws ServiceException;

	public  List<Qualification> getAll() throws ServiceException ;

	public  void add(String title) throws ServiceException ;

	public  void delete(int id) throws ServiceException;

}
