package com.epam.teamdev.service.intf;

import java.util.List;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.service.ServiceException;

public interface WorkTypeService {

	public WorkType getById(int id) throws ServiceException;

	public  List<WorkType> getAll() throws ServiceException ;
	
	public  void add(String title) throws ServiceException ;
	
	public  void delete(int id) throws ServiceException ;
	
}
