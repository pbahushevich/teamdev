package com.epam.teamdev.service.intf;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.service.ServiceException;

public interface TechTaskService {

	/**
	 * Gets all techtasks that don't have projects yet
	 * @return 				list of techtasks
	 * @throws ServiceException
	 */
	public List<TechTask> getUnprocessed() throws ServiceException;

	public TechTask getById(int id) throws ServiceException;

	public void createNew(Map<String, String[]> values) throws ServiceException;
	
	public void update(Map<String, String[]> values) throws ServiceException;
	
	public void deleteById(int id) throws ServiceException;

	/**
	 * Gets list of the work for the specified techtask
	 * @param techTask		techtask, work list is get for.
	 * @return				list of the work for techtask.
	 * @throws ServiceException
	 */
		public List<TaskWork> getWorkList(TechTask task) throws ServiceException;


}