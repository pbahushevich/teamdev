package com.epam.teamdev.service;

import com.epam.teamdev.service.impl.CustomerServiceImpl;
import com.epam.teamdev.service.impl.DeveloperServiceImpl;
import com.epam.teamdev.service.impl.InvoiceServiceImpl;
import com.epam.teamdev.service.impl.ManagerServiceImpl;
import com.epam.teamdev.service.impl.ProjectServiceImpl;
import com.epam.teamdev.service.impl.QualificationServiceImpl;
import com.epam.teamdev.service.impl.RoleServiceImpl;
import com.epam.teamdev.service.impl.TechTaskServiceImpl;
import com.epam.teamdev.service.impl.UserServiceImpl;
import com.epam.teamdev.service.impl.WorkTypeServiceImpl;
import com.epam.teamdev.service.intf.CustomerService;
import com.epam.teamdev.service.intf.DeveloperService;
import com.epam.teamdev.service.intf.InvoiceService;
import com.epam.teamdev.service.intf.ManagerService;
import com.epam.teamdev.service.intf.ProjectService;
import com.epam.teamdev.service.intf.QualificationService;
import com.epam.teamdev.service.intf.RoleService;
import com.epam.teamdev.service.intf.TechTaskService;
import com.epam.teamdev.service.intf.UserService;
import com.epam.teamdev.service.intf.WorkTypeService;

public class ServiceFactory {

	private static final ServiceFactory instance = new ServiceFactory();

	public static ServiceFactory getInstance() {
		return instance;
	}

	private ServiceFactory() {
	}

	public CustomerService getCustomerService() {
		return CustomerServiceImpl.getInstance();
	}

	public DeveloperService getDeveloperService() {
		return DeveloperServiceImpl.getInstance();
	}

	public InvoiceService getInvoiceService() {
		return InvoiceServiceImpl.getInstance();
	}

	public ManagerService getManagerService() {
		return ManagerServiceImpl.getInstance();
	}

	public ProjectService getProjectService() {
		return ProjectServiceImpl.getInstance();
	}

	public QualificationService getQualificationService() {
		return QualificationServiceImpl.getInstance();
	}

	public RoleService getRoleService() {
		return RoleServiceImpl.getInstance();
	}

	public TechTaskService getTechTaskService() {
		return TechTaskServiceImpl.getInstance();
	}

	public UserService getUserService() {
		return UserServiceImpl.getInstance();
	}

	public WorkTypeService getWorkTypeService() {
		return WorkTypeServiceImpl.getInstance();
	}

}
