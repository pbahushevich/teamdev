package com.epam.teamdev.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.Role;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.DeveloperDAO;
import com.epam.teamdev.dao.intf.QualificationDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.DeveloperService;

public class DeveloperServiceImpl implements DeveloperService {

	private static final String PROJECT_ID_PARAM = "projectId";

	private static final DeveloperService instance = new DeveloperServiceImpl();

	private static final String DATE_RANGE_PARAM = "dateRange";
	private static final String QUALIFICATION_ID_PARAM = "qualification_id";
	private static final String EMPTY_REQUIRED_FIELDS_STRING = "Some required fields are empty";

	private static final Pattern DATE_PATTERN = Pattern
			.compile("(?<dateStartGroup>([0-9]{2}\\.){2}[0-9]{4})( - )?(?<dateEndGroup>([0-9]{2}\\.){2}[0-9]{4})?");
	private static final String DATE_END_GR = "dateEndGroup";
	private static final String DATE_START_GROUP = "dateStartGroup";
	private static final String DATE_FORMAT = "dd.MM.yyyy";
	private static final String PASSWORD_PARAM = "password";
	private static final String LOGIN_PARAM = "login";
	private static final String TELEPHONE_PARAM = "telephone";
	private static final String EMAIL_PARAM = "email";
	private static final String NAME_PARAM = "name";

	public static DeveloperService getInstance() {
		return instance;
	}

	@Override
	public Developer getById(int id) throws ServiceException {
		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		Developer developer = dao.getById(id);
		return developer;
	}

	@Override
	public Developer getByUser(User user) throws ServiceException {

		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		Developer developer = dao.getByUser(user);
		return developer;
	}

	@Override
	public List<Developer> getAll() throws ServiceException {
		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		List<Developer> developers = dao.getAll();
		return developers;
	}

	@Override
	public List<Developer> getAvailable(Map<String, String> params) throws ServiceException {

		if (!checkParamsForGetAvailableRequest(params)) {
			throw new ServiceException("Can't parse params for developer list");
		}

		Qualification qualification = getQualificationFromParams(params);
		Date dateStart = new Date();
		Date dateEnd = new Date();

		int projectId = getProjectIdFromParams(params);
		fillDatesFromParams(params, dateStart, dateEnd);

		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		List<Developer> list = dao.getAvailable(qualification, dateStart, dateEnd, projectId);
		return list;
		
	}

	public List<ProjectWork> getDeveloperSchedule(){
		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		List<ProjectWork> list = dao.getDevelopersSchedule();
		return list;
	}
	
	@Override
	public Developer createNew(Map<String, String> values) throws ServiceException {

		if (!checkParamsForCreateNew(values)) {
			throw new ServiceException(EMPTY_REQUIRED_FIELDS_STRING);
		}

		Developer developer = buildDeveloper(values);
		int id = JDBCDAOFactory.getInstance().getDeveloperDAO().insert(developer);
		developer.setId(id);
		
		return developer;
		
	}

	@Override
	public void deleteById(int id) throws ServiceException {
		Developer developer = getById(id);

		if (developer == null) {
			throw new ServiceException("Unable to delete developer by id. There is no developer with id " + id);
		}
		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		dao.deleteById(id);
	}


	@Override
	public List<ProjectWork> getDeveloperWorkList(int id) throws ServiceException {

		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		return dao.getDeveloperWorkList(id);
	}

	@Override
	public void addWorkSpentTime(int workId, double timeSpent) throws ServiceException {
			
		DeveloperDAO dao = JDBCDAOFactory.getInstance().getDeveloperDAO();
		dao.addWorkSpentTime(workId, timeSpent);
	}


	private boolean checkParamsForCreateNew(Map<String, String> values) {

		if (values.get(QUALIFICATION_ID_PARAM) == null || "".equals(values.get(QUALIFICATION_ID_PARAM))) {
			return false;
		}
		if (values.get(NAME_PARAM) == null || "".equals(values.get(NAME_PARAM))) {
			return false;
		}
		if (values.get(TELEPHONE_PARAM) == null || "".equals(values.get(TELEPHONE_PARAM))) {
			return false;
		}
		if (values.get(EMAIL_PARAM) == null || "".equals(values.get(EMAIL_PARAM))) {
			return false;
		}
		if (values.get(LOGIN_PARAM) == null || "".equals(values.get(LOGIN_PARAM))) {
			return false;
		}
		if (values.get(PASSWORD_PARAM) == null || "".equals(values.get(PASSWORD_PARAM))) {
			return false;
		}
			return true;

	}

	private boolean checkParamsForGetAvailableRequest(Map<String, String> params) {

		if(!checkIntParam(params,QUALIFICATION_ID_PARAM)){
			return false;
		}
		if(!checkIntParam(params,PROJECT_ID_PARAM)){
			return false;
		}
		if(!checkStringParam(params,DATE_RANGE_PARAM)){
			return false;
		}
		return true;
	}

	private boolean checkIntParam(Map<String, String> params, String key) {
		String paramString = params.get(key);
		if (paramString == null || "".equals(paramString)) {
			return false;
		} else {
			try {
				Integer.valueOf(paramString);
			} catch (NumberFormatException exception) {
				return false;
			}
		}
		return true;

	}

	private boolean checkStringParam(Map<String, String> params, String key) {
		String paramString = params.get(key);
		if (paramString == null || "".equals(paramString)) {
			return false;
		}
		return true;
		
	}

	private Developer buildDeveloper(Map<String, String> values) throws ServiceException {

		Developer developer = new Developer();

		developer.setName(values.get(NAME_PARAM));
		developer.setEmail(values.get(EMAIL_PARAM));
		developer.setTelephone(values.get(TELEPHONE_PARAM));
		developer.setLogin(values.get(LOGIN_PARAM));
		developer.setPassword(values.get(PASSWORD_PARAM));
		developer.setRole(Role.DEVELOPER);

		int qId = Integer.valueOf(values.get(QUALIFICATION_ID_PARAM));
		QualificationDAO dao = JDBCDAOFactory.getInstance().getQualificationDAO();
		developer.setQualification(dao.getById(qId));

		return developer;
	}


	private Qualification getQualificationFromParams(Map<String, String> params) {
		int id = Integer.valueOf(params.get(QUALIFICATION_ID_PARAM));
		Qualification qualification = new Qualification();
		qualification.setId(id);
		return qualification;
	}

	private int getProjectIdFromParams(Map<String, String> params) {

		return Integer.valueOf(params.get(PROJECT_ID_PARAM));

	}

	/**
	 * Method fills start date and end date values from parameters that were sent to {@link #getAvailable(Map)} method.
	 * Dates are passed in parameters and are set inside this method.	
	 * @param params			map containing values of required parameters.
	 * @param dateStart		start date parameter that should be filled
	 * @param dateEnd			end date parameter that should be filled
	 * @throws ServiceException
	 */
	private void fillDatesFromParams(Map<String, String> params, Date dateStart, Date dateEnd) throws ServiceException {

		String dateString = params.get(DATE_RANGE_PARAM);

		if (dateString == null) {
			return;
		}
		Pattern pattern = DATE_PATTERN;
		Matcher matcher = pattern.matcher(dateString);

		while (matcher.find()) {

			String startString = (matcher.group(DATE_START_GROUP));
			String endString = (matcher.group(DATE_END_GR));
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			try {
				if (startString != null) {
					dateStart.setTime(formatter.parse(startString).getTime());
				}
				if (endString != null) {
					dateEnd.setTime(formatter.parse(endString).getTime());
				}
			} catch (ParseException e) {
				throw new ServiceException("Can't parse date values");
			}

		}
	}

}
