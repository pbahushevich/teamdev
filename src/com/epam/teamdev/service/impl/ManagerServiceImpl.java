package com.epam.teamdev.service.impl;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Role;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.ManagerDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.ManagerService;

public class ManagerServiceImpl implements ManagerService {

	private static final ManagerService instance = new ManagerServiceImpl();

	private static final String PASSWORD_PARAM = "password";
	private static final String LOGIN_PARAM = "login";
	private static final String TELEPHONE_PARAM = "telephone";
	private static final String EMAIL_PARAM = "email";
	private static final String NAME_PARAM = "name";
	private static final String DEPARTMENT_PARAM = "department";

	public  static  ManagerService getInstance() {
		return instance;
	}

	@Override
	public Manager getById(int id) throws ServiceException {
		ManagerDAO dao = JDBCDAOFactory.getInstance().getManagerDAO();
		Manager manager = dao.getById(id);
		return manager;

	}

	@Override
	public Manager getByUser(User user) throws ServiceException {

		ManagerDAO dao = JDBCDAOFactory.getInstance().getManagerDAO();
		Manager manager = dao.getByUser(user);
		return manager;
	}

	@Override
	public List<Manager> getAll() throws ServiceException {
		ManagerDAO dao = JDBCDAOFactory.getInstance().getManagerDAO();
		List<Manager> managers = dao.getAll();
		return managers;

	}

	@Override
	public Manager createNew(Map<String, String> values) throws ServiceException {

		Manager manager = buildManager(values);

		int id = JDBCDAOFactory.getInstance().getManagerDAO().insert(manager);
		manager.setId(id);
		return manager;
	}

	@Override
	public void deleteById(int id) throws ServiceException {
		ManagerDAO dao = JDBCDAOFactory.getInstance().getManagerDAO();
		dao.deleteById(id);

	}

	private boolean checkValues(Map<String, String> values) {

		if (values.get(DEPARTMENT_PARAM) == null || "".equals(values.get(DEPARTMENT_PARAM))) {
			return false;
		}
		if (values.get(NAME_PARAM) == null || "".equals(values.get(NAME_PARAM))) {
			return false;
		}
		if (values.get(TELEPHONE_PARAM) == null || "".equals(values.get(TELEPHONE_PARAM))) {
			return false;
		}
		if (values.get(EMAIL_PARAM) == null || "".equals(values.get(EMAIL_PARAM))) {
			return false;
		}
		if (values.get(LOGIN_PARAM) == null || "".equals(values.get(LOGIN_PARAM))) {
			return false;
		}
		if (values.get(PASSWORD_PARAM) == null || "".equals(values.get(PASSWORD_PARAM))) {
			return false;
		}
		return true;

	}

	private Manager buildManager(Map<String, String> values) {

		if (!checkValues(values)) {
			throw new IllegalArgumentException("Missing some required fields.");
		}

		Manager manager = new Manager();
		manager.setDepartment(values.get(DEPARTMENT_PARAM));
		manager.setName(values.get(NAME_PARAM));
		manager.setEmail(values.get(EMAIL_PARAM));
		manager.setTelephone(values.get(TELEPHONE_PARAM));
		manager.setLogin(values.get(LOGIN_PARAM));
		manager.setPassword(values.get(PASSWORD_PARAM));
		manager.setRole(Role.MANAGER);


		return manager;
	}

}
