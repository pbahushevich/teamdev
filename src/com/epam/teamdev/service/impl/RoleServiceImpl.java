package com.epam.teamdev.service.impl;

import com.epam.teamdev.bean.Role;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.RoleDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.RoleService;

public class RoleServiceImpl implements RoleService{


	private static final RoleService instance = new RoleServiceImpl();
	
	public static RoleService getInstance(){
		return instance;
	}

	@Override
	public Role getById(int id) throws ServiceException {

		RoleDAO roleDAO = JDBCDAOFactory.getInstance().getRoleDAO();
		return roleDAO.getById(id);
	}

	@Override
	public int getId(Role role) throws ServiceException {
		RoleDAO roleDAO = JDBCDAOFactory.getInstance().getRoleDAO();
		return roleDAO.getId(role);
	}

}
