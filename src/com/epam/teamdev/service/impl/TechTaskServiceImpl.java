package com.epam.teamdev.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.TechTaskDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.TechTaskService;

public class TechTaskServiceImpl implements TechTaskService {

	private static final String ID_PARAM = "id";

	private static final TechTaskService instance = new TechTaskServiceImpl();

	private static final String DATE_FORMAT = "dd.MM.yyyy";
	private static final String DEV_COUNT_PARAM = "devCount";
	private static final String WORK_TYPE_PARAM = "workType";
	private static final String QUALIFICATION_PARAM = "qualification";
	private static final String CUSTOMER_ID_PARAM = "customer_id";
	private static final String DATE_PARAM = "date";
	private static final String TITLE_PARAM = "title";
	private static final String ROW_COUNT_PARAM = "rowCount";

	public static TechTaskService getInstance() {
		return instance;
	}

	@Override
	public List<TechTask> getUnprocessed() throws ServiceException {

		TechTaskDAO dao = JDBCDAOFactory.getInstance().getTechTaskDAO();
		return dao.getUnprocessed();

	}

	@Override
	public TechTask getById(int id) throws ServiceException {

		TechTaskDAO dao = JDBCDAOFactory.getInstance().getTechTaskDAO();
		return dao.getHeaderById(id);

	}

	@Override
	public void createNew(Map<String, String[]> values) throws ServiceException {

		checkNewTaskParamValues(values);

		TechTask task = buildTask(values);

		TechTaskDAO taskDAO = JDBCDAOFactory.getInstance().getTechTaskDAO();

		int taskId = taskDAO.insert(task);
		task.setId(taskId);
		setId(task, values);

	}

	@Override
	public void update(Map<String, String[]> values) throws ServiceException {

		checkUpdateParamValues(values);

		TechTask task = buildTask(values);
		setId(task, values);

		TechTaskDAO taskDAO = JDBCDAOFactory.getInstance().getTechTaskDAO();
		taskDAO.update(task);

	}

	@Override
	public void deleteById(int id) throws ServiceException {

		TechTaskDAO dao = JDBCDAOFactory.getInstance().getTechTaskDAO();
		dao.deleteById(id);

	}

	@Override
	public List<TaskWork> getWorkList(TechTask task) throws ServiceException {

		TechTaskDAO taskDAO = JDBCDAOFactory.getInstance().getTechTaskDAO();
		return taskDAO.getWorkList(task);

	}

	private TechTask buildTask(Map<String, String[]> values) throws ServiceException {

		TechTask task = new TechTask();
		setTitle(task, values);
		setCustomer(task, values);
		setDate(task, values);
		setWorkList(task, values);

		return task;
	}

	private void setId(TechTask task, Map<String, String[]> values) throws ServiceException {
		String[] paramValues = values.get(ID_PARAM);
		if (paramValues != null) {
			try {
				int id = Integer.valueOf(paramValues[0]);
				task.setId(id);
			} catch (NumberFormatException e) {
				throw new ServiceException("Can't parse id parameter for saving edited task.");
			}
		}
	}

	private void setTitle(TechTask task, Map<String, String[]> values) {
		task.setTitle(values.get(TITLE_PARAM)[0]);
	}

	private void setCustomer(TechTask task, Map<String, String[]> values) {
		Customer customer = new Customer();
		customer.setId(Integer.valueOf(values.get(CUSTOMER_ID_PARAM)[0]));
		task.setCustomer(customer);
	}

	private void setDate(TechTask task, Map<String, String[]> values) throws ServiceException {
		try {

			DateFormat format = new SimpleDateFormat(DATE_FORMAT);
			Date date = format.parse(values.get(DATE_PARAM)[0]);
			task.setDate(date);

		} catch (ParseException e) {
			throw new ServiceException("Can't read date value", e);
		}
	}

	private void setWorkList(TechTask task, Map<String, String[]> values) {
		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);
		List<TaskWork> list = new ArrayList<>();

		for (int i = 0; i < rowCount; i++) {
			TaskWork work = task.new TaskWork();

			WorkType workType = new WorkType();
			workType.setId(Integer.valueOf(values.get(WORK_TYPE_PARAM)[i]));
			work.setWorkType(workType);

			Qualification qualification = new Qualification();
			qualification.setId(Integer.valueOf(values.get(QUALIFICATION_PARAM)[i]));
			work.setQualification(qualification);

			work.setDevCount(Integer.valueOf(values.get(DEV_COUNT_PARAM)[i]));

			list.add(work);
		}

		task.setWorkList(list);
	}

	private void checkNewTaskParamValues(Map<String, String[]> values) throws ServiceException {

		checkSimpleValue(values, ROW_COUNT_PARAM);
		checkSimpleValue(values, TITLE_PARAM);
		checkSimpleValue(values, DATE_PARAM);
		checkSimpleValue(values, CUSTOMER_ID_PARAM);

		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);

		checkTableDataParam(values, QUALIFICATION_PARAM, rowCount);
		checkTableDataParam(values, WORK_TYPE_PARAM, rowCount);
		checkTableDataParam(values, DEV_COUNT_PARAM, rowCount);

	}

	private void checkUpdateParamValues(Map<String, String[]> values) throws ServiceException {

		checkSimpleValue(values, ROW_COUNT_PARAM);
		checkSimpleValue(values, TITLE_PARAM);
		checkSimpleValue(values, DATE_PARAM);
		checkSimpleValue(values, CUSTOMER_ID_PARAM);
		checkSimpleValue(values, ID_PARAM);

		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);

		checkTableDataParam(values, QUALIFICATION_PARAM, rowCount);
		checkTableDataParam(values, WORK_TYPE_PARAM, rowCount);
		checkTableDataParam(values, DEV_COUNT_PARAM, rowCount);

	}

	/**
	 * Building techtask we face two kind of parameters: header parameters and
	 * table information about project work. This method checks parameter from
	 * the table. This parameter should fit three rules:
	 * <ul>
	 * <li>It should exist in map of values
	 * <li>It should not be empty
	 * <li>It should be presented in the same quantity as the row count of the
	 * project work list.
	 * </ul>
	 * <p>
	 * If parameter if further will be converted to integer value, then it's
	 * value also check for correct parsing to int.
	 * 
	 * @param values
	 *            map of parameter values.
	 * @param key
	 *            name of the parameter
	 * @param rowCount
	 *            size of the project work list
	 * @param shouldBeInt
	 *            whether value should be checked for correct parsing to int
	 *            value.
	 * @throws ServiceException
	 */
	private void checkTableDataParam(Map<String, String[]> values, String key, int rowCount) throws ServiceException {

		String[] paramValues = values.get(key);
		if (paramValues == null) {
			throw new ServiceException("Can't read parameter while saving techtask: " + key);
		}
		if (paramValues.length != rowCount) {
			throw new ServiceException("Row count and paramerter count doesn't match for: " + key);
		}
		for (int i = 0; i < paramValues.length; i++) {
			if ("".equals(paramValues[i])) {
				throw new ServiceException("Can't save techtask without " + key + " filled in every row");
			}
		}
	}

	/**
	 * Analog for method {@link #checkTableDataParam(Map, String, int, boolean)}
	 * , but for the header values.
	 * <p>
	 * Values of the project header should not be null or empty.
	 * 
	 * @param values
	 *            map of parameter values.
	 * @param key
	 *            name of the parameter
	 * @param shouldBeInt
	 *            whether value should be checked for correct parsing to int
	 *            value.
	 * @throws ServiceException
	 */
	private void checkSimpleValue(Map<String, String[]> values, String key) throws ServiceException {

		String[] paramValues = values.get(key);
		if (paramValues == null) {
			throw new ServiceException("Can't read parameter while saving techtask: " + key);
		}
		if (paramValues.length == 0) {
			throw new ServiceException("Can't read parameter while saving techtask: " + key);
		}
		String value = paramValues[0];
		if ("".equals(value)) {
			throw new ServiceException("Can't save techtask without " + key + " filled");
		}
	}

}
