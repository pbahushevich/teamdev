package com.epam.teamdev.service.impl;

import java.util.List;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.WorkTypeDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.WorkTypeService;

public class WorkTypeServiceImpl implements WorkTypeService {

	private static final WorkTypeService instance = new WorkTypeServiceImpl();

	public static WorkTypeService getInstance() {
		return instance;
	}

	@Override
	public WorkType getById(int id) throws ServiceException {
		WorkTypeDAO dao = JDBCDAOFactory.getInstance().getWorkTypeDAO();
		return dao.getById(id);
	}

	public List<WorkType> getAll() throws ServiceException {

		WorkTypeDAO dao = JDBCDAOFactory.getInstance().getWorkTypeDAO();
		List<WorkType> list = dao.getAll();
		return list;

	}

	public void add(String title) throws ServiceException {

		WorkType workType = new WorkType();
		workType.setTitle(title);
		WorkTypeDAO dao = JDBCDAOFactory.getInstance().getWorkTypeDAO();
		dao.insert(workType);

	}

	public void delete(int id) throws ServiceException {

		WorkTypeDAO dao = JDBCDAOFactory.getInstance().getWorkTypeDAO();

		WorkType workType = dao.getById(id);
		if (workType == null) {
			throw new ServiceException("Unable to delete workType. There is no workType with id" + id);
		}

		dao.deleteById(id);

	}

}
