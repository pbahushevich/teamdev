package com.epam.teamdev.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.InvoiceDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.InvoiceService;

public class InvoiceServiceImpl implements InvoiceService {

	private static final String ID_PARAM = "id";
	private static final String PROJECT_ID_PARAM = "projectId";
	private static final String DATE_FORMAT = "dd.MM.yyyy";
	private static final String DATE_PARAM = "date";
	private static final String NUMBER_PARAM = "number";
	private static final String PRICE_PARAM = "price";
	private static final InvoiceService instance = new InvoiceServiceImpl();

	public static InvoiceService getInstance() {
		return instance;
	}

	@Override
	public Invoice getById(int id) throws ServiceException {
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		Invoice invoice = dao.getById(id);
		return invoice;

	}

	@Override
	public List<Invoice> getForCustomer(Customer customer) throws ServiceException {
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		List<Invoice> list = dao.getUnpaid(customer);
		return list;

	}

	@Override
	public Invoice getForProject(Project project) throws ServiceException {
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		Invoice invoice = dao.getByProjectId(project.getId());
		return invoice;
	}

	@Override
	public void deleteById(int id) throws ServiceException {
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		dao.deleteById(id);
	}

	@Override
	public Invoice createNew(Map<String, String> values) throws ServiceException {

		checkNewParams(values);
		Invoice invoice = buildNewInvoice(values);
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		int id = dao.insert(invoice);
		invoice.setId(id);
		return invoice;
	}

	@Override
	public void update(Map<String, String> values) throws ServiceException {

		checkUpdateParams(values);
		Invoice invoice = buildInvoiceForUpdate(values);
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		dao.update(invoice);

	}

	@Override
	public void setPaidFlag(int invoiceId, boolean paidFlag) throws ServiceException {
		InvoiceDAO dao = JDBCDAOFactory.getInstance().getInvoiceDAO();
		dao.setPaidFlag(invoiceId, paidFlag);

	}

	/**
	 * Method builds new Invoice object from map of parameters that comes from
	 * user request
	 * 
	 * @param values
	 *            map of parameter
	 * @return new created invoice
	 */
	private Invoice buildNewInvoice(Map<String, String> values) {

		Invoice invoice = new Invoice();

		invoice.setPrice(Double.valueOf(values.get(PRICE_PARAM)));
		invoice.setNumber(values.get(NUMBER_PARAM));

		String date = values.get(DATE_PARAM);
		DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		try {
			invoice.setDate(formatter.parse(date));
		} catch (Exception e) {
			throw new ServiceException("Error creating new invoice. Error after correct date parsing.");
		}

		Project project = new Project();
		project.setId(Integer.valueOf(values.get(PROJECT_ID_PARAM)));
		invoice.setProject(project);

		return invoice;
	}

	/**
	 * Method builds Invoice object from map of parameters to update existing Invoice.
	 * As long as Invoice already exist, in this method we need also and id of existing Invoice.
	 * @param values
	 *            map of parameter
	 * @return created invoice
	 */
	private Invoice buildInvoiceForUpdate(Map<String, String> values) {

		Invoice invoice = new Invoice();

		invoice.setId(Integer.valueOf(values.get(ID_PARAM)));
		invoice.setPrice(Double.valueOf(values.get(PRICE_PARAM)));
		invoice.setNumber(values.get(NUMBER_PARAM));

		String date = values.get(DATE_PARAM);
		DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		try {
			invoice.setDate(formatter.parse(date));
		} catch (Exception e) {
			throw new ServiceException("Error creating new invoice. Error after correct date parsing.");
		}

		Project project = new Project();
		project.setId(Integer.valueOf(values.get(PROJECT_ID_PARAM)));
		invoice.setProject(project);

		return invoice;
	}

	private void checkNewParams(Map<String, String> values) {
		checkPrice(values);
		checkNumber(values);
		checkDate(values);
		checkProjectId(values);

	}

	private void checkUpdateParams(Map<String, String> values) {

		checkId(values);
		checkPrice(values);
		checkNumber(values);
		checkDate(values);
		checkProjectId(values);

	}

	private void checkId(Map<String, String> values) {
		try {
			int id = Integer.valueOf(values.get(ID_PARAM));
			if (id == 0) {
				throw new ServiceException("Error updating invoice. Id can't be 0.");
			}
		} catch (NumberFormatException e) {
			throw new ServiceException("Error updating new invoice. Can't parse invoice id.");
		}
	}

	private void checkDate(Map<String, String> values) {
		try {
			String date = values.get(DATE_PARAM);
			if (date == null || "".equals(date)) {
				throw new ServiceException("Error creating new invoice. Date can't be empty");
			}
			DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			formatter.parse(date);
		} catch (ParseException e) {
			throw new ServiceException("Error creating new invoice. Can't parse date value.");
		}
	}

	private void checkNumber(Map<String, String> values) {
		String number = values.get(NUMBER_PARAM);
		if (number == null || "".equals(number)) {
			throw new ServiceException("Error creating new invoice. Number can't be empty.");
		}
	}

	private void checkPrice(Map<String, String> values) {
		try {
			double price = Double.valueOf(values.get(PRICE_PARAM));
			if (price == 0) {
				throw new ServiceException("Error creating new invoice. Price can't be 0.");
			}
		} catch (NumberFormatException e) {
			throw new ServiceException("Error creating new invoice. Can't parse invoice price.");
		}
	}

	private void checkProjectId(Map<String, String> values) {
		try {
			int projectId = Integer.valueOf(values.get(PROJECT_ID_PARAM));
			if (projectId == 0) {
				throw new ServiceException("Error creating new invoice. Illegal project id.");
			}
		} catch (NumberFormatException e) {
			throw new ServiceException("Error creating new invoice. Can't parse invoice project.");
		}
	}

}
