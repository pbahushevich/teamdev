package com.epam.teamdev.service.impl;

import java.util.List;
import java.util.Map;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Role;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.CustomerDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.CustomerService;

public class CustomerServiceImpl implements CustomerService {

	private static final String COMPANY_PARAM = "company";
	private static final String PASSWORD_PARAM = "password";
	private static final String LOGIN_PARAM = "login";
	private static final String TELEPHONE_PARAM = "telephone";
	private static final String EMAIL_PARAM = "email";
	private static final String NAME_PARAM = "name";

	private static final CustomerService instance = new CustomerServiceImpl();

	public static CustomerService getInstance() {
		return instance;
	}

	@Override
	public Customer getById(int id) throws ServiceException {

		CustomerDAO dao = JDBCDAOFactory.getInstance().getCustomerDAO();
		Customer customer = dao.getById(id);
		return customer;

	}

	@Override
	public Customer getByUser(User user) throws ServiceException {

		CustomerDAO dao = JDBCDAOFactory.getInstance().getCustomerDAO();
		Customer customer = dao.getByUser(user);
		return customer;
	}

	@Override
	public List<Customer> getAll() throws ServiceException {

		CustomerDAO dao = JDBCDAOFactory.getInstance().getCustomerDAO();
		List<Customer> customers = dao.getAll();
		return customers;

	}

	@Override
	public Customer createNew(Map<String, String> values) throws ServiceException {

		if (!checkValues(values)) {
			throw new ServiceException("Some required fields are empty");
		}

		Customer customer = buildCustomer(values);
		int id = JDBCDAOFactory.getInstance().getCustomerDAO().insert(customer);
		customer.setId(id);

		return customer;

	}

	@Override
	public void deleteById(int id) throws ServiceException {

		CustomerDAO dao = JDBCDAOFactory.getInstance().getCustomerDAO();
		Customer customer = getById(id);

		if (customer == null) {
			throw new ServiceException("Unable to delete customer by id. There is no customer with id " + id);
		}

		dao.deleteById(id);
	}

	/**
	 * Method checks presence of all required fields
	 * 
	 * @param values
	 *            map of values to check
	 * @return <code>true</code> if map contains all the necessary values and
	 *         they are not empty.
	 */
	private boolean checkValues(Map<String, String> values) {

		if (values.get(COMPANY_PARAM) == null || "".equals(values.get(COMPANY_PARAM))) {
			return false;
		}
		if (values.get(NAME_PARAM) == null || "".equals(values.get(NAME_PARAM))) {
			return false;
		}
		if (values.get(TELEPHONE_PARAM) == null || "".equals(values.get(TELEPHONE_PARAM))) {
			return false;
		}
		if (values.get(EMAIL_PARAM) == null || "".equals(values.get(EMAIL_PARAM))) {
			return false;
		}
		if (values.get(LOGIN_PARAM) == null || "".equals(values.get(LOGIN_PARAM))) {
			return false;
		}
		if (values.get(PASSWORD_PARAM) == null || "".equals(values.get(PASSWORD_PARAM))) {
			return false;
		}
		return true;

	}

	/**
	 * Method builds customer from map, that contains all the necessary fields
	 * for it.
	 * 
	 * @param values
	 *            map containing values to fill Customer object fields
	 * @return
	 */
	private Customer buildCustomer(Map<String, String> values) {

		Customer customer = new Customer();
		customer.setName(values.get(NAME_PARAM));
		customer.setEmail(values.get(EMAIL_PARAM));
		customer.setTelephone(values.get(TELEPHONE_PARAM));
		customer.setLogin(values.get(LOGIN_PARAM));
		customer.setPassword(values.get(PASSWORD_PARAM));
		customer.setRole(Role.CUSTOMER);
		customer.setCompany(values.get(COMPANY_PARAM));

		return customer;
	}

}
