package com.epam.teamdev.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.ProjectTaskInvoice;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.InvoiceDAO;
import com.epam.teamdev.dao.intf.ProjectDAO;
import com.epam.teamdev.dao.intf.TechTaskDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.ProjectService;

public class ProjectServiceImpl implements ProjectService {

	private static final ProjectService instance = new ProjectServiceImpl();

	private static final String TASK_ID_PARAM = "task_id";
	private static final String DATE_RANGE_PARAM = "projectStartEnd";
	private static final String WORK_DATES_PARAM = "workStartEnd";
	private static final String DATE_PARAM = "projectDate";

	private static final String DATE_FORMAT = "dd.MM.yyyy";
	private static final Pattern DATE_PATTERN = Pattern
			.compile("(?<dateStartGroup>([0-9]{2}\\.){2}[0-9]{4})( - )?(?<dateEndGroup>([0-9]{2}\\.){2}[0-9]{4})?");
	private static final String DATE_END_GR = "dateEndGroup";
	private static final String DATE_START_GROUP = "dateStartGroup";

	private static final String TASKWORK_ID_PARAM = "taskWorkId";
	private static final String MANAGER_ID_PARAM = "manager_id";
	private static final String DEVELOPER_ID_PARAM = "developer_id";
	private static final String TITLE_PARAM = "title";
	private static final String ROW_COUNT_PARAM = "rowCount";
	private static final String PROJECT_WORK_ID_PARAM = "projectWorkId";
	private static final String TIME_SPENT_PARAM = "timeSpent";
	private static final String PROJECT_ID_PARAM = "project_id";

	public static ProjectService getInstance() {
		return instance;
	}

	@Override
	public Project getById(int id) throws ServiceException {

		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		return dao.getHeaderById(id);
	}

	@Override
	public Project getByTechTaskId(int id) throws ServiceException {
		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		return dao.getByTechTaskId(id);
	}

	@Override
	public List<ProjectWork> getWorkList(Project project) throws ServiceException {

		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		return dao.getWorkList(project);
	}

	@Override
	public void createNew(Map<String, String[]> values) throws ServiceException {

		checkNewProjectParamValues(values);
		Project project = buildNewProject(values);
		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		dao.insert(project);

	}

	@Override
	public void update(Map<String, String[]> values) throws ServiceException {

		checkUpdateProjectParamValues(values);
		Project project = buildUpdateProject(values);

		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		dao.update(project);

	}

	@Override
	public void deleteById(int id) throws ServiceException {

		ProjectDAO dao = JDBCDAOFactory.getInstance().getProjectDAO();
		if (dao.getHeaderById(id) == null) {
			throw new ServiceException("Error deleting project. There is no project with such id");
		}
		dao.deleteById(id);
	}

	@Override
	public List<ProjectTaskInvoice> getProjectListForCustomer(Customer customer) throws ServiceException {

		try {
			TechTaskDAO taskDAO = JDBCDAOFactory.getInstance().getTechTaskDAO();
			ProjectDAO projectDAO = JDBCDAOFactory.getInstance().getProjectDAO();

			List<TechTask> tasks = taskDAO.getByCustomer(customer);
			List<ProjectTaskInvoice> result = new ArrayList<>();

			for (TechTask techTask : tasks) {
				Project project = projectDAO.getByTechTaskId(techTask.getId());

				ProjectTaskInvoice vo = buildFromProjectTask(techTask, project);
				result.add(vo);
			}
			return result;
		} catch (DAOException e) {
			throw new ServiceException("Unable to get list of tasks for customer");
		}
	}

	public List<ProjectTaskInvoice> getProjectListForManager(Manager manager) throws ServiceException {
		try {
			TechTaskDAO taskDAO = JDBCDAOFactory.getInstance().getTechTaskDAO();
			ProjectDAO projectDAO = JDBCDAOFactory.getInstance().getProjectDAO();

			List<Project> projects = projectDAO.getByManager(manager);
			List<ProjectTaskInvoice> result = new ArrayList<>();

			for (Project project : projects) {

				TechTask techTask = taskDAO.getHeaderById(project.getTechTask().getId());

				ProjectTaskInvoice vo = buildFromProjectTask(techTask, project);

				result.add(vo);
			}
			return result;
		} catch (DAOException e) {
			throw new ServiceException("Unable to get list of tasks for manager");
		}

	}

	/**
	 * Method creates ProjectTaskInvoice bean from project and techtask objects.
	 * If there is invoice in database its is also added to the bean.
	 * 
	 * @param techTask
	 *            techtask added to the bean
	 * @param project
	 *            project added to the bean
	 * @return ProjectTaskInvoice bean
	 * @throws ServiceException
	 */
	private ProjectTaskInvoice buildFromProjectTask(TechTask techTask, Project project) throws ServiceException {
		ProjectTaskInvoice vo = new ProjectTaskInvoice();
		InvoiceDAO invoiceDAO = JDBCDAOFactory.getInstance().getInvoiceDAO();

		vo.setTechTask(techTask);
		vo.setCustomer(techTask.getCustomer());
		vo.setProject(project);
		if (project != null) {

			Invoice invoice = invoiceDAO.getByProjectId(project.getId());
			vo.setDateStart(project.getDateStart());
			vo.setDateEnd(project.getDateEnd());
			vo.setInvoice(invoice);
			vo.setManager(project.getManager());
		}
		return vo;
	}

	private void checkNewProjectParamValues(Map<String, String[]> values) throws ServiceException {

		checkSimpleValue(values, ROW_COUNT_PARAM, true);
		checkSimpleValue(values, TASK_ID_PARAM, true);
		checkSimpleValue(values, TITLE_PARAM, false);
		checkSimpleValue(values, DATE_PARAM, false);
		checkSimpleValue(values, MANAGER_ID_PARAM, true);

		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);

		checkTableDataParam(values, TASKWORK_ID_PARAM, rowCount, true);
		checkTableDataParam(values, WORK_DATES_PARAM, rowCount, false);
		checkTableDataParam(values, DEVELOPER_ID_PARAM, rowCount, true);

	}

	private void checkUpdateProjectParamValues(Map<String, String[]> values) throws ServiceException {

		checkSimpleValue(values, PROJECT_ID_PARAM, true);
		checkSimpleValue(values, TASK_ID_PARAM, true);
		checkSimpleValue(values, ROW_COUNT_PARAM, true);
		checkSimpleValue(values, MANAGER_ID_PARAM, true);
		checkSimpleValue(values, TITLE_PARAM, false);
		checkSimpleValue(values, DATE_PARAM, false);

		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);

		checkTableDataParam(values, TASKWORK_ID_PARAM, rowCount, true);
		checkTableDataParam(values, DEVELOPER_ID_PARAM, rowCount, true);
		checkTableDataParam(values, WORK_DATES_PARAM, rowCount, false);
		checkTimeSpentDoubleValues(values);
	}

	private void checkTimeSpentDoubleValues(Map<String, String[]> values) {

		String[] paramValues = values.get(TIME_SPENT_PARAM);
		for (int i = 0; i < paramValues.length; i++) {
			try {
				Double.valueOf(paramValues[i]);
			} catch (NumberFormatException e) {
				throw new ServiceException("Invalid value for parameter: timeSpent");
			}
		}

	}

	/**
	 * Method builds Project for creating new project.
	 * 
	 * @param values
	 *            map of parameter
	 * @return created Project
	 * @throws ServiceException
	 */
	private Project buildNewProject(Map<String, String[]> values) throws ServiceException {

		Project project = new Project();
		setTechTask(project, values);
		setTitle(project, values);
		setProjectDates(project, values);
		setManager(project, values);
		setNewWorkList(project, values);
		return project;
	}

	/**
	 * Method builds Project for updating existing project.
	 * 
	 * @param values
	 *            map of parameter
	 * @return created Project
	 * @throws ServiceException
	 */
	private Project buildUpdateProject(Map<String, String[]> values) throws ServiceException {

		Project project = new Project();
		setId(project, values);
		setTechTask(project, values);
		setTitle(project, values);
		setProjectDates(project, values);
		setManager(project, values);
		setUpdateWorkList(project, values);
		return project;
	}

	private void setId(Project project, Map<String, String[]> values) {
		if (values.get(PROJECT_ID_PARAM) != null) {
			project.setId(Integer.valueOf(values.get(PROJECT_ID_PARAM)[0]));
		}
	}

	private void setTechTask(Project project, Map<String, String[]> values) {

		try {
			TechTask task = new TechTask();
			task.setId(Integer.valueOf(values.get(TASK_ID_PARAM)[0]));
			project.setTechTask(task);
		} catch (NumberFormatException e) {
			throw new ServiceException("Error parsing task id.");
		}
	}

	private void setTitle(Project project, Map<String, String[]> values) {
		project.setTitle(values.get(TITLE_PARAM)[0]);
	}

	private void setManager(Project project, Map<String, String[]> values) {
		Manager manager = new Manager();
		manager.setId(Integer.valueOf(values.get(MANAGER_ID_PARAM)[0]));
		project.setManager(manager);
	}

	private void setProjectDates(Project project, Map<String, String[]> values) throws ServiceException {

		try {

			DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			project.setDate(formatter.parse(values.get(DATE_PARAM)[0]));

			Matcher matcher = DATE_PATTERN.matcher(values.get(DATE_RANGE_PARAM)[0]);

			if (matcher.find()) {
				String startString = matcher.group(DATE_START_GROUP);
				String endString = matcher.group(DATE_END_GR);
				project.setDateStart(formatter.parse(startString));
				project.setDateEnd(formatter.parse(endString));
			} else
				throw new ServiceException("Error while parsing dates for project.");
		} catch (ParseException e) {
			throw new ServiceException("Error while parsing dates for project.");
		}
	}

	private void setNewWorkList(Project project, Map<String, String[]> values) {
		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);
		List<ProjectWork> list = new ArrayList<>();

		for (int i = 0; i < rowCount; i++) {

			ProjectWork projectWork = project.new ProjectWork();
			TaskWork taskWork = project.getTechTask().new TaskWork();

			taskWork.setId(Integer.valueOf(values.get(TASKWORK_ID_PARAM)[i]));
			projectWork.setTaskWork(taskWork);

			setWorkDates(projectWork, values, i);

			Developer developer = new Developer();
			developer.setId(Integer.valueOf(values.get(DEVELOPER_ID_PARAM)[i]));
			projectWork.setDeveloper(developer);

			list.add(projectWork);
		}

		project.setWorkList(list);
	}

	private void setUpdateWorkList(Project project, Map<String, String[]> values) {
		int rowCount = Integer.valueOf(values.get(ROW_COUNT_PARAM)[0]);
		List<ProjectWork> list = new ArrayList<>();

		for (int i = 0; i < rowCount; i++) {

			ProjectWork projectWork = project.new ProjectWork();
			TaskWork taskWork = project.getTechTask().new TaskWork();

			taskWork.setId(Integer.valueOf(values.get(TASKWORK_ID_PARAM)[i]));
			projectWork.setTaskWork(taskWork);

			setWorkDates(projectWork, values, i);

			Developer developer = new Developer();
			developer.setId(Integer.valueOf(values.get(DEVELOPER_ID_PARAM)[i]));
			projectWork.setDeveloper(developer);

			projectWork.setTimeSpent(Double.parseDouble(values.get(TIME_SPENT_PARAM)[i]));
			projectWork.setId(Integer.parseInt(values.get(PROJECT_WORK_ID_PARAM)[i]));

			list.add(projectWork);
		}

		project.setWorkList(list);
	}

	private void setWorkDates(ProjectWork projectWork, Map<String, String[]> values, int i) throws ServiceException {

		try {

			DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			Matcher matcher = DATE_PATTERN.matcher(values.get(WORK_DATES_PARAM)[i]);

			if (matcher.find()) {
				String startString = matcher.group(DATE_START_GROUP);
				String endString = matcher.group(DATE_END_GR);
				projectWork.setDateStart(formatter.parse(startString));
				projectWork.setDateEnd(formatter.parse(endString));
			} else
				throw new ServiceException("Error while parsing dates for project.");
		} catch (ParseException e) {
			throw new ServiceException("Error while parsing dates for project.");
		}
	}

	/**
	 * Building project we face two kind of parameters: header parameters and
	 * table information about project work. This method checks parameter from
	 * the table. This parameter should fit three rules:
	 * <ul>
	 * <li>It should exist in map of values
	 * <li>It should not be empty
	 * <li>It should be presented in the same quantity as the row count of the
	 * project work list.
	 * </ul>
	 * <p>
	 * If parameter if further will be converted to integer value, then it's
	 * value also check for correct parsing to int.
	 * 
	 * @param values
	 *            map of parameter values.
	 * @param key
	 *            name of the parameter
	 * @param rowCount
	 *            size of the project work list
	 * @param shouldBeInt
	 *            whether value should be checked for correct parsing to int
	 *            value.
	 * @throws ServiceException
	 */
	private void checkTableDataParam(Map<String, String[]> values, String key, int rowCount, boolean shouldBeInt)
			throws ServiceException {

		String[] paramValues = values.get(key);
		if (paramValues == null) {
			throw new ServiceException("Can't read parameter while saving project: " + key);
		}
		if (paramValues.length != rowCount) {
			throw new ServiceException("Row count and paramerter count doesn't match for: " + key);
		}
		for (int i = 0; i < paramValues.length; i++) {
			if ("".equals(paramValues[i])) {
				throw new ServiceException("Can't save project without " + key + " filled in every row");
			}
			if (shouldBeInt) {
				try {
					Integer.valueOf(paramValues[i]);
				} catch (NumberFormatException e) {
					throw new ServiceException("Invalid value for parameter: " + key);
				}
			}
		}
	}

	/**
	 * Analog for method {@link #checkTableDataParam(Map, String, int, boolean)}
	 * , but for the header values.
	 * <p>
	 * Values of the project header should not be null or empty.
	 * 
	 * @param values
	 *            map of parameter values.
	 * @param key
	 *            name of the parameter
	 * @param shouldBeInt
	 *            whether value should be checked for correct parsing to int
	 *            value.
	 * @throws ServiceException
	 */
	private void checkSimpleValue(Map<String, String[]> values, String key, boolean shouldBeInt)
			throws ServiceException {

		String[] paramValues = values.get(key);
		if (paramValues == null) {
			throw new ServiceException("Can't read parameter while saving project: " + key);
		}
		if (paramValues.length == 0) {
			throw new ServiceException("Can't read parameter while saving project: " + key);
		}
		String value = paramValues[0];
		if ("".equals(value)) {
			throw new ServiceException("Can't save project without " + key + " filled");
		}
		if (shouldBeInt) {
			try {
				Integer.valueOf(value);
			} catch (NumberFormatException e) {
				throw new ServiceException("Invalid value for parameter: " + key);
			}
		}
	}

}
