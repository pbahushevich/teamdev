package com.epam.teamdev.service.impl;

import java.util.List;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.QualificationDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.QualificationService;

public class QualificationServiceImpl implements QualificationService {

	private static final QualificationService instance = new QualificationServiceImpl();

	public static QualificationService getInstance() {
		return instance;
	}

	@Override
	public Qualification getById(int id) throws ServiceException {
		QualificationDAO dao = JDBCDAOFactory.getInstance().getQualificationDAO();
		return dao.getById(id);
	}

	@Override
	public List<Qualification> getAll() throws ServiceException {

		QualificationDAO dao = JDBCDAOFactory.getInstance().getQualificationDAO();
		List<Qualification> list = dao.getAll();

		return list;

	}

	@Override
	public void add(String title) throws ServiceException {

		Qualification qualification = new Qualification();
		qualification.setTitle(title);
		QualificationDAO dao;
		dao = JDBCDAOFactory.getInstance().getQualificationDAO();
		dao.insert(qualification);

	}

	@Override
	public void delete(int id) throws ServiceException {

		QualificationDAO dao = JDBCDAOFactory.getInstance().getQualificationDAO();
		Qualification qualification = dao.getById(id);
		if (qualification == null) {
			throw new ServiceException("Unable to delete qualification. There is no qualification with id" + id);
		}
		dao.deleteById(id);

	}

}
