package com.epam.teamdev.service.impl;

import java.util.Map;

import com.epam.teamdev.bean.Role;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.dao.JDBCDAOFactory;
import com.epam.teamdev.dao.intf.UserDAO;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.intf.UserService;

public class UserServiceImpl implements UserService {

	private static final UserService instance = new UserServiceImpl();

	private static final String ROLE_PARAM = "role";
	private static final String PASSWORD_PARAM = "password";
	private static final String LOGIN_PARAM = "login";
	private static final String TELEPHONE_PARAM = "telephone";
	private static final String EMAIL_PARAM = "email";
	private static final String NAME_PARAM = "name";

	public static UserService getInstance() {
		return instance;
	}

	@Override
	public User getById(int id) throws ServiceException {

		UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();
		User user = userDAO.getById(id);
		return user;

	}

	@Override
	public User getByLoginPass(String login, String password) throws ServiceException {

		UserDAO userDAO = JDBCDAOFactory.getInstance().getUserDAO();
		User user = userDAO.getByLoginPass(login, password);

		return user;

	}

	@Override
	public User buildUser(Map<String, String> values) {

		if (!checkValues(values)) {
			throw new IllegalArgumentException();
		}

		User user = new User();
		user.setName(values.get(NAME_PARAM));
		user.setEmail(values.get(EMAIL_PARAM));
		user.setTelephone(values.get(TELEPHONE_PARAM));
		user.setLogin(values.get(LOGIN_PARAM));
		user.setPassword(values.get(PASSWORD_PARAM));
		user.setPassword(values.get(PASSWORD_PARAM));
		Role role = Role.valueOf(values.get(ROLE_PARAM));
		user.setRole(role);
		return user;
	}

	private boolean checkValues(Map<String, String> values) {
		if (values.get(NAME_PARAM) == null || "".equals(values.get(NAME_PARAM))) {
			return false;
		}
		if (values.get(TELEPHONE_PARAM) == null || "".equals(values.get(TELEPHONE_PARAM))) {
			return false;
		}
		if (values.get(EMAIL_PARAM) == null || "".equals(values.get(EMAIL_PARAM))) {
			return false;
		}
		if (values.get(LOGIN_PARAM) == null || "".equals(values.get(LOGIN_PARAM))) {
			return false;
		}
		if (values.get(PASSWORD_PARAM) == null || "".equals(values.get(PASSWORD_PARAM))) {
			return false;
		}
		if (values.get(ROLE_PARAM) == null || "".equals(values.get(ROLE_PARAM))) {
			return false;
		}
		return true;

	}

}
