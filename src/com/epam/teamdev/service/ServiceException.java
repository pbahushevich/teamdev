package com.epam.teamdev.service;

public class ServiceException extends RuntimeException {


	private static final long serialVersionUID = -570895812492317749L;

	public ServiceException(String message, Exception cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	
}
