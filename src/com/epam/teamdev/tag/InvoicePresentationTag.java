package com.epam.teamdev.tag;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.teamdev.bean.Invoice;

/**
 * Tag used to present invoice in readable view. Also this
 * presentation contains link on the page of invoice itself.
 */
public class InvoicePresentationTag extends TagSupport {

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.gLogger");
	private Invoice invoice;

	private static final long serialVersionUID = 1L;

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@Override
	public int doStartTag() throws JspException {

		if (invoice == null) {
			return SKIP_BODY;
		}

		JspWriter out = pageContext.getOut();
		String tagText = getTagText();
		try {
			out.append(tagText);
		} catch (IOException e) {
			LOG.error("Can't get invoice presentation", e);
		}
		return SKIP_BODY;
	}

	private String getTagText() {
		String presentation = getNumberDatePresentation();
		String formTagStart = "<form action=\"/teamdev/controller\" method=\"POST\">";
		String formTagEnd = "</form>";
		String commandInput = " <input type=\"hidden\" name=\"command\" value=\"show_invoice\">";
		String idInputStart = " <input type=\"hidden\" name=\"id\" value=\"";
		String idInputEnd = "\">";
		String hrefTagStart = "<a href=\"#";
		String hrefAttributes = "\" onclick=\"$(this).closest('form').submit()\">";
		String hrefTagEnd = "</a>";

		StringBuilder result = new StringBuilder();
		result.append(formTagStart);
		result.append(commandInput);
		result.append(idInputStart).append(invoice.getId()).append(idInputEnd);
		result.append(hrefTagStart).append(presentation);
		result.append(hrefAttributes).append(presentation);
		result.append(hrefTagEnd);
		result.append(formTagEnd);

		return result.toString();
	}

	private String getNumberDatePresentation() {

		String presentationString = "№ ";
		String datePattern = " - dd.MM.yyyy";

		StringBuffer buffer = new StringBuffer(presentationString);

		buffer.append(invoice.getNumber());
		DateFormat formatter = new SimpleDateFormat(datePattern);
		buffer.append(formatter.format(invoice.getDate()));

		return buffer.toString();
	}

}
