package com.epam.teamdev.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Method that renders HTML code for presentation of a message to a user. It has
 * 2 attributes message itself and flag either it is a error message or not. If
 * it is a error message then its displayed in red block.
 *
 */
public class welcomeMessageTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.gLogger");

	private String text;
	private boolean error;

	public void setError(String error) {
		this.error = Boolean.valueOf(error);

	}

	public void setText(String text) {

		this.text = text;

	}

	@Override
	public int doStartTag() throws JspException {

		String startText = "<div class=\"alert alert-";
		String okSpan = "success";
		String errorSpan = "danger";
		String spanParam = " \" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> ";
		String endText = "</div>";

		if (text == null || "".equals(text)) {
			return SKIP_BODY;
		}

		JspWriter out = pageContext.getOut();
		try {

			out.append(startText);

			if (error) {
				out.append(errorSpan);
			} else {
				out.append(okSpan);
			}
			out.append(spanParam);
			out.append(text);
			out.append(endText);
		} catch (IOException e) {

			LOG.error("Unable to write welcome message tag", e);
		}
		return SKIP_BODY;
	}

}
