package com.epam.teamdev.tag;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class renders the HTML code for display of date range picker element on the
 * page.
 * <p>
 * Data picker can be two types: single or range. If it's type is single , then
 * one date can be selected through this controll. In other case user can select
 * start of the date and end of the date. Also depends on user locale, so its
 * months and days of week are displayed correctly.
 *
 */
public class CalendarTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	private final static String LOCALIZATION_ADDRESS = "com.epam.teamdev.configuration.localization.local";
	private static final Logger LOG = LogManager.getLogger("com.epam.teamdev.gLogger");
	private final static String SINGLE_TYPE = "single";
	private final static String RANGE_TYPE = "range";
	private static final String READONLY_ATTRIBUTE = "readonly";
	private static final String DATE_LOCAL_TITLE = "calendar.dateTitle";
	private static final String CALENDAR_LOCAL_TITLE = "calendar.calendarParams";

	private static final Pattern DATE_PATTERN = Pattern
			.compile("(?<dateStartGroup>([0-9]{2}\\.){2}[0-9]{4})( - )?(?<dateEndGroup>([0-9]{2}\\.){2}[0-9]{4})?");
	private static final String DATE_END_GR = "dateEndGroup";
	private static final String DATE_START_GROUP = "dateStartGroup";
	private static final String DATE_FORMAT = "dd.MM.yyyy";

	private Locale locale;
	private String type;
	private String name;
	private String id;
	private String value;
	private Date dateStart = new Date();
	private Date dateEnd = new Date();
	private boolean readonly;

	public void setReadonly(String readonly) {
		this.readonly = Boolean.valueOf(readonly);

	}

	public void setLocale(String locale) {
		this.locale = new Locale(locale);
	}

	public void setValue(String value) {
		this.value = value;
		LOG.info(value);
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int doStartTag() throws JspException {

		parseDateValue();

		JspWriter out = pageContext.getOut();
		ResourceBundle bundle = ResourceBundle.getBundle(LOCALIZATION_ADDRESS, locale);

		switch (type) {
		case SINGLE_TYPE:
			writeSingleDatePicker(out, bundle);
			break;
		case RANGE_TYPE:
			writeRangeDatePicker(out, bundle);
			break;
		default:
			break;
		}
		return SKIP_BODY;
	}

	/**
	 * If date value is specified in tag attribute, then it should be parsed so
	 * element behavior is set correctly. In case of single type, only start
	 * date is filled, otherwise both dates are processed and set for this
	 * control element.
	 */
	private void parseDateValue() {

		if (value == null) {
			return;
		}
		Pattern pattern = DATE_PATTERN;
		Matcher matcher = pattern.matcher(value);

		while (matcher.find()) {

			String startString = (matcher.group(DATE_START_GROUP));
			String endString = (matcher.group(DATE_END_GR));
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			try {
				if (startString != null) {
					dateStart = formatter.parse(startString);
				}
				if (endString != null) {
					dateEnd = formatter.parse(endString);
				}
			} catch (ParseException e) {
				LOG.error("Can't parse date values");
			}

		}
	}

	/**
	 * Method consist that writes HTML text to the jsp writer.
	 * <p>
	 * It contains two parts HTML text itself and also javascript parameters
	 * initialization. So that the event of choosing element is handled
	 * correctly. Element parameters includes current date value of the element,
	 * localization and behavior (simple or range type).
	 * 
	 * @param out
	 *            the JspWriter of the jsp page.
	 * @param bundle
	 *            the localization bundle
	 */
	private void writeSingleDatePicker(JspWriter out, ResourceBundle bundle) {
		try {
			String input = getSimpleInputText(bundle);
			String script = getSingleHadlerScript(bundle);
			out.print(input);
			if (!readonly) {
				out.print(script);
			}

		} catch (IOException e) {
			LOG.error("Unable to write calendar tag to jsp", e);
		}
	}

	/**
	 * Analog of the same method but for range type of element(@see
	 * {@link #writeSingleDatePicker(JspWriter, ResourceBundle)}
	 */
	private void writeRangeDatePicker(JspWriter out, ResourceBundle bundle) {
		try {
			String input = getRangeInputText(bundle);
			String script = getRangeHadlerScript(bundle);
			out.print(input);
			if (!readonly) {
				out.print(script);
			}

		} catch (IOException e) {
			LOG.error("Unable to write calendar tag to jsp", e);
		}
	}

	private String getSimpleInputText(ResourceBundle bundle) {

		String startTag = "<input type=\"text\" class=\"form-control\" id=\"";
		String nameAtt = "\" name=\"";
		String placeholderAtt = "\" placeholder=\"";
		String valueAtt = "\" value=\"";
		String valueAttEnd = "\"";
		String endTag = "/>";

		StringBuilder result = new StringBuilder();

		result.append(startTag);
		result.append(id);
		result.append(nameAtt);
		result.append(name);
		result.append(placeholderAtt);
		result.append(bundle.getString(DATE_LOCAL_TITLE));

		result.append(valueAtt);
		result.append(getFormattedDate(dateStart));
		result.append(valueAttEnd);

		if (readonly) {
			result.append(READONLY_ATTRIBUTE);
		}
		result.append(endTag);
		return result.toString();
	}

	private String getRangeInputText(ResourceBundle bundle) {

		String startTag = "<input type=\"text\" class=\"form-control\" id=\"";
		String nameAtt = "\" name=\"";
		String placeholderAtt = "\" placeholder=\"";
		String valueAtt = "\" value=\"";
		String dateSeparator = " - ";
		String valueAttEnd = "\"";
		String endTag = "/>";

		StringBuilder result = new StringBuilder();

		result.append(startTag);
		result.append(id);
		result.append(nameAtt);
		result.append(name);
		result.append(placeholderAtt);
		result.append(bundle.getString(DATE_LOCAL_TITLE));

		result.append(valueAtt);
		result.append(getFormattedDate(dateStart));
		result.append(dateSeparator);
		result.append(getFormattedDate(dateEnd));

		result.append(valueAttEnd);

		if (readonly) {
			result.append(READONLY_ATTRIBUTE);
		}
		result.append(endTag);
		return result.toString();
	}

	private String getFormattedDate(Date date) {

		SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);
		if (date != null) {
			return formater.format(date);
		} else {
			return "";
		}

	}

	private String getSingleHadlerScript(ResourceBundle bundle) {

		StringBuilder result = new StringBuilder();

		String scriptInit = "<script type=\"text/javascript\"> $(\'#" + id + "\').daterangepicker({";
		String scriptParams = "\"startDate\": \"" + getFormattedDate(dateStart)
				+ "\",\"singleDatePicker\": true,\"showDropdowns\": true,\"autoApply\": true,\"linkedCalendars\": false,";

		String scriptLocaleParams = bundle.getString(CALENDAR_LOCAL_TITLE);
		String scriptEnd = "}, function(start, end, label) {});</script>";

		result.append(scriptInit);
		result.append(scriptParams);
		result.append(scriptLocaleParams);
		result.append(scriptEnd);
		return result.toString();
	}

	private String getRangeHadlerScript(ResourceBundle bundle) {
		StringBuilder result = new StringBuilder();

		String scriptInit = "<script type=\"text/javascript\"> $(\'#" + id + "\').daterangepicker({";
		String scriptParams = " ,\"singleDatePicker\": false,\"showDropdowns\": true,\"autoApply\": true,\"linkedCalendars\": false,";
		String dateStartParam = "\"startDate\": \"" + getFormattedDate(dateStart) + "\"";
		String dateEndParam = ", \"endDate\": \"" + getFormattedDate(dateEnd) + "\" ";
		String scriptLocaleParams = bundle.getString(CALENDAR_LOCAL_TITLE);
		String scriptEnd = "}, function(start, end, label) {});</script>";

		result.append(scriptInit);
		result.append(dateStartParam);
		result.append(dateEndParam);
		result.append(scriptParams);
		result.append(scriptLocaleParams);
		result.append(scriptEnd);
		return result.toString();
	}

}
