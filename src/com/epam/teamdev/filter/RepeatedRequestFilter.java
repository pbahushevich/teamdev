package com.epam.teamdev.filter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.teamdev.command.CommandName;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.RequestParameterName;

/**
 * 
 * Filter that protects the application from duplicate data modification
 * requests.
 * <p>
 * Jsp pages have javascript that adds unique token to all post requests. This
 * token is stored in user session, and when next time comes request with the
 * same token, it usually means that the user reloaded page. So this request is
 * ignored.
 */
public class RepeatedRequestFilter implements Filter {

	private static final String WRONG_TOKEN_MESSAGE = "Unable to fullfill request due to invalid token param";
	private static final String REPEATED_REQUEST_MESSAGE = "You are trying to repeate request, that already has been processed.";
	private static final String TOKEN_PARAM = "token";
	private static final String TOKENS_ATTRIBUTE = "tokens";
	private static final Set<CommandName> commands = new HashSet<>();

	/**
	 * Initializes filter and adds list of commands that should be checked,
	 * before execution.
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		commands.add(CommandName.ADD_TIME_SPENT);
		commands.add(CommandName.DELETE_INVOICE);
		commands.add(CommandName.DELETE_PROJECT);
		commands.add(CommandName.DELETE_TECHTASK);
		commands.add(CommandName.DELETE_USER);
		commands.add(CommandName.DELETE_WORKTYPE);
		commands.add(CommandName.SAVE_INVOICE);
		commands.add(CommandName.SAVE_PROJECT);
		commands.add(CommandName.SAVE_TECHTASK);
		commands.add(CommandName.SAVE_USER);
		commands.add(CommandName.SAVE_WORKTYPE);
		commands.add(CommandName.SET_PAID_FLAG);
		commands.add(CommandName.UPDATE_INVOICE);
		commands.add(CommandName.UPDATE_PROJECT);
		commands.add(CommandName.UPDATE_TECHTASK);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		System.out.println("Filter worked");
		if (!dataModifyingRequest(request)) {
			chain.doFilter(request, response);
		} else {
			processRequest(request, response, chain);
		}

	}

	/**
	 * Method stores token, that came in request, in a Set. If that token
	 * already was added to the Set, it means we have processed that request
	 * earlier.
	 * 
	 */
	private void processRequest(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;

		HttpSession session = httpRequest.getSession();
		Set<Double> tokens = getTokensFromSession(session);

		try {
			Double token = Double.valueOf(request.getParameter(TOKEN_PARAM));
			if (tokens.add(token)) {
				chain.doFilter(request, response);
			} else {
				String forwardString = CommandUtil.getWelcomePageWithMessage(httpRequest, REPEATED_REQUEST_MESSAGE,
						true);
				request.getRequestDispatcher(forwardString).forward(request, response);
			}
		} catch (NumberFormatException | NullPointerException e) {
			String forwardString = CommandUtil.getWelcomePageWithMessage(httpRequest, WRONG_TOKEN_MESSAGE, true);
			request.getRequestDispatcher(forwardString).forward(request, response);
		}
	}

	/**
	 * Method that gets a <code>Set</code> of tokens from session
	 * 
	 * @param session
	 *            the current session of the user
	 * @return
	 */
	private Set<Double> getTokensFromSession(HttpSession session) {
		Set<Double> tokens;
		try {
			tokens = (Set<Double>) session.getAttribute(TOKENS_ATTRIBUTE);
		} catch (ClassCastException e) {
			tokens = null;
		}

		if (tokens == null) {
			tokens = new HashSet<>();
			session.setAttribute(TOKENS_ATTRIBUTE, tokens);
		}
		return tokens;

	}

	/**
	 * Checks either the request should be filtered or not.
	 * 
	 * @param request
	 *            the request sent by user
	 * @return <code>true</code> if request has data modifying command and
	 *         should be filtered. <code>false</code> if it could be ignored.
	 */
	private boolean dataModifyingRequest(ServletRequest request) {

		String name = request.getParameter(RequestParameterName.COMMAND_NAME);
		try {
			CommandName commandName = CommandName.valueOf(name.toUpperCase());
			System.out.println("Command name =" + commandName);

			return commands.contains(commandName);
		} catch (NullPointerException e) {
			return false;
		}
	}

	@Override
	public void destroy() {
	}

}
