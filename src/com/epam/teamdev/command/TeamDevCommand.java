package com.epam.teamdev.command;

import javax.servlet.http.HttpServletRequest;

/*
 * Interface for the command that is executed by controller. Implementation should fill request attributes and
 * return address that request is forwarded to.
 */
public interface TeamDevCommand {
	public String execute(HttpServletRequest request) throws CommandException;
}