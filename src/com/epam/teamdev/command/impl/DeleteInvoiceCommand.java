package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.InvoiceService;

/**
 * 
 * Command used to delete Invoice by its id from database. 
 *
 */
public class DeleteInvoiceCommand implements TeamDevCommand {

	private static final String SUCCESS_MESSAGE = "Invoice successfully deleted.";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
	
		InvoiceService service = ServiceFactory.getInstance().getInvoiceService();
		service.deleteById(id);
		
		return CommandUtil.getWelcomePageWithMessage(request, SUCCESS_MESSAGE,false);
	}

}
