package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.DeveloperService;

/**
 * 
 * Command used to show the list of developers with the work they are working on.
 *
 */
public class ShowDevListCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		
		DeveloperService service = ServiceFactory.getInstance().getDeveloperService();
		List<ProjectWork> list = service.getDeveloperSchedule();
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		return JspPageName.DEVELOPER_LIST;
	
	
	}

}
