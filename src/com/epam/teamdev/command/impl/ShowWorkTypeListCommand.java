package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.WorkTypeService;

/**
 * 
 * Command used to show list of worktypes to user
 *
 */
public class ShowWorkTypeListCommand implements TeamDevCommand {

	private static final String WORKTYPE_LIST_PARAM = "worktypeList";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		WorkTypeService workTypeService = ServiceFactory.getInstance().getWorkTypeService();
		List<WorkType> list= workTypeService.getAll();
		request.setAttribute(WORKTYPE_LIST_PARAM, list);
		return JspPageName.WORKTYPE_LIST_PAGE;
	}

}
