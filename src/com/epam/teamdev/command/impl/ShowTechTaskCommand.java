package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.ProjectService;
import com.epam.teamdev.service.intf.TechTaskService;

/**
 * 
 * Command used to show techtask with its specification.
 *
 */
public class ShowTechTaskCommand implements TeamDevCommand {

	private static final String NOT_FOUND_ERROR = "Techtask not found by id";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);

		TechTaskService techTaskService = ServiceFactory.getInstance().getTechTaskService();
		TechTask task = techTaskService.getById(id);

		if (task == null) {
			return CommandUtil.getErrorPageWithMessage(request, NOT_FOUND_ERROR);
		}

		List<TaskWork> list = techTaskService.getWorkList(task);

		ProjectService projectService = ServiceFactory.getInstance().getProjectService();
		Project project = projectService.getByTechTaskId(task.getId());

		request.setAttribute(RequestParameterName.PROJECT, project);
		request.setAttribute(RequestParameterName.TECHTASK, task);
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		request.setAttribute(RequestParameterName.READ_ONLY, true);

		return JspPageName.TECHTASK_PAGE;
	}

}
