package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.WorkTypeService;

/**
 * 
 * Command used to delete Worktype by its id from database. 
 *
 */
public class DeleteWorkTypeCommand implements TeamDevCommand {

	private static final String WORKTYPE_LIST_PARAM = "worktypeList";
	private static final String SUCCESS_MESSAGE = "Worktype successfully removed";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
		WorkTypeService workTypeService = ServiceFactory.getInstance().getWorkTypeService();

		workTypeService.delete(id);
		request.setAttribute(RequestParameterName.USER_MESSAGE, SUCCESS_MESSAGE);

		List<WorkType> list = workTypeService.getAll();
		request.setAttribute(RequestParameterName.USER_MESSAGE, SUCCESS_MESSAGE);
		request.setAttribute(WORKTYPE_LIST_PARAM, list);
		
		return JspPageName.WORKTYPE_LIST_PAGE;

	}

}
