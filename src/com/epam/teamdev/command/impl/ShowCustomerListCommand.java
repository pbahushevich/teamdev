package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.CustomerService;

/**
 * 
 * Command used to display list of customers.
 *
 */
public class ShowCustomerListCommand implements TeamDevCommand {

	private static final String CUSTOMER_LIST_PARAM = "customerList";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		CustomerService customerService = ServiceFactory.getInstance().getCustomerService();
		List<Customer> list = customerService.getAll();
		request.setAttribute(CUSTOMER_LIST_PARAM, list);
		return JspPageName.CUSTOMER_LIST_PAGE;

	}
}
