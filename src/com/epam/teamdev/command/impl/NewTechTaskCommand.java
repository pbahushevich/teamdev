package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.QualificationService;
import com.epam.teamdev.service.intf.WorkTypeService;

/**
 * Command used to show page for creation new techtask.
 *
 */
public class NewTechTaskCommand implements TeamDevCommand {

	private static final String NEW_TASK_PARAM = "newTask";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		QualificationService qualificationService = ServiceFactory.getInstance().getQualificationService();
		List<Qualification> qualifications = qualificationService.getAll();

		WorkTypeService workTypeService = ServiceFactory.getInstance().getWorkTypeService();
		List<WorkType> workTypes = workTypeService.getAll();

		request.setAttribute(RequestParameterName.QUALIFICATION_LIST, qualifications);
		request.setAttribute(RequestParameterName.WORKTYPE_LIST, workTypes);
		request.setAttribute(NEW_TASK_PARAM, true);

		return JspPageName.TECHTASK_PAGE;

	}

}
