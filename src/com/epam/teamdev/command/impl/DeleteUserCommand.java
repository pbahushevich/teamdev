package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.CustomerService;
import com.epam.teamdev.service.intf.DeveloperService;
import com.epam.teamdev.service.intf.ManagerService;
import com.epam.teamdev.service.intf.UserService;

/**
 * 
 * Command used to delete User by its id from database.
 */
public class DeleteUserCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);

		deleteSpecificUser(id);

		return CommandUtil.getWelcomePageWithMessage(request, "Successfully deleted", false);
	}

	/**
	 * Method that deletes user by his id. First it determines whether user is customer, developer or manager,
	 * and after that deletes user with his inheritant.
	 * 
	 * @param id
	 */
	private void deleteSpecificUser(int id) {

		UserService service = ServiceFactory.getInstance().getUserService();
		User user = service.getById(id);

		switch (user.getRole()) {
		case CUSTOMER:
			CustomerService customerService = ServiceFactory.getInstance().getCustomerService();
			Customer customer = customerService.getByUser(user);
			customerService.deleteById(customer.getId());
			break;
		case MANAGER:
			ManagerService managerService = ServiceFactory.getInstance().getManagerService();
			Manager manager = managerService.getByUser(user);
			managerService.deleteById(manager.getId());
			break;
		case DEVELOPER:
			DeveloperService developerService = ServiceFactory.getInstance().getDeveloperService();
			Developer developer = developerService.getByUser(user);
			developerService.deleteById(developer.getId());
			break;
		default:
			throw new CommandException("Unexpected user role.");
		}
	}

}
