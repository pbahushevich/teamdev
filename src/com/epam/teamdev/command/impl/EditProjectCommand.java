package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.ProjectService;

/**
 * 
 * Command used to show edit page for Project by its id. 
 * 
 */
public class EditProjectCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
		
		ProjectService service = ServiceFactory.getInstance().getProjectService();
		Project project = service.getById(id);
		
		List<ProjectWork> list = service.getWorkList(project);
		
		request.setAttribute(RequestParameterName.PROJECT, project);
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		
		return JspPageName.EDIT_PROJECT_PAGE;
	
	}

}
