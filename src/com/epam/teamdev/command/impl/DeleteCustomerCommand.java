package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.CustomerService;

/**
 * 
 * Command used to delete customer by his id from database.
 *
 */
public class DeleteCustomerCommand implements TeamDevCommand {

	private static final String SUCCESS_MESSAGE = "Successfully removed";
	private static final String CUSTOMER_LIST_PARAM = "customerList";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);

		CustomerService customerService = ServiceFactory.getInstance().getCustomerService();
		
		customerService.deleteById(id);
		List<Customer> list = customerService.getAll();
		
		request.setAttribute(RequestParameterName.USER_MESSAGE, SUCCESS_MESSAGE);
		request.setAttribute(CUSTOMER_LIST_PARAM, list);
		
		return JspPageName.CUSTOMER_LIST_PAGE;
	
	}

}
