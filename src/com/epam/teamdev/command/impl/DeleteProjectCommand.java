package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.ProjectService;

/**
 * 
 * Command used to delete Project by its id from database. 
  */
public class DeleteProjectCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
	
		ProjectService service = ServiceFactory.getInstance().getProjectService();
		service.deleteById(id);
		return CommandUtil.getWelcomePageWithMessage(request, "Project successfully deleted.",false);
	
	
	}

}
