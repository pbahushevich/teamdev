package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.controller.JspPageName;

/**
 * 
 * Command logs out the user, clears his session, and returns address of login page.
 */
public class LogOutCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		HttpSession session = request.getSession();
		session.invalidate();

		return JspPageName.LOGIN_PAGE;
	}

}
