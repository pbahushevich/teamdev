package com.epam.teamdev.command.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.ProjectService;

/*
 * Command used to save project to database. Returns the address of user starting page.
 * <p>
 *  As we need to save the project specification table we got a list of rows, where qualification,
 *  techtask work, developer and dates are specified. According servlet specification each of parameters 
 *  comes as array of its values. So to process this params method use map&lt;String,String[]&gt;
 */
public class SaveProjectCommand implements TeamDevCommand{
	
	private static final String SUCCESS_MESSAGE = "Project successfully created";
	private static final String USER_PARAM = "user";
	private static final String MANAGER_ID_PARAM = "manager_id";


	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Map<String, String[]> params = CommandUtil.getArrayParameterMap(request);
		addCurrentUserToParams(params, request);
		ProjectService service = ServiceFactory.getInstance().getProjectService();
		service.createNew(params);
		
		return CommandUtil.getWelcomePageWithMessage(request,SUCCESS_MESSAGE,false);
	}

	/**
	 * Method adds current user from session to project attributes.
	 * Current user is saved as author(manager) of the project.
	 * @param params			list of project attribute values
	 * @param request			request sent by user
	 * @throws CommandException	in case user in not defined, or is of illegal type.
	 */
	private void addCurrentUserToParams(Map<String, String[]> params, HttpServletRequest request)
			throws CommandException {

		HttpSession session = request.getSession();
		try {

			Manager manager = (Manager) session.getAttribute(USER_PARAM);
			addManagerIdToParams(manager,params);

		} catch (ClassCastException | NullPointerException e) {
			throw new CommandException("Can't crate project. Current user is undefiend", e);
		}
	}

	/**
	 * Method creates an array of size 1 containing manager and adds it to the map of values.
	 */
	private void addManagerIdToParams(Manager manager,Map<String, String[]> params) {
		String[] managerId = new String[1];
		managerId[0] = String.valueOf(manager.getId());
		params.put(MANAGER_ID_PARAM, managerId);
	}
}
