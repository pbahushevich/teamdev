package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.TechTaskService;
/**
 * 
 * Command used to display page for creation new project on the base of tech task.
 */
public class NewProjectCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);

		TechTaskService techTaskService = ServiceFactory.getInstance().getTechTaskService();
		TechTask task = techTaskService.getById(id);
		List<TaskWork> list = techTaskService.getWorkList(task);
		request.setAttribute(RequestParameterName.TECHTASK, task);
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		
		return JspPageName.NEW_PROJECT_PAGE;
	}

}
