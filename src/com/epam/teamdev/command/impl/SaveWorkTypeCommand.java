package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.WorkTypeService;

/**
 * 
 * Saves new worktype to database.
 *
 */
public class SaveWorkTypeCommand implements TeamDevCommand {

	private static final String TITLE_PARAM = "title";
	private static final String SUCCESS_MESSAGE = "Successfully added";
	private static final String WORKTYPE_LIST_PARAM = "worktypeList";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String titleIso = CommandUtil.getNotNullParam(request, TITLE_PARAM);
		String title = CommandUtil.convertISOtoUTF(titleIso);

		WorkTypeService workTypeService = ServiceFactory.getInstance().getWorkTypeService();
		workTypeService.add(title);
		request.setAttribute(RequestParameterName.USER_MESSAGE, SUCCESS_MESSAGE);

		List<WorkType> list = workTypeService.getAll();
		request.setAttribute(RequestParameterName.USER_MESSAGE, SUCCESS_MESSAGE);
		request.setAttribute(WORKTYPE_LIST_PARAM, list);
		
		return JspPageName.WORKTYPE_LIST_PAGE;

	}

}
