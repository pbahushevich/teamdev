package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.InvoiceService;
import com.epam.teamdev.service.intf.ProjectService;

/**
 * 
 *	Command used to show project with its work list to user. 
 *
 */
public class ShowProjectCommand implements TeamDevCommand {

	private static final String NOT_FOUND_ERROR = "Project not found by id";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
		ProjectService projectService = ServiceFactory.getInstance().getProjectService();
		Project project = projectService.getById(id);

		if (project == null) {
			return CommandUtil.getErrorPageWithMessage(request, NOT_FOUND_ERROR);
		}

		List<ProjectWork> list = projectService.getWorkList(project);
		
		InvoiceService invoiceService = ServiceFactory.getInstance().getInvoiceService();
		Invoice invoice = invoiceService.getForProject(project);
		
		request.setAttribute(RequestParameterName.PROJECT, project);
		request.setAttribute(RequestParameterName.INVOICE, invoice);
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		request.setAttribute(RequestParameterName.READ_ONLY, true);

		return JspPageName.PROJECT_PAGE;
	}

}
