package com.epam.teamdev.command.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.InvoiceService;


/**
 * 
 * Command used to update Invoice data.
 * 
 */
public class UpdateInvoiceCommand implements TeamDevCommand {

	private static final String SUCCESS_MESSAGE = "Successfully updated invoice.";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Map<String, String> params = CommandUtil.getSimpleParameterMap(request);
		InvoiceService service = ServiceFactory.getInstance().getInvoiceService();
		service.update(params);

		return CommandUtil.getWelcomePageWithMessage(request, SUCCESS_MESSAGE, false);
	}

}
