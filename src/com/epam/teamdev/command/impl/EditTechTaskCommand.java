package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.WorkType;
import com.epam.teamdev.bean.TechTask.TaskWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.QualificationService;
import com.epam.teamdev.service.intf.TechTaskService;
import com.epam.teamdev.service.intf.WorkTypeService;

/**
 * 
 * Command used to show edit page for Techtask by its id. 
 * As long as user could add new rows, we should provide him with all possible work types and qualifications.
 */
public class EditTechTaskCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
		
		TechTaskService service = ServiceFactory.getInstance().getTechTaskService();
		TechTask task = service.getById(id);
		List<TaskWork> list = service.getWorkList(task);
		
		QualificationService qualificationService = ServiceFactory.getInstance().getQualificationService();
		List<Qualification> qualifications = qualificationService.getAll();

		WorkTypeService workTypeService = ServiceFactory.getInstance().getWorkTypeService();
		List<WorkType> workTypes = workTypeService.getAll();

		request.setAttribute(RequestParameterName.TECHTASK, task);
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		request.setAttribute(RequestParameterName.QUALIFICATION_LIST, qualifications);
		request.setAttribute(RequestParameterName.WORKTYPE_LIST, workTypes);
		request.setAttribute(RequestParameterName.READ_ONLY, false);
		
		return JspPageName.TECHTASK_PAGE;
	}

}
