package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.DeveloperService;

/**
 * Command used to store to database time spent by developer on specific job.
 *
 */
public class AddTimeSpentCommand implements TeamDevCommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String workIdString = CommandUtil.getNotNullParam(request, "workId");
		String timeString= CommandUtil.getNotNullParam(request, "timeSpent");

		try{
			int workId = Integer.valueOf(workIdString);
			double timeSpent = Double.valueOf(timeString);
		DeveloperService service = ServiceFactory.getInstance().getDeveloperService();
		service.addWorkSpentTime(workId, timeSpent);
		}catch(NumberFormatException e){
			throw new CommandException("Error parsing parameters.",e);
		}
		
		return CommandUtil.getWelcomePageWithMessage(request, "", false);
	}

}
