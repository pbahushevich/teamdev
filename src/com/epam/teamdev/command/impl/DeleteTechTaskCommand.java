package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.TechTaskService;

/**
 * 
 * Command used to delete Techtask by its id from database. After success returns user to the welcome page.
 */
public class DeleteTechTaskCommand implements TeamDevCommand {

	private static final String SUCCESS_MESSAGE = "Techtask successfully removed";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);

		TechTaskService taskService = ServiceFactory.getInstance().getTechTaskService();
		taskService.deleteById(id);
		
		String forwardAddress = CommandUtil.getWelcomePageWithMessage(request,SUCCESS_MESSAGE,false);
		return forwardAddress;
		
		}

}
