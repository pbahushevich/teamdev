package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.ProjectService;

/**
 * 
 * Command show the page for creating new invoice. Also shows information 
 * about the work that was done on the according project. 
 */
public class NewInvoiceCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		
		int projectId = CommandUtil.getIdParam(request);
		
		ProjectService service = ServiceFactory.getInstance().getProjectService();
		Project  project = service.getById(projectId);
		List<ProjectWork> list = service.getWorkList(project);
		request.setAttribute(RequestParameterName.PROJECT, project );
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		request.setAttribute("newInvoice", true);
		return JspPageName.INVOICE;
	}

}
