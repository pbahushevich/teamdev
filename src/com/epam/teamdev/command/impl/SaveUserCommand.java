package com.epam.teamdev.command.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Role;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.CustomerService;
import com.epam.teamdev.service.intf.DeveloperService;
import com.epam.teamdev.service.intf.ManagerService;

/**
 * 
 * Command saves new user to databse according to its role;
 *
 */
public class SaveUserCommand implements TeamDevCommand {

	private static final String ROLE_PARAM = "role";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String roleString = CommandUtil.getNotNullParam(request, ROLE_PARAM);
		Role role = Role.valueOf(roleString.toUpperCase());
		
		Map<String, String> values = CommandUtil.getSimpleParameterMap(request);
		switch (role) {
		case CUSTOMER:
			CustomerService customerService = ServiceFactory.getInstance().getCustomerService();
			customerService.createNew(values);
			break;
		case MANAGER:
			ManagerService managerService = ServiceFactory.getInstance().getManagerService();
			managerService.createNew(values);
			break;
		case DEVELOPER:
			DeveloperService developerService = ServiceFactory.getInstance().getDeveloperService();
			developerService.createNew(values);
			break;
		default:
			throw new CommandException("Unexpected user role, while adding new user.");
		}
		return CommandUtil.getWelcomePageWithMessage(request, "Successfully created new user", false);
	}

}
