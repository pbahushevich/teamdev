package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.bean.ProjectTaskInvoice;
import com.epam.teamdev.bean.TechTask;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.dao.DAOException;
import com.epam.teamdev.service.ServiceException;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.DeveloperService;
import com.epam.teamdev.service.intf.InvoiceService;
import com.epam.teamdev.service.intf.ProjectService;
import com.epam.teamdev.service.intf.TechTaskService;

/**
 * 
 * Command used to show welcome page to the user according to his role. If user
 * is not correctly specified in session attributes, than command method returns
 * the address of login page.
 * 
 */
public class ShowWelcomePageCommand implements TeamDevCommand {

	private static final String INVOICE_LIST_PARAM = "invoiceList";
	private static final String MANAGER_ERROR_MESSAGE = "Unable to get manager data";
	private static final String CUSTOMER_ERROR_MESSAGE = "Unable to get customer data";
	private static final String INVALID_ROLE_MESSAGE = "Invalid role for welcome page";
	private static final String SESSION_EXPIRED_MESSAGE = "Sorry your session has expired. Please login";
	private static final String USER_ATTRIBUTE = "user";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		HttpSession session = request.getSession(true);

		User user = (User) session.getAttribute(USER_ATTRIBUTE);
		if (user == null) {
			return CommandUtil.getLoginPageWithMessage(request, SESSION_EXPIRED_MESSAGE);
		}

		try {
			return getProcessedWelcomePage(user, request);
		} catch (Exception e) {
			return CommandUtil.getLoginPageWithMessage(request, e.getMessage());
		}

	}

	/**
	 * Method that determines the user role and return adrress of the according
	 * page name.
	 * 
	 * @param user
	 *            user to display page for
	 * @param request
	 *            request sent by user
	 * @return address of shown to user page
	 * @throws CommandException
	 *             in case user role was one of three: Manager,
	 *             Developer,Customer.
	 */
	private String getProcessedWelcomePage(User user, HttpServletRequest request) throws CommandException {

		switch (user.getRole()) {
		case MANAGER:
			return getProcessedManagerPage(user, request);
		case CUSTOMER:
			return getProcessedCustomerPage(user, request);
		case DEVELOPER:
			return getProcessedDeveloperPage(user, request);
		default:
			throw new CommandException(INVALID_ROLE_MESSAGE);
		}

	}

	/**
	 * Fills the data of developer welcome page and return the address of
	 * according jsp
	 * 
	 * @param user
	 *            user to display page for
	 * @param request
	 *            request sent by user
	 * @return address of shown to user page
	 */
	private String getProcessedDeveloperPage(User user, HttpServletRequest request) {

		try {
			if (user.getClass() != Developer.class) {
				throw new CommandException("Can't process user to manager information");
			}

			Developer developer = (Developer) user;

			DeveloperService service = ServiceFactory.getInstance().getDeveloperService();
			List<ProjectWork> list = service.getDeveloperWorkList(developer.getId());

			request.setAttribute(RequestParameterName.WORK_LIST, list);
			return JspPageName.DEVELOPER_WELCOME_PAGE;
		} catch (DAOException | ServiceException e) {
			return CommandUtil.getErrorPageWithMessage(request, MANAGER_ERROR_MESSAGE);
		}

	}

	/**
	 * Fills the data of manager welcome page and return the address of
	 * according jsp
	 * 
	 * @param user
	 *            user to display page for
	 * @param request
	 *            request sent by user
	 * @return address of shown to user page
	 */
	private String getProcessedManagerPage(User user, HttpServletRequest request) {

		try {
			if (user.getClass() != Manager.class) {
				throw new CommandException("Can't process user to manager information");
			}

			Manager manager = (Manager) user;

			ProjectService service = ServiceFactory.getInstance().getProjectService();
			List<ProjectTaskInvoice> list = service.getProjectListForManager(manager);

			request.setAttribute(RequestParameterName.PROJECT_TASK_LIST, list);

			TechTaskService techTaskService = ServiceFactory.getInstance().getTechTaskService();
			List<TechTask> unprocessedTasks = techTaskService.getUnprocessed();
			request.setAttribute(RequestParameterName.TECHTASK_LIST, unprocessedTasks);

			return JspPageName.MANAGER_WELCOME_PAGE;
		} catch (DAOException | ServiceException e) {
			return CommandUtil.getErrorPageWithMessage(request, MANAGER_ERROR_MESSAGE);
		}
	}

	/**
	 * Fills the data of customer welcome page and return the address of
	 * according jsp
	 * 
	 * @param user
	 *            user to display page for
	 * @param request
	 *            request sent by user
	 * @return address of shown to user page
	 */

	private String getProcessedCustomerPage(User user, HttpServletRequest request) throws CommandException {

		try {
			if (user.getClass() != Customer.class) {
				throw new CommandException("Can't process user to customer");
			}

			Customer customer = (Customer) user;
			ProjectService service = ServiceFactory.getInstance().getProjectService();
			List<ProjectTaskInvoice> list = service.getProjectListForCustomer(customer);

			request.setAttribute(RequestParameterName.PROJECT_TASK_LIST, list);

			InvoiceService invoiceService = ServiceFactory.getInstance().getInvoiceService();
			List<Invoice> invoiceList = invoiceService.getForCustomer(customer);
			request.setAttribute(INVOICE_LIST_PARAM, invoiceList);

			return JspPageName.CUSTOMER_WELCOME_PAGE;

		} catch (DAOException | ServiceException e) {
			return CommandUtil.getErrorPageWithMessage(request, CUSTOMER_ERROR_MESSAGE);
		}
	}

}
