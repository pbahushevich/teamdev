package com.epam.teamdev.command.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.TechTaskService;

/**
 * Command saves the techtask to database.
 *
 */
public class SaveTechTaskCommand implements TeamDevCommand {

	private static final String SUCCESS_MESSAGE = "TechTask successfully added";
	private static final String USER_PARAM = "user";
	private static final String CUSTOMER_ID_PARAM = "customer_id";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Map<String, String[]> params = CommandUtil.getArrayParameterMap(request);
		addCurrentUserToParams(params, request);
		TechTaskService techTaskService = ServiceFactory.getInstance().getTechTaskService();
		techTaskService.createNew(params);
		
		return CommandUtil.getWelcomePageWithMessage(request,SUCCESS_MESSAGE,false);

	}

	/**
	 * Method adds current session user as customer for techtask to the map of parameters.
	 * @param params			values for creating new techtask
	 * @param request			request sent by user
	 * @throws CommandException	in case of illegal value of user in session params.
	 */
	private void addCurrentUserToParams(Map<String, String[]> params, HttpServletRequest request)
			throws CommandException {

		HttpSession session = request.getSession();
		try {

			Customer customer = (Customer) session.getAttribute(USER_PARAM);
			String[] customerId = getIDParamInArrayString(customer);

			params.put(CUSTOMER_ID_PARAM, customerId);

		} catch (ClassCastException | NullPointerException e) {
			throw new CommandException("Can't save tech task. Current user is undefiend", e);
		}
	}

	/**
	 * Method creates an array of 1 element where customer id is stored, to add it to the map of techtask values.
	 * @param customer	Customer, who creates the techtask;
	 * @return			id of the customer stored in array;
	 */
	private String[] getIDParamInArrayString(Customer customer) {
		String[] customerId = new String[1];
		customerId[0] = String.valueOf(customer.getId());
		return customerId;
	}

}
