package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.controller.SessionAttributeName;

/**
 * 
 * Command used to change localization of user interface.
 *
 */
public class SetLocaleCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String page = CommandUtil.getNotNullParam(request, RequestParameterName.TARGET_PAGE);
		String localeParam = CommandUtil.getNotNullParam(request, RequestParameterName.LOCALE);

		HttpSession session = request.getSession();
		session.setAttribute(SessionAttributeName.LOCALE, localeParam);
		return page;
	}

}
