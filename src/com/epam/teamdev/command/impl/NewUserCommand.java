package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Qualification;
import com.epam.teamdev.bean.Role;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.QualificationService;

/*
 * Command used to show page for creating new user(for all roles). 
 * For developers we also need to display the list of all possible qualifications.
 */
public class NewUserCommand implements TeamDevCommand {

	private static final String ROLE_PARAM = "role";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String roleString = CommandUtil.getNotNullParam(request, ROLE_PARAM);
		Role role = Role.valueOf(roleString.toUpperCase());
		
		if(role == Role.DEVELOPER){
			QualificationService service = ServiceFactory.getInstance().getQualificationService();
			List<Qualification> list= service.getAll();
			request.setAttribute(RequestParameterName.QUALIFICATION_LIST, list);
		}
		request.setAttribute(ROLE_PARAM, role);
		
		return JspPageName.NEW_USER_PAGE;
	}

}
