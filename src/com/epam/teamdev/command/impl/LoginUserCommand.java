package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Customer;
import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.bean.Manager;
import com.epam.teamdev.bean.User;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.controller.SessionAttributeName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.CustomerService;
import com.epam.teamdev.service.intf.DeveloperService;
import com.epam.teamdev.service.intf.ManagerService;
import com.epam.teamdev.service.intf.UserService;

/**
 * 
 * Command handle the login of the user. It takes login and password params from the request, gets the user, 
 * and after defining his role writes to session according customer, developer or manager.
 * After that return the address of welcome page.
 */
public class LoginUserCommand implements TeamDevCommand {

	private static final String EMPTY_USER_MESSAGE = "";
	private static final String ERROR_MESSAGE = "Invalid login/password. Please try again";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		String login = CommandUtil.getNotNullParam(request, RequestParameterName.LOGIN);
		String password = CommandUtil.getNotNullParam(request, RequestParameterName.PASSWORD);

		UserService userService = ServiceFactory.getInstance().getUserService();
		User user = userService.getByLoginPass(login, password);
		if (user != null) {

			switch (user.getRole()) {
			case CUSTOMER:
				CustomerService customerServicee = ServiceFactory.getInstance().getCustomerService();
				Customer customer = customerServicee.getByUser(user);
				request.getSession().setAttribute(SessionAttributeName.USER, customer);
				break;
			case MANAGER:
				ManagerService managerService = ServiceFactory.getInstance().getManagerService();
				Manager manager = managerService.getByUser(user);
				request.getSession().setAttribute(SessionAttributeName.USER, manager);
				break;
			case DEVELOPER:
				DeveloperService developerService = ServiceFactory.getInstance().getDeveloperService();
				Developer developer = developerService.getByUser(user);
				request.getSession().setAttribute(SessionAttributeName.USER, developer);
				break;
			default:
				break;
			}
			String forwardAddress = CommandUtil.getWelcomePageWithMessage(request, EMPTY_USER_MESSAGE, false);
			return forwardAddress;

		} else {
			String forwardAddress = CommandUtil.getLoginPageWithMessage(request, ERROR_MESSAGE);
			return forwardAddress;
		}

	}

}