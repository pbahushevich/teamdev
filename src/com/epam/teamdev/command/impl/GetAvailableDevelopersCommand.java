package com.epam.teamdev.command.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Developer;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.DeveloperService;

/**
 * 
 *Command that processes ajax request and sends back a list of developers that are not busy on any other projects.
 *Command is called while creating new project, and while editing new existing project as well. 
 *So we need an id of the project (so we don't take in account project that we are editing) qualification of the developer 
 *and dates that we are looking developer for. Parameters are checked in developer service.
 */
public class GetAvailableDevelopersCommand implements TeamDevCommand {

	private static final String DEVELOPERS_PARAM = "developers";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Map<String, String> params = CommandUtil.getSimpleParameterMap(request);

		DeveloperService developerService = ServiceFactory.getInstance().getDeveloperService();
		List<Developer> list = developerService.getAvailable(params);
		request.setAttribute(DEVELOPERS_PARAM, list);
		return JspPageName.AVAILABLE_DEVS_LIST;
	}

}
