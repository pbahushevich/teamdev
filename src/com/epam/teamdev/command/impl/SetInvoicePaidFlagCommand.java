package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.InvoiceService;

/**
 * 
 *	Command used to set invoice as paid by customer, or cancel payment by manager.
 *	<p>
 *	Gets invoice id and flag boolean value as request parameters.
 */
public class SetInvoicePaidFlagCommand implements TeamDevCommand {

	private static final String PAID_PARAM = "paid";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
		String paid = CommandUtil.getNotNullParam(request, PAID_PARAM);
		boolean paidFlag = Boolean.valueOf(paid);
		
		InvoiceService service = ServiceFactory.getInstance().getInvoiceService();
		service.setPaidFlag(id, paidFlag);
	
		return CommandUtil.getWelcomePageWithMessage(request, "Paid flag successfully changed.", false);
	}

}
