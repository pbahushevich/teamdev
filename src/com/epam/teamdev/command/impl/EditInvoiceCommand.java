package com.epam.teamdev.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.Invoice;
import com.epam.teamdev.bean.Project;
import com.epam.teamdev.bean.Project.ProjectWork;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.InvoiceService;
import com.epam.teamdev.service.intf.ProjectService;

/**
 * 
 * Command used to show edit page for Invoice. Also shows all the work done on the project, that invoice is made for.
 *
 */
public class EditInvoiceCommand implements TeamDevCommand{

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);
		
		InvoiceService invoiceService = ServiceFactory.getInstance().getInvoiceService();
		Invoice invoice = invoiceService.getById(id);
		Project project = invoice.getProject();

		ProjectService service = ServiceFactory.getInstance().getProjectService();
		
		List<ProjectWork> list = service.getWorkList(project);
		
		request.setAttribute(RequestParameterName.INVOICE, invoice);
		request.setAttribute(RequestParameterName.PROJECT, project);
		request.setAttribute(RequestParameterName.WORK_LIST, list);
		request.setAttribute(RequestParameterName.READ_ONLY, false);
		
		return JspPageName.INVOICE;
	}

}
