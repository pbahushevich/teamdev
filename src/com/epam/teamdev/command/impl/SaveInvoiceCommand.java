package com.epam.teamdev.command.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.InvoiceService;

/*
 * Command used to save Invoice to database. Converts request parameters to map and send it to the Invoice service.
 */
public class SaveInvoiceCommand implements TeamDevCommand {

	private static final String SUCCESS_MESSAGE = "Invoice successfully created.";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		Map<String, String> values = CommandUtil.getSimpleParameterMap(request);
		InvoiceService service = ServiceFactory.getInstance().getInvoiceService();
		service.createNew(values);
		
		return CommandUtil.getWelcomePageWithMessage(request, SUCCESS_MESSAGE, false);
		
	}

}
