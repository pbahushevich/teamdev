package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;

/*
 * Command that is returned to controller when it wasn't possible to identify the proper command.
 */
public class NoSuchCommand implements TeamDevCommand {
	private static final String NO_COMMAND_MESSAGE = "There is no such command that you are trying to execute.";

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
	
		String forwardAddress = CommandUtil.getErrorPageWithMessage(request,NO_COMMAND_MESSAGE);
		return forwardAddress;
	}
}