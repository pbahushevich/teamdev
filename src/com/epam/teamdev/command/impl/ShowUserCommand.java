package com.epam.teamdev.command.impl;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.bean.User;
import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.command.TeamDevCommand;
import com.epam.teamdev.command.util.CommandUtil;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
import com.epam.teamdev.service.ServiceFactory;
import com.epam.teamdev.service.intf.CustomerService;
import com.epam.teamdev.service.intf.DeveloperService;
import com.epam.teamdev.service.intf.ManagerService;
import com.epam.teamdev.service.intf.UserService;

/**
 * 
 * Command used to display user information. 
 * 
 *
 */
public class ShowUserCommand implements TeamDevCommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {

		int id = CommandUtil.getIdParam(request);

		User user = getSpecificUser(id);
		request.setAttribute(RequestParameterName.USER, user);
		request.setAttribute(RequestParameterName.READ_ONLY, true);

		return JspPageName.USER_PAGE;
	}

	/**
	 * Method gets the user by his id, and according to his role gets customer,developer or manager.
	 * @param id
	 * @return
	 */
	private User getSpecificUser(int id) {

		UserService service = ServiceFactory.getInstance().getUserService();
		User user = service.getById(id);

		if (user == null) {
			throw new CommandException("No user found for id " + id);
		}
		switch (user.getRole()) {
			case CUSTOMER:
				CustomerService customerService = ServiceFactory.getInstance().getCustomerService();
				user = customerService.getByUser(user);
				break;
			case MANAGER:
				ManagerService managerService = ServiceFactory.getInstance().getManagerService();
				user = managerService.getByUser(user);
				break;
			case DEVELOPER:
				DeveloperService developerService = ServiceFactory.getInstance().getDeveloperService();
				user = developerService.getByUser(user);
				break;
			default:
				throw new CommandException("Unexpected user role.");
		}
		return user;
	}

}
