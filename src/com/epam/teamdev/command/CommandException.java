package com.epam.teamdev.command;

/**
 * 
 * Exception thrown by commands.
 *
 */
public class CommandException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public CommandException(String msg) {
		super(msg);
	}

	public CommandException(String msg, Exception e) {
		super(msg, e);
	}
	

}
