package com.epam.teamdev.command;

import java.util.HashMap;
import java.util.Map;

import com.epam.teamdev.command.impl.AddTimeSpentCommand;
import com.epam.teamdev.command.impl.DeleteInvoiceCommand;
import com.epam.teamdev.command.impl.DeleteProjectCommand;
import com.epam.teamdev.command.impl.DeleteTechTaskCommand;
import com.epam.teamdev.command.impl.DeleteUserCommand;
import com.epam.teamdev.command.impl.DeleteWorkTypeCommand;
import com.epam.teamdev.command.impl.EditInvoiceCommand;
import com.epam.teamdev.command.impl.EditProjectCommand;
import com.epam.teamdev.command.impl.EditTechTaskCommand;
import com.epam.teamdev.command.impl.GetAvailableDevelopersCommand;
import com.epam.teamdev.command.impl.LogOutCommand;
import com.epam.teamdev.command.impl.LoginUserCommand;
import com.epam.teamdev.command.impl.NewInvoiceCommand;
import com.epam.teamdev.command.impl.NewProjectCommand;
import com.epam.teamdev.command.impl.NewTechTaskCommand;
import com.epam.teamdev.command.impl.NewUserCommand;
import com.epam.teamdev.command.impl.NoSuchCommand;
import com.epam.teamdev.command.impl.SaveInvoiceCommand;
import com.epam.teamdev.command.impl.SaveProjectCommand;
import com.epam.teamdev.command.impl.SaveTechTaskCommand;
import com.epam.teamdev.command.impl.SaveUserCommand;
import com.epam.teamdev.command.impl.SaveWorkTypeCommand;
import com.epam.teamdev.command.impl.SetInvoicePaidFlagCommand;
import com.epam.teamdev.command.impl.SetLocaleCommand;
import com.epam.teamdev.command.impl.ShowCustomerListCommand;
import com.epam.teamdev.command.impl.ShowDevListCommand;
import com.epam.teamdev.command.impl.ShowInvoiceCommand;
import com.epam.teamdev.command.impl.ShowProjectCommand;
import com.epam.teamdev.command.impl.ShowTechTaskCommand;
import com.epam.teamdev.command.impl.ShowUserCommand;
import com.epam.teamdev.command.impl.ShowWelcomePageCommand;
import com.epam.teamdev.command.impl.ShowWorkTypeListCommand;
import com.epam.teamdev.command.impl.UpdateInvoiceCommand;
import com.epam.teamdev.command.impl.UpdateProjectCommand;
import com.epam.teamdev.command.impl.UpdateTechTaskCommand;

/**
 * Class designed to get command object by the command name.
 *
 */
public final class CommandFactory {

	private static final CommandFactory instance = new CommandFactory();
	private Map<CommandName, TeamDevCommand> commands = new HashMap<>();

	private CommandFactory() {
		commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());

		commands.put(CommandName.SET_LOCALE, new SetLocaleCommand());
		commands.put(CommandName.LOGIN_USER, new LoginUserCommand());
		commands.put(CommandName.LOG_OUT, new LogOutCommand());
		commands.put(CommandName.SHOW_WELCOME_PAGE, new ShowWelcomePageCommand());

		commands.put(CommandName.SHOW_USER, new ShowUserCommand());
		commands.put(CommandName.DELETE_USER, new DeleteUserCommand());
		commands.put(CommandName.NEW_USER, new NewUserCommand());
		commands.put(CommandName.SAVE_USER, new SaveUserCommand());

		commands.put(CommandName.SHOW_CUSTOMER_LIST, new ShowCustomerListCommand());

		commands.put(CommandName.SHOW_WORKTYPE, new ShowWorkTypeListCommand());
		commands.put(CommandName.SAVE_WORKTYPE, new SaveWorkTypeCommand());
		commands.put(CommandName.DELETE_WORKTYPE, new DeleteWorkTypeCommand());

		commands.put(CommandName.SHOW_TECHTASK, new ShowTechTaskCommand());
		commands.put(CommandName.NEW_TECHTASK, new NewTechTaskCommand());
		commands.put(CommandName.SAVE_TECHTASK, new SaveTechTaskCommand());
		commands.put(CommandName.DELETE_TECHTASK, new DeleteTechTaskCommand());
		commands.put(CommandName.EDIT_TECHTASK, new EditTechTaskCommand());
		commands.put(CommandName.UPDATE_TECHTASK, new UpdateTechTaskCommand());

		commands.put(CommandName.SHOW_PROJECT, new ShowProjectCommand());
		commands.put(CommandName.NEW_PROJECT, new NewProjectCommand());
		commands.put(CommandName.SAVE_PROJECT, new SaveProjectCommand());
		commands.put(CommandName.DELETE_PROJECT, new DeleteProjectCommand());
		commands.put(CommandName.EDIT_PROJECT, new EditProjectCommand());
		commands.put(CommandName.UPDATE_PROJECT, new UpdateProjectCommand());

		commands.put(CommandName.SHOW_INVOICE, new ShowInvoiceCommand());
		commands.put(CommandName.NEW_INVOICE, new NewInvoiceCommand());
		commands.put(CommandName.SAVE_INVOICE, new SaveInvoiceCommand());
		commands.put(CommandName.EDIT_INVOICE, new EditInvoiceCommand());
		commands.put(CommandName.UPDATE_INVOICE, new UpdateInvoiceCommand());
		commands.put(CommandName.DELETE_INVOICE, new DeleteInvoiceCommand());
		commands.put(CommandName.SET_PAID_FLAG, new SetInvoicePaidFlagCommand());

		commands.put(CommandName.GET_AVAILABLE_DEVS, new GetAvailableDevelopersCommand());
		commands.put(CommandName.SHOW_DEVLIST, new ShowDevListCommand());
		commands.put(CommandName.ADD_TIME_SPENT, new AddTimeSpentCommand());
		commands.put(null, new NoSuchCommand());

	}

	public static CommandFactory getInstance() {
		return instance;
	}

	/**
	 * Method returns the same instance of each command type by its name.
	 * Concurrent issues should be taken into consideration.
	 *
	 * @param commandName
	 *            name of Command
	 * @return according command object
	 */
	public TeamDevCommand getCommand(String commandName) {

		TeamDevCommand command;
		CommandName name;
		try {
			name = CommandName.valueOf(commandName.toUpperCase());
		} catch (IllegalArgumentException | NullPointerException e) {
			return new NoSuchCommand();
		}

		command = commands.get(name);

		return command != null ? command : new NoSuchCommand();
	}

}