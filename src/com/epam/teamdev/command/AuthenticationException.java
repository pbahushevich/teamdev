package com.epam.teamdev.command;

/**
 * Exception that is thrown when user failed to authenticate authentication 
 *
 */
public class AuthenticationException extends CommandException{

	private static final long serialVersionUID = -7863379201089992784L;

	public AuthenticationException(String msg) {
		super(msg);
	}

	public AuthenticationException(String msg, Exception e) {
		super(msg, e);
	}
	

}
