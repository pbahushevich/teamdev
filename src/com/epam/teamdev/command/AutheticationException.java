package com.epam.teamdev.command;

public class AutheticationException extends CommandException{

	private static final long serialVersionUID = -7863379201089992784L;

	public AutheticationException(String msg) {
		super(msg);
	}

	public AutheticationException(String msg, Exception e) {
		super(msg, e);
	}
	

}
