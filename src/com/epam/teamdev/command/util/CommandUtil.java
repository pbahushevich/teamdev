package com.epam.teamdev.command.util;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.epam.teamdev.command.CommandException;
import com.epam.teamdev.controller.JspPageName;
import com.epam.teamdev.controller.RequestParameterName;
/*
 * Class contains operations that are executed by majority of the commands.
 */
public class CommandUtil {
	
	private static final String WELCOME_PAGE_ADDRESS_STRING = "/controller?command=show_welcome_page";
	private static final String ID_PARAM = "id";

	/**
	 * function used to forward user to his main page showing him a message. Usually used after some data manipulation, 
	 * or after getting some error.
	 * @param request - request sent by user
	 * @param message - message that is shown to the user
	 * @param errorFlag - if true, than the message is an error, and should be displayed in red block. 
	 * Otherwise message is shown in green block.
	 * @return - adress for forwarding to welcome page.
	 */
	public static String getWelcomePageWithMessage(HttpServletRequest request, String message, boolean errorFlag) {

		request.setAttribute(RequestParameterName.USER_MESSAGE, message);
		request.setAttribute(RequestParameterName.ERROR_FLAG, errorFlag);
		
		return WELCOME_PAGE_ADDRESS_STRING;

	}

	/**
	 * convert ISO strings in parameters to UTF-encodinf (in case tomcat hasn't propelry configured to use UTF encoding).
	 * @param value - string in ISO format
	 * @return - String in UTF-8 format 
	 * @throws CommandException in case conversion didn't go well.
	 */
	public static String convertISOtoUTF(String value) throws CommandException {
		try {
			return new String(value.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new CommandException("Unable to parse parameters");
		}
	}

	/**
	 * analog for welcome page, but in case we cann't show welcome page correctly.
	 * @param request - sent request by the user
	 * @param message - message to show the user
	 * @return - address of the error page
	 */
	public static String getErrorPageWithMessage(HttpServletRequest request, String message) {
		request.setAttribute(RequestParameterName.USER_MESSAGE, message);
		request.setAttribute(RequestParameterName.ERROR_FLAG, true);
		return JspPageName.ERROR_PAGE;
	}

	/**
	 * analog for welcome page, but in case the user is not logged in, or his session has expired.
	 * @param request - request sent by user
	 * @param message - message to show the user
	 * @return address of log in page.
	 */
	public static String getLoginPageWithMessage(HttpServletRequest request, String message) {
		request.setAttribute(RequestParameterName.USER_MESSAGE, message);
		request.setAttribute(RequestParameterName.ERROR_FLAG, true);
		return JspPageName.LOGIN_PAGE;
	}

	/**
	 * Function that converts request parameters to Map< Parameter Name,Parameter Value> for processing on the server. 
	 * Usually used for creating new objects.
	 * @param request - request sent by user
	 * @return - map of request parameters
	 * @throws CommandException
	 */
	public static Map<String, String> getSimpleParameterMap(HttpServletRequest request) throws CommandException {

		Map<String, String> attributes = new HashMap<>();

		Enumeration<String> params = request.getParameterNames();

		while (params.hasMoreElements()) {
			String param = params.nextElement();
			String value = request.getParameter(param);
			attributes.put(param, convertISOtoUTF(value));
		}
		return attributes;

	}
	
/**
 * In case we need to save some table data, simple map<String, String> isn't enough to do it. 
 * So this function converts request parameters to presentation <Parameter Name, Array of values>. 
 * If parameter has only one value (such as title or date) than it is stored in array with 1 element.
 * @param request - request sent by the user
 * @return - map or parameters of type <Paramete Name, Array of values>
 * @throws CommandException in case some strings were not converted from ISO to UTF-8
 */
	public static Map<String, String[]> getArrayParameterMap(HttpServletRequest request) throws CommandException {

		Map<String, String[]> attributes = new HashMap<>();

		Enumeration<String> params = request.getParameterNames();

		while (params.hasMoreElements()) {
			String param = params.nextElement();
			String[] values = request.getParameterValues(param);

			for (int i = 0; i < values.length; i++) {
				values[i] = convertISOtoUTF(values[i]);
				attributes.put(param, values);
			}
		}
		return attributes;

	}

	/**
	 * Most of request explicitly require id parameter to be processed. Function get this parameter from request and converts it to Integer. 
	 * @param request - request sent by user
	 * @return - value of id parameter in request
	 * @throws CommandException - in case of absence or illegal value of id parameter
	 */
	public static int getIdParam(HttpServletRequest request) throws CommandException {

		String idParam = request.getParameter(ID_PARAM);
		if (idParam == null) {
			throw new CommandException("Missing required parameter id");
		}

		try {
			int id = Integer.valueOf(idParam);
			return id;
		} catch (NumberFormatException e) {
			throw new CommandException("Can't parse id parameter in request.");
		}
	}

	/**
	 * Function checks the presence of parameter in requests and returns its value by the name of parameter.
	 * @param request - request sent by user
	 * @param paramName - name of parameter
	 * @return - value of parameter
	 * @throws CommandException in case parameter isn't in request.
	 */
	public static String getNotNullParam(HttpServletRequest request, String paramName) throws CommandException {
		String param = request.getParameter(paramName);
		if (param == null) {
			throw new CommandException("Missing required parameter " + paramName);
		}
		return param;

	}

}
