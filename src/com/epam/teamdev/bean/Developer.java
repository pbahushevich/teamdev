package com.epam.teamdev.bean;

import java.io.Serializable;

public class Developer extends User implements Serializable {

	private static final long serialVersionUID = 1L;

	private int userId;
	private Qualification qualification;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Qualification getQualification() {
		return qualification;
	}

	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Developer other = (Developer) obj;
		if (qualification == null) {
			if (other.qualification != null) {
				return false;
			}
		} else if (!qualification.equals(other.qualification)) {
			return false;
		}
		if (userId != other.userId) {
			return false;
		}
		return true;
	}

}
