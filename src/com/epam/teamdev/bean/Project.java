package com.epam.teamdev.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.epam.teamdev.bean.TechTask.TaskWork;

public class Project implements Serializable{
	
	
	private static final long serialVersionUID = 5239445820085874839L;
	
	private int id;
	private String title;
	private Date date;
	private Date dateStart;
	private Date dateEnd;
	private TechTask techTask;
	private Manager manager;
	
	List<ProjectWork> workList;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}


	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	public TechTask getTechTask() {
		return techTask;
	}

	public void setTechTask(TechTask techTask) {
		this.techTask = techTask;
	}

	public List<ProjectWork> getWorkList() {
		return workList;
	}

	public void setWorkList(List<ProjectWork> workList) {
		this.workList = workList;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateEnd == null) ? 0 : dateEnd.hashCode());
		result = prime * result + ((dateStart == null) ? 0 : dateStart.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((techTask == null) ? 0 : techTask.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Project other = (Project) obj;
		if (dateEnd == null) {
			if (other.dateEnd != null) {
				return false;
			}
		} else if (!dateEnd.equals(other.dateEnd)) {
			return false;
		}
		if (date == null) {
			if (other.date != null) {
				return false;
			}
		} else if (!date.equals(other.date)) {
			return false;
		}
		if (dateStart == null) {
			if (other.dateStart != null) {
				return false;
			}
		} else if (!dateStart.equals(other.dateStart)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		if (manager == null) {
			if (other.manager != null) {
				return false;
			}
		} else if (!manager.equals(other.manager)) {
			return false;
		}
		if (techTask == null) {
			if (other.techTask != null) {
				return false;
			}
		} else if (!techTask.equals(other.techTask)) {
			return false;
		}
		return true;
	}

	public class ProjectWork implements Serializable{
		

		private static final long serialVersionUID = -5922252414568666155L;

		private int id;
		private TaskWork taskWork;
		private Developer developer;
		private Date dateStart;
		private Date dateEnd;
		private double timeSpent;
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Project getProject() {
			return Project.this;
		}

		public TaskWork getTaskWork() {
			return taskWork;
		}

		public void setTaskWork(TaskWork taskWork) {
			this.taskWork = taskWork;
		}

		public Developer getDeveloper() {
			return developer;
		}

		public void setDeveloper(Developer developer) {
			this.developer = developer;
		}

		public Date getDateStart() {
			return dateStart;
		}

		public void setDateStart(Date dateStart) {
			this.dateStart = dateStart;
		}

		public Date getDateEnd() {
			return dateEnd;
		}

		public void setDateEnd(Date dateEnd) {
			this.dateEnd = dateEnd;
		}

		public double getTimeSpent() {
			return timeSpent;
		}

		public void setTimeSpent(double timeSpent) {
			this.timeSpent = timeSpent;
		}

	
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getProject().hashCode();
			result = prime * result + ((dateEnd == null) ? 0 : dateEnd.hashCode());
			result = prime * result + ((dateStart == null) ? 0 : dateStart.hashCode());
			result = prime * result + ((developer == null) ? 0 : developer.hashCode());
			result = prime * result + ((taskWork == null) ? 0 : taskWork.hashCode());
			long temp;
			temp = Double.doubleToLongBits(timeSpent);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			ProjectWork other = (ProjectWork) obj;
			if (!getProject().equals(other.getProject())) {
				return false;
			}
			if (dateEnd == null) {
				if (other.dateEnd != null) {
					return false;
				}
			} else if (!dateEnd.equals(other.dateEnd)) {
				return false;
			}
			if (dateStart == null) {
				if (other.dateStart != null) {
					return false;
				}
			} else if (!dateStart.equals(other.dateStart)) {
				return false;
			}
			if (developer == null) {
				if (other.developer != null) {
					return false;
				}
			} else if (!developer.equals(other.developer)) {
				return false;
			}
			if (taskWork == null) {
				if (other.taskWork != null) {
					return false;
				}
			} else if (!taskWork.equals(other.taskWork)) {
				return false;
			}
			if (Double.doubleToLongBits(timeSpent) != Double.doubleToLongBits(other.timeSpent)) {
				return false;
			}
			return true;
		}


	}
}
