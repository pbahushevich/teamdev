package com.epam.teamdev.bean;

import java.util.Date;
/**
 * bean that is used to display grouped information about techtask, attached project and invoice
 *
 */
public class ProjectTaskInvoice {

	private TechTask techTask;
	private Project project;
	private Date dateStart;
	private Date dateEnd;
	private Invoice invoice;
	private Manager manager;
	private Customer customer;

	public TechTask getTechTask() {
		return techTask;
	}

	public void setTechTask(TechTask techTask) {
		this.techTask = techTask;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateEnd == null) ? 0 : dateEnd.hashCode());
		result = prime * result + ((dateStart == null) ? 0 : dateStart.hashCode());
		result = prime * result + ((invoice == null) ? 0 : invoice.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		result = prime * result + ((techTask == null) ? 0 : techTask.hashCode());
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProjectTaskInvoice other = (ProjectTaskInvoice) obj;
		if (dateEnd == null) {
			if (other.dateEnd != null) {
				return false;
			}
		} else if (!dateEnd.equals(other.dateEnd)) {
			return false;
		}
		if (dateStart == null) {
			if (other.dateStart != null) {
				return false;
			}
		} else if (!dateStart.equals(other.dateStart)) {
			return false;
		}
		if (invoice == null) {
			if (other.invoice != null) {
				return false;
			}
		} else if (!invoice.equals(other.invoice)) {
			return false;
		}
		if (techTask == null) {
			if (other.techTask != null) {
				return false;
			}
		} else if (!techTask.equals(other.techTask)) {
			return false;
		}
		if (project == null) {
			if (other.project != null) {
				return false;
			}
		} else if (!project.equals(other.project)) {
			return false;
		}
		if (manager == null) {
			if (other.manager != null) {
				return false;
			}
		} else if (!manager.equals(other.manager)) {
			return false;
		}
		if (customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!manager.equals(other.customer)) {
			return false;
		}
		return true;
	}

}
