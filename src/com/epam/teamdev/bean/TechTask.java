package com.epam.teamdev.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TechTask implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;
	private Date date;
	private Customer customer;

	private List<TaskWork> workList;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<TaskWork> getWorkList() {
		return workList;
	}

	public void setWorkList(List<TaskWork> workList) {
		this.workList = workList;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TechTask other = (TechTask) obj;
		if (customer == null) {
			if (other.customer != null) {
				return false;
			}
		} else if (!customer.equals(other.customer)) {
			return false;
		}
		if (date == null) {
			if (other.date != null) {
				return false;
			}
		} else if (!date.equals(other.date)) {
			return false;
		}
		if (title == null) {
			if (other.title != null) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		return true;
	}


	public class TaskWork implements Serializable{

		private static final long serialVersionUID = -1357439559882555588L;
		
		private int id;
		private WorkType workType;
		private Qualification qualification;
		private int devCount;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public WorkType getWorkType() {
			return workType;
		}
		public void setWorkType(WorkType workType) {
			this.workType = workType;
		}
		public Qualification getQualification() {
			return qualification;
		}
		public void setQualification(Qualification qualification) {
			this.qualification = qualification;
		}
		public int getDevCount() {
			return devCount;
		}
		public void setDevCount(int devCount) {
			this.devCount = devCount;
		}
	
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + devCount;
			result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
			result = prime * result + ((workType == null) ? 0 : workType.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			TaskWork other = (TaskWork) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (devCount != other.devCount) {
				return false;
			}
			if (qualification == null) {
				if (other.qualification != null) {
					return false;
				}
			} else if (!qualification.equals(other.qualification)) {
				return false;
			}
			if (workType == null) {
				if (other.workType != null) {
					return false;
				}
			} else if (!workType.equals(other.workType)) {
				return false;
			}
			return true;
		}
		private TechTask getOuterType() {
			return TechTask.this;
		}
		

	}
}
