index.welcomeMessage = Добрый день. Авторизируйтесь....
index.invalidLoginMessage = Неверные логин / пароль.
index.loginButton= Войти
index.userTitle= Имя пользователя
index.passwordTitle= Пароль

manager.workTypes = Типы работ
manager.newCustomer= Добавить заказчика
manager.customerList= Заказчики
manager.developerList= Разработчики
manager.projectsLabel= Список проектов
manager.techTaskLabel= Список новых техзаданий
manager.techTaskColumn = Тех задание
manager.projectColumn = Проект
manager.dateStartColumn = Дата начала
manager.dateEndColumn = Дата конца
manager.invoiceColumn = Счет
manager.costColumn = Стоимость
manager.numberColumn= №
manager.customerColumn = Заказчик

customer.newTechTaskButton= Новое задание
customer.techTaskLabel = Список технических заданий
customer.techTaskColumn = Техническое задание
customer.dateStartEndColumn =  Дата начала \ конца проекта
customer.invoiceColumn = Счет
customer.managerColumn = Менеджер
customer.invoicesTitle = Неоплеченные счета 
customer.costColumn = Сумма
customer.numberColumn= №

developer.workListLabel = Список работ
developer.numberColumn = №
developer.projectColumn = Проект
developer.managerColumn = Мэнеджер
developer.workTypeColumn = Тип работ
developer.dateStartColumn = Дата начала
developer.dateEndColumn = Дата окончания
developer.spentHours= Потрачено часов
developer.addHoursButton = Внести затраченное время
developer.saveButton = Сохр.

userpage.title = Страница пользователя
userpage.newtitle = Добавить пользователя
userpage.name = Имя
userpage.company= Компания
userpage.department = Отдел
userpage.qualification = Квалификация
userpage.telephone = Телефон
userpage.email = Эл. почта
userpage.login= Логин
userpage.password = Пароль
userpage.confirmPassword = Подтвердите пароль
userpage.addButton = Сохранить
userpage.resetButton = Сброс
userpage.deleteButton = Удалить

newcustomer.title = Добавить заказчика
newcustomer.name = Имя
newcustomer.telephone = Телефон
newcustomer.company= Компания
newcustomer.email = Эл. почта
newcustomer.login= Логин
newcustomer.password = Пароль
newcustomer.confirmPassword = Подтвердите пароль
newcustomer.addButton = Добавить заказчика
newcustomer.resetButton = Сброс

worktypelist.worktypeListLabel= Список видов работ	
worktypelist.numberColumn= №
worktypelist.workTypeColumn= Вид работ
worktypelist.newWorkType= Новый вид работ
worktypelist.addButton = Добавить

customerlist.customerListLabel= Список заказчиков
customerlist.numberColumn= №
customerlist.nameColumn= Имя
customerlist.companyColumn= Компания
customerlist.telephoneColumn= Телефон
customerlist.emailColumn= Эл. почта
customerlist.login= Логин
customerlist.newCustomer= Создать

techtask.pageTitle = Техническое задание
techtask.techtaskTitle= Название
techtask.date= Дата
techtask.workListLabel = Список работ
techtask.numberColumn= №
techtask.workTypeColumn = Тип работ
techtask.qualificationColumn = Квалификация
techtask.devcountColumn = Количество разработчиков
techtask.saveButton= Сохранить
techtask.resetButton= Сбросить
techtask.editButton= Редактировать
techtask.deleteButton= Удалить
techtask.createProjectButton= Создать проект
techtask.projectExistTitle= По данному тз создан проект: 

project.pageTitle = Проект
project.projectTitle= Название
project.date= Дата
project.workListLabel = Список работ по проекту
project.numberColumn= №
project.workTypeColumn = Тип работ
project.qualificationColumn = Квалификация
project.dateColumn = Даты начала-конца
project.developerColumn = Разработчик
project.saveButton= Сохранить
project.resetButton= Сбросить
project.editButton= Редактировать
project.deleteButton= Удалить
project.projectStartedTitle= Работа по данному проекту уже началась 
project.techTaskTitle = Техническое задание: 
project.chooseDatesTitle = 	Выберите даты...   
project.timeSpentColumn = Затраченное время   
project.createInvoiceButton = Выставить счет
project.invoiceCreatedLabel = Нельзя редактировать проект после того как выставлен счет...   
project.workStartedLabel = Работа по данному проекту уже началась...  


invoice.pageTitle = Счет
invoice.number= Номер
invoice.date= Дата
invoice.workListLabel = Список работ по проекту
invoice.numberColumn= №
invoice.workTypeColumn = Тип работ
invoice.qualificationColumn = Квалификация
invoice.dateColumn = Даты начала-конца
invoice.developerColumn = Разработчик
invoice.timeSpentColumn = Затраченное время   
invoice.saveButton= Сохранить
invoice.editButton= Редактировать
invoice.deleteButton= Удалить
invoice.setPaidButton = Пометить как оплаченный
invoice.setUnPaidButton = Отмена оплаты
invoice.price= Стоимость

devlist.pageTitle = Список разработчиков
devlist.devListLabel = Список разработчиков
devlist.numberColumn = №
devlist.projectColumn = Проект
devlist.workTypeColumn = Тип работ
devlist.qualificationColumn = Квалификация
devlist.dateColumn = Даты начала-конца
devlist.developerColumn = Разработчик
devlist.timeSpentColumn = Затраченное время   
devlist.addDeveloperButton = Добавить нового разработчика   


calendar.calendarParams ="locale": {"format": "DD.MM.YYYY","separator": " - ", "applyLabel": "\u041F\u0440\u0438\u043D\u044F\u0442\u044C", "cancelLabel": "\u041E\u0442\u043C\u0435\u043D\u0430", "fromLabel": "\u041E\u0442", "toLabel": "\u0414\u043E", "customRangeLabel": "\u041F\u0440\u043E\u0438\u0437\u0432\u043E\u043B\u044C\u043D\u044B\u0439", "daysOfWeek": ["\u0412\u0441","\u041F\u043D","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041F\u0442","\u0421\u0431"], "monthNames": ["\u042F\u043D\u0432\u0430\u0440\u044C", "\u0424\u0435\u0432\u0440\u0430\u043B\u044C", "\u041C\u0430\u0440\u0442", "\u0410\u043F\u0440\u0435\u043B","\u041C\u0430\u0439", "\u0418\u044E\u043D\u044C", "\u0418\u044E\u043B\u044C","\u0410\u0432\u0433\u0443\u0441\u0442", "\u0421\u0435\u043D\u0442\u044F\u0431\u0440\u044C", "\u041E\u043A\u0442\u044F\u0431\u0440\u044C","\u041D\u043E\u044F\u0431\u0440\u044C", "\u0414\u0435\u043A\u0430\u0431\u0440\u044C" ],"firstDay": 1 }
calendar.dateTitle = \u0414\u0430\u0442\u0430