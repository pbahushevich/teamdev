<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<%@ page import="com.epam.teamdev.bean.Role"%>
<html>
<head>

<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<c:set var="totalTime" value="0" />
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="pageTitle" key="devlist.pageTitle" />
<fmt:message bundle="${loc }" var="projectTitle" key="devlist.projectTitle" />
<fmt:message bundle="${loc }" var="addDeveloperButton" key="devlist.addDeveloperButton" />
<fmt:message bundle="${loc }" var="devListLabel" key="devlist.devListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="devlist.numberColumn" />
<fmt:message bundle="${loc }" var="projectColumn" key="devlist.projectColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="devlist.workTypeColumn" />
<fmt:message bundle="${loc }" var="qualificationColumn" key="devlist.qualificationColumn" />
<fmt:message bundle="${loc }" var="dateColumn" key="devlist.dateColumn" />
<fmt:message bundle="${loc }" var="developerColumn" key="devlist.developerColumn" />
<fmt:message bundle="${loc }" var="techTaskTitle" key="devlist.techTaskTitle" />
<fmt:message bundle="${loc }" var="timeSpentColumn" key="devlist.timeSpentColumn" />
<fmt:message bundle="${loc }" var="dateLocale" key="local.dateLocale" />

<c:set var="page_addr" value="/controller?command=show_devlist" />
<c:if test="${readonly}">
	<c:set var="ro_string" value="readonly" />
</c:if>
<c:if test="${newProject}">
	<c:set var="page_addr" value="/controller?command=new_project" />
</c:if>
<fmt:formatDate value="${project.dateStart}" var="dateStart" pattern="dd.MM.YYYY" />
<fmt:formatDate value="${project.dateEnd}" var="dateEnd" pattern="dd.MM.YYYY" />
<fmt:formatDate value="${project.date}" var="projectDate" pattern="dd.MM.YYYY" />
<c:set var="editDate" value="false" />
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/daterangepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>


<title>Developers</title>
<style type="text/css">
</style>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row" style="margin-bottom: 5px">
			<div class="col-lg-9">
				<form name="devlist" action="${pageContext.request.contextPath}/controller" method="POST">
					<input type="hidden" name="command" value="new_user">
					<input type="hidden" name="role" value="developer">
					<button type="submit" class="btn btn-success ">
						<span class="glyphicon glyphicon-user"></span> ${addDeveloperButton}
					</button>
				</form>
			</div>
			<div class="col-lg-3">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<div class="row"></div>
		<div class="clearfix visible-md-block"></div>
		<div class="row" style="height: 5px"></div>

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">${devListLabel }</div>
					<table id="workList" class="table table-striped">
						<thead>
							<tr>
								<th style="width: 5%">${numberColumn}</th>
								<th>${developerColumn}</th>
								<th>${qualificationColumn}</th>
								<th>${projectColumn}</th>
								<th>${workTypeColumn}</th>
								<th>${dateColumn}</th>
								<th style="width: 10%">${timeSpentColumn}</th>
							</tr>
						</thead>
						<tbody>

							<c:forEach items="${workList}" var="work" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
										<c:if test="${curDeveloper!=work.developer}">
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_user">
											<input type="hidden" name="id" value="${work.developer.userId}">
											<a href="#${work.developer.name}" onclick="$(this).closest('form').submit()">${work.developer.name}</a>
										</form>
									</c:if>

									</td>
									<td>
										<c:if test="${curDeveloper!=work.developer}">
											${work.developer.qualification.title}</c:if>
									</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_project">
											<input type="hidden" name="id" value="${work.project.id}">
											<a href="#${work.project.title}" onclick="$(this).closest('form').submit()">${work.project.title}</a>
										</form>
									</td>
									<td>${work.taskWork.workType.title}</td>
									<td>
										<fmt:formatDate value="${work.dateStart}" pattern="dd.MM.YYYY" />
										-
										<fmt:formatDate value="${work.dateEnd}" pattern="dd.MM.YYYY" />
									</td>
									<td>${work.timeSpent}</td>
								</tr>
								<c:set var="curDeveloper" value="${work.developer}" />
							</c:forEach>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>

</body>

</html>