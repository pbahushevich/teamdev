<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<%@ page import="com.epam.teamdev.bean.Role"%>
<html>
<head>

<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<c:set var="totalTime" value="0" />
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="pageTitle" key="project.pageTitle" />
<fmt:message bundle="${loc }" var="projectTitle" key="project.projectTitle" />
<fmt:message bundle="${loc }" var="date" key="project.date" />
<fmt:message bundle="${loc }" var="workListLabel" key="project.workListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="project.numberColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="project.workTypeColumn" />
<fmt:message bundle="${loc }" var="qualificationColumn" key="project.qualificationColumn" />
<fmt:message bundle="${loc }" var="dateColumn" key="project.dateColumn" />
<fmt:message bundle="${loc }" var="developerColumn" key="project.developerColumn" />
<fmt:message bundle="${loc }" var="saveButton" key="project.saveButton" />
<fmt:message bundle="${loc }" var="resetButton" key="project.resetButton" />
<fmt:message bundle="${loc }" var="deleteButton" key="project.deleteButton" />
<fmt:message bundle="${loc }" var="editButton" key="project.editButton" />
<fmt:message bundle="${loc }" var="projectStartedTitle" key="project.projectStartedTitle" />
<fmt:message bundle="${loc }" var="techTaskTitle" key="project.techTaskTitle" />
<fmt:message bundle="${loc }" var="timeSpentColumn" key="project.timeSpentColumn" />
<fmt:message bundle="${loc }" var="createInvoiceButton" key="project.createInvoiceButton" />
<fmt:message bundle="${loc }" var="invoiceCreatedLabel" key="project.invoiceCreatedLabel" />
<fmt:message bundle="${loc }" var="workStartedLabel" key="project.workStartedLabel" />
<fmt:message bundle="${loc }" var="dateLocale" key="local.dateLocale" />

<c:set var="page_addr" value="/controller?command=show_project&id=${project.id }" />
<c:if test="${readonly}">
	<c:set var="ro_string" value="readonly" />
</c:if>
<c:if test="${newProject}">
	<c:set var="page_addr" value="/controller?command=new_project" />
</c:if>
<fmt:formatDate value="${project.dateStart}" var="dateStart" pattern="dd.MM.YYYY" />
<fmt:formatDate value="${project.dateEnd}" var="dateEnd" pattern="dd.MM.YYYY" />
<fmt:formatDate value="${project.date}" var="projectDate" pattern="dd.MM.YYYY" />
<c:set var="editDate" value="false" />
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/daterangepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>


<title>Project</title>
<style type="text/css">
</style>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row" style="margin-bottom: 5px">
			<div class="col-lg-12">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<form name="project" action="${pageContext.request.contextPath}/controller" method="POST">
			<input type="hidden" name="command" value="save_project" />
			<div class="row">
				<div class="col-lg-9">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-italic"></span></span>
						<input type="text" name="title" class="form-control" placeholder="${projectTitle}" value="${project.title}" ${readonly ? "readonly" :""} required>

					</div>
				</div>
				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="projectDate" name="projectDate" type="single" locale="${sessionScope.locale}" readonly="${readonly}" value="${projectDate}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix visible-md-block"></div>
			<div class="row" style="height: 5px"></div>

			<div class="row" style="margin-bottom: 10px">
				<div class="col-lg-5">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
						<input type="text" class="form-control" placeholder="${customerTitle}" value="${project.techTask.customer.name}" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon  glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="${managerTitle}" value="${project.manager.name}" readonly>
					</div>
				</div>


				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="projectStartEnd" name="projectStartEnd" type="range" locale="${sessionScope.locale}" readonly="${readonly}" value="${dateStart} - ${dateEnd}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">${workListLabel }</div>
						<table id="workList" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 5%">${numberColumn}</th>
									<th>${workTypeColumn}</th>
									<th>${qualificationColumn}</th>
									<th>${dateColumn}</th>
									<th>${developerColumn}</th>
									<th>${timeSpentColumn}</th>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${workList}" var="projectWork" varStatus="loop">
									<tr>
										<td>${loop.count}</td>
										<td>${projectWork.taskWork.workType.title}</td>
										<td>${projectWork.taskWork.qualification.title}</td>

										<td>
											<fmt:formatDate value="${projectWork.dateStart}" pattern="dd.MM.YYYY" />
											-
											<fmt:formatDate value="${projectWork.dateEnd}" pattern="dd.MM.YYYY" />
										</td>
										<td>${projectWork.developer.name}</td>
										<td>${projectWork.timeSpent}</td>
									</tr>
									<c:set var="totalTime" value="${totalTime+ projectWork.timeSpent}" />
								</c:forEach>

							</tbody>
						</table>

					</div>
				</div>
			</div>
		</form>
		<div class="row">
			<div class="col-lg-6" style="float: left">
				<div style="display: table-cell; float: left;">
					<form method="POST" action="${pageContext.request.contextPath}/controller">
						<input type="hidden" name="command" value="show_techtask" />
						<input type="hidden" name="id" value="${project.techTask.id}" />
						${techTaskTitle} <a href="#${project.techTask.title}" onclick="$(this).closest('form').submit()">${project.techTask.title} </a>
					</form>
				</div>
			</div>
			<c:if test="${user.role == Role.MANAGER}">
				<c:choose>
					<c:when test="${totalTime==0}">
						<div class="col-lg-6" style="float: left">
							<div style="display: table-cell; float: right;">
								<form method="post" action="${pageContext.request.contextPath}/controller">
									<input type="hidden" name="command" value="delete_project" />
									<input type="hidden" name="id" value="${project.id}" />
									<button type="submit" class="btn btn-danger">${deleteButton}</button>
								</form>
							</div>
							<div style="display: table-cell; float: right;">
								<form method="post" action="${pageContext.request.contextPath}/controller">
									<input type="hidden" name="command" value="edit_project" />
									<input type="hidden" name="id" value="${project.id}" />
									<button type="submit" class="btn btn-primary">${editButton}</button>
								</form>
							</div>
						</div>
					</c:when>
					<c:when test="${totalTime>0 && empty invoice}">
						<div class="col-lg-6" style="float: right">
							<div style="float: right">${workStartedLabel}</div>
							<br>
							<div style="float: right;">
								<form method="post" action="${pageContext.request.contextPath}/controller">
									<input type="hidden" name="command" value="new_invoice" />
									<input type="hidden" name="id" value="${project.id}" />
									<button type="submit" class="btn btn-success">${createInvoiceButton}</button>
								</form>
							</div>
							<div style="display: table-cell; float: right;">
								<form method="post" action="${pageContext.request.contextPath}/controller">
									<input type="hidden" name="command" value="edit_project" />
									<input type="hidden" name="id" value="${project.id}" />
									<button type="submit" class="btn btn-primary">${editButton}</button>
								</form>
							</div>
						</div>
					</c:when>
					<c:when test="${invoice!=null}">
						<div class="col-lg-6" style="float: right">
							<div style="float: right;display:table-row">${invoiceCreatedLabel}
							<td:invoiceRef invoice="${invoice}"/>
							</div>
							<br>
						</div>
					</c:when>
				</c:choose>
			</c:if>
		</div>
	</div>

</body>

</html>