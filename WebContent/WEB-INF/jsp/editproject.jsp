<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>

<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="pageTitle" key="project.pageTitle" />
<fmt:message bundle="${loc }" var="projectTitle" key="project.projectTitle" />
<fmt:message bundle="${loc }" var="date" key="project.date" />
<fmt:message bundle="${loc }" var="workListLabel" key="project.workListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="project.numberColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="project.workTypeColumn" />
<fmt:message bundle="${loc }" var="qualificationColumn" key="project.qualificationColumn" />
<fmt:message bundle="${loc }" var="dateColumn" key="project.dateColumn" />
<fmt:message bundle="${loc }" var="developerColumn" key="project.developerColumn" />
<fmt:message bundle="${loc }" var="saveButton" key="project.saveButton" />
<fmt:message bundle="${loc }" var="resetButton" key="project.resetButton" />
<fmt:message bundle="${loc }" var="deleteButton" key="project.deleteButton" />
<fmt:message bundle="${loc }" var="editButton" key="project.editButton" />
<fmt:message bundle="${loc }" var="timeSpentColumn" key="project.timeSpentColumn" />
<fmt:message bundle="${loc }" var="projectStartedTitle" key="project.projectStartedTitle" />
<fmt:message bundle="${loc }" var="chooseDatesTitle" key="project.chooseDatesTitle" />
<fmt:message bundle="${loc }" var="dateLocale" key="local.dateLocale" />

<c:set var="newproject" value="true" />
<c:if test="${project!=null}">
</c:if>
<c:set var="readonly" value="false" />
<c:set var="page_addr" value="/controller?command=new_project&task_id=3" />
<c:set var="newproject" value="false" />
<c:set var="page_addr" value="/controller?command=edit_project&id=${project.id}" />
<c:set var="title" value="${project.title}" />
<c:set var="customer" value="${project.techTask.customer.name}" />
<fmt:formatDate value="${project.dateStart}" var="dateStart" pattern="dd.MM.YYYY" />
<fmt:formatDate value="${project.dateEnd}" var="dateEnd" pattern="dd.MM.YYYY" />
<fmt:formatDate value="${project.date}" var="projectDate" pattern="dd.MM.YYYY" />

<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/daterangepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>


<title>Project</title>
<style type="text/css">
</style>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row" style="margin-bottom: 5px">
			<div class="col-lg-11">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<form name="project" action="${pageContext.request.contextPath}/controller" method="POST">
			<input type="hidden" name="command" value="update_project" />
			<input type="hidden" name="task_id" value="${project.techTask.id }">
			<input type="hidden" name="project_id" value="${project.id }">
			<input type="hidden" name="rowCount" id="rowCount" value="0">
			<div class="row">
				<div class="col-lg-8">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-italic"></span></span>
						<input type="text" name="title" class="form-control" placeholder="${projectTitle}" value="${title}" ${readonly ? "readonly" :""} required>

					</div>
				</div>

				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="projectDate" name="projectDate" type="single" locale="${sessionScope.locale}" readonly="${readonly}" value="${projectDate}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix visible-md-block"></div>
			<div class="row" style="height: 5px"></div>

			<div class="row" style="margin-bottom: 10px">
				<div class="col-lg-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="${customerTitle}" value="${customer}" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-education"></span></span>
						<input type="text" class="form-control" placeholder="${managerTitle}" value="${user.name}" readonly>
					</div>
				</div>


				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="projectStartEnd" name="projectStartEnd" type="range" locale="${sessionScope.locale}" readonly="${readonly}" value="${dateStart} - ${dateEnd}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-11">
					<div class="panel panel-default">
						<div class="panel-heading">${workListLabel }</div>
						<table id="workList" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 5%">${numberColumn}</th>
									<th>${workTypeColumn}</th>
									<th>${qualificationColumn}</th>
									<th>${dateColumn}</th>
									<th>${developerColumn}</th>
									<th style="width: 10%">${timeSpentColumn}</th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${!newproject}">
									<c:forEach items="${workList}" var="projectWork" varStatus="loop">
										<tr>
											<c:set var="rowNumber" value="${loop.count}"></c:set>
											<td>${loop.count}
											<input type="hidden" name="projectWorkId" value="${projectWork.id}">
											<input type="hidden" name="taskWorkId" value="${projectWork.taskWork.id}">
											</td>
											<td>${projectWork.taskWork.workType.title}</td>
											<td>${projectWork.taskWork.qualification.title}<input type="hidden" id="qid_${rowNumber}" value="${projectWork.taskWork.qualification.id}">
											</td>
											<td>
												<fmt:formatDate value="${projectWork.dateStart}" var="workDateStart" pattern="dd.MM.YYYY" />
												<fmt:formatDate value="${projectWork.dateEnd}" var="workDateEnd" pattern="dd.MM.YYYY" />
												<input type="hidden" id="activeDevId_${rowNumber}" value="${projectWork.developer.id}">
												<input type="hidden" id="activeDevName_${rowNumber}" value="${projectWork.developer.name}">
												<td:calendar locale="${locale}" id="workStartEnd_${rowNumber }" type="range" name="workStartEnd" value="${workDateStart} - ${workDateEnd}" />
												<script type="text/javascript">
																$('#workStartEnd_${rowNumber}').on('apply.daterangepicker', function(ev, picker) {
																	getDeveloperList(${rowNumber});});
																</script>
											</td>
											<td>
												<select name="developer_id" id="devList_${rowNumber}" class="selectpicker" onclick="getDeveloperList(${rowNumber})" required>
													<option value="${projectWork.developer.id}">${projectWork.developer.name}</option>
												</select>
											</td>
											<td>
												<input type="text" name = "timeSpent"  id="timeSpent_${rowNumber}" class="form-control" value="${projectWork.timeSpent}" pattern="[\d]+(\.[\d]+)?" title="0.0-999">
											</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-1  col-lg-offset-10" style="float: left">
					<div style="display: table-cell; float: right;">
						<input type="submit" class="btn btn-success" value="${saveButton}">
					</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript">
	window.onload = function() {
	    var rowCountElement = document.getElementById("rowCount");
		rowCountElement.value = document.getElementById("workList").rows.length-1;
		var table  = document.getElementById("workList");
		for (var i =1, row; row = table.rows[i]; i++) {
			getDeveloperList(i)
		}
	
	};	
	  </script>

	<script type="text/javascript">
function getDeveloperList(rowNumber){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
				 if (xhttp.readyState == 4 && xhttp.status == 200) {
				 		var elSelect  = document.getElementById("devList_"+rowNumber);
					 elSelect.innerHTML = xhttp.responseText;
					//	alert(xhttp.responseText); 
					var activeDevId = document.getElementById('activeDevId_'+rowNumber).value;
					//	var activeDevName = document.getElementById('activeDevName_'+rowNumber).value;
					//	elSelect.options[elSelect.options.length] = new Option(activeDevName, activeDevId);				 
					 	elSelect.value = activeDevId;
					 $('.selectpicker').selectpicker('refresh');}};

		var dates = document.getElementById("workStartEnd_"+rowNumber).value;
		var qid = document.getElementById("qid_"+rowNumber).value;
		xhttp.open("GET", 
				"controller?command=get_available_devs&qualification_id="
						+qid+"&dateRange="+dates+"&projectId="+${project.id}, true);
		xhttp.send();	
}
</script>
</body>

</html>