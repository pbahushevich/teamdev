<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>

<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="pageTitle" key="project.pageTitle" />
<fmt:message bundle="${loc }" var="projectTitle" key="project.projectTitle" />
<fmt:message bundle="${loc }" var="date" key="project.date" />
<fmt:message bundle="${loc }" var="workListLabel" key="project.workListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="project.numberColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="project.workTypeColumn" />
<fmt:message bundle="${loc }" var="qualificationColumn" key="project.qualificationColumn" />
<fmt:message bundle="${loc }" var="dateColumn" key="project.dateColumn" />
<fmt:message bundle="${loc }" var="developerColumn" key="project.developerColumn" />
<fmt:message bundle="${loc }" var="saveButton" key="project.saveButton" />
<fmt:message bundle="${loc }" var="resetButton" key="project.resetButton" />
<fmt:message bundle="${loc }" var="deleteButton" key="project.deleteButton" />
<fmt:message bundle="${loc }" var="editButton" key="project.editButton" />
<fmt:message bundle="${loc }" var="timeSpentColumn" key="project.timeSpentColumn" />
<fmt:message bundle="${loc }" var="projectStartedTitle" key="project.projectStartedTitle" />
<fmt:message bundle="${loc }" var="chooseDatesTitle" key="project.chooseDatesTitle" />
<fmt:message bundle="${loc }" var="dateLocale" key="local.dateLocale" />

<c:set var="newproject" value="true" />
<c:if test="${project!=null}">
</c:if>
<c:set var="readonly" value="false" />
<c:set var="page_addr" value="/controller?command=new_project&task_id=3" />
<c:if test="${readonly}">
	<c:set var="ro_string" value="readonly" />
</c:if>
<c:choose>
<c:when test="${project==null}">
	<c:set var="newproject" value="true" />
	<c:set var="page_addr" value="/controller?command=new_project&id=${techTask.id}" />
	<c:set var="title" value="${techTask.title}" />
	<c:set var="customer" value="${techTask.customer.name}" />
	<fmt:formatDate value="${techTask.date}" var="dateStart" pattern="dd.MM.YYYY" />
	<fmt:formatDate value="${today}" var="dateEnd" pattern="dd.MM.YYYY" />
	<fmt:formatDate value="${techTask.date}" var="projectDate" pattern="dd.MM.YYYY" />
</c:when>
<c:otherwise>
	<c:set var="newproject" value="false" />
	<c:set var="page_addr" value="/controller?command=edit_project&id=${project.id}" />
	<c:set var="title" value="${project.title}" />
	<c:set var="customer" value="${project.techTask.customer.name}" />
	<fmt:formatDate value="${project.dateStart}" var="dateStart" pattern="dd.MM.YYYY" />
	<fmt:formatDate value="${project.dateEnd}" var="dateEnd" pattern="dd.MM.YYYY" />
	<fmt:formatDate value="${project.date}" var="projectDate" pattern="dd.MM.YYYY" />


</c:otherwise>
</c:choose>

<c:set var="editDate" value="false" />

<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/daterangepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>


<title>Project</title>
<style type="text/css">
</style>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row" style="margin-bottom: 5px">
			<div class="col-lg-11">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<form name="project" action="${pageContext.request.contextPath}/controller" method="POST">
			<input type="hidden" name="command" value="save_project" />
			<input type="hidden" name="task_id" value="${techTask.id }">
			<input type="hidden" name="rowCount" id="rowCount" value="0">
			<div class="row">
				<div class="col-lg-8">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-italic"></span></span>
						<input type="text" name="title" class="form-control" placeholder="${projectTitle}" value="${title} project" ${readonly ? "readonly" :""} required>

					</div>
				</div>

				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="projectDate" name="projectDate" type="single" locale="${sessionScope.locale}" readonly="${readonly}" value="${projectDate}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix visible-md-block"></div>
			<div class="row" style="height: 5px"></div>

			<div class="row" style="margin-bottom: 10px">
				<div class="col-lg-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
						<input type="text" class="form-control" placeholder="${customerTitle}" value="${customer}" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-education"></span></span>
						<input type="text" class="form-control" placeholder="${managerTitle}" value="${user.name}" readonly>
					</div>
				</div>


				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="projectStartEnd" name="projectStartEnd" type="range" locale="${sessionScope.locale}" readonly="${readonly}" value="${dateStart} - ${dateEnd}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-11">
					<div class="panel panel-default">
						<div class="panel-heading">${workListLabel }</div>
						<table id="workList" class="table table-striped">
							<thead>
								<tr>
									<th style="width: 5%">${numberColumn}</th>
									<th>${workTypeColumn}</th>
									<th>${qualificationColumn}</th>
									<th>${dateColumn}</th>
									<th>${developerColumn}</th>
								</tr>
							</thead>
							<tbody>
									<c:forEach items="${workList}" var="taskWork" varStatus="loop">
										<c:forEach begin="1" end="${taskWork.devCount }" varStatus="innerLoop">
											<tr>
												<c:set var="rowNumber" value="${loop.count}${innerLoop.count}"></c:set>
												<td>${loop.count}.${innerLoop.count}<input type="hidden" name="taskWorkId" value="${taskWork.id}">
												</td>
												<td>${taskWork.workType.title}</td>
												<td>${taskWork.qualification.title}<input type="hidden" id="qid_${rowNumber}" value="${taskWork.qualification.id}">
												</td>
												<td>
													<td:calendar locale="${locale}" id="workStartEnd_${rowNumber }" type="range" name="workStartEnd" />
													<script type="text/javascript">
																$('#workStartEnd_${rowNumber}').on('apply.daterangepicker', function(ev, picker) {
																	getDeveloperList(${rowNumber});});
																</script>
												</td>
												<td>
													<select name="developer_id" id="devList_${rowNumber}" class="selectpicker" required="required">
														<option>${chooseDatesTitle}</option>
													</select>
												</td>
											</tr>
										</c:forEach>
									</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-1  col-lg-offset-10" style="float: left">
					<div style="display: table-cell; float: right;">
						<input type="submit" class="btn btn-success" value="${saveButton}">
					</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript">
	window.onload = function() {
	    var rowCountElement = document.getElementById("rowCount");
		rowCountElement.value = document.getElementById("workList").rows.length-1;
	  };	
	  </script>

	<script type="text/javascript">
function getDeveloperList(rowNumber){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
				 if (xhttp.readyState == 4 && xhttp.status == 200) {
				 		var elSelect  = document.getElementById("devList_"+rowNumber);
					 elSelect.innerHTML = xhttp.responseText;
					 $('.selectpicker').selectpicker('refresh');}};
				<!--  alert(document.getElementById("workStartEnd_"+rowNumber));  alert(document.getElementById("qid_"+rowNumber)); 	-->
		var dates = document.getElementById("workStartEnd_"+rowNumber).value;
		var qid = document.getElementById("qid_"+rowNumber).value;
		xhttp.open("GET", 
				"controller?command=get_available_devs&qualification_id="+qid
						+"&dateRange="+dates+"&projectId=0", true);
		xhttp.send();	
	
}
</script>
</body>




</html>