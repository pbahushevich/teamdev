<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<select id ="${select_id}" name ="${select.id}">
<c:forEach items="${developers}" var="developer">
	<option value="${developer.id}"> ${developer.name}</option>
</c:forEach>
</select> 