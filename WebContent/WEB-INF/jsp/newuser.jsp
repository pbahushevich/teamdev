<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.epam.teamdev.bean.Role"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>

<c:set var="page_addr" value="/controller?command=new_user&role=${role}" />
<c:if test="${readonly&&user==sessionScope.user}">
	<c:set var="reaonly" value="false" />
</c:if>
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc}" key="userpage.name" var="name" />
<fmt:message bundle="${loc}" key="userpage.company" var="company" />
<fmt:message bundle="${loc}" key="userpage.telephone" var="telephone" />
<fmt:message bundle="${loc}" key="userpage.email" var="email" />
<fmt:message bundle="${loc}" key="userpage.login" var="login" />
<fmt:message bundle="${loc}" key="userpage.qualification" var="qualification" />
<fmt:message bundle="${loc}" key="userpage.department" var="department" />
<fmt:message bundle="${loc}" key="userpage.password" var="password" />
<fmt:message bundle="${loc}" key="userpage.confirmPassword" var="confirmPassword" />
<fmt:message bundle="${loc}" key="userpage.addButton" var="addButton" />
<fmt:message bundle="${loc}" key="userpage.deleteButton" var="deleteButton" />
<fmt:message bundle="${loc}" key="userpage.cancelButton" var="cancelButton" />
<fmt:message bundle="${loc}" key="userpage.title" var="title" />
<fmt:message bundle="${loc}" key="userpage.resetButton" var="resetButton" />


<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>
<c:if test="${readonly}">
	<script type="text/javascript">
		window.onload = function() {
			$('#userForm input').attr('readonly', 'readonly');
		}
	</script>
</c:if>
<style>
h1 {
	margin: 30px;
	padding: 0 50px 1px 0;
	border-bottom: 1px solid #E5E5E5;
}
</style>
<title>User</title>
</head>
<body>
	<div class="bs-example">
		<div class="container">
			<div class="col-lg-6">
				<h1>${title}</h1>
			</div>
			<div class="col-lg-4" style="margin: 30px">
				<%@ include file="localebuttons.jsp"%>
			</div>
		</div>
		<div class="container">

			<td:welcomeMessageTag text="${userMessage}" error="${error}" />

			<form id="userForm" class="form-horizontal" action="${pageContext.request.contextPath}/controller" method="POST">
				<div class="form-group">
					<label class="control-label col-lg-2" for="name">${name}</label>
					<div class="col-lg-4">
						<input type="text" name="name" class="form-control" id="name" placeholder="${name }" required>
					</div>
				</div>

				<c:choose>
					<c:when test="${role == Role.CUSTOMER}">
						<div class="form-group">
							<label class="control-label col-lg-2" for="lastName">${company }</label>
							<div class="col-lg-4">
								<input type="text" name="company" class="form-control" id="company" placeholder="${company }" required>
							</div>
						</div>
					</c:when>
					<c:when test="${role == Role.MANAGER}">
						<div class="form-group">
							<label class="control-label col-lg-2" for="lastName">${department }</label>
							<div class="col-lg-4">
								<input type="text" name="company" class="form-control" id="company" placeholder="${department }" required>
							</div>
						</div>
					</c:when>
					<c:when test="${role == Role.DEVELOPER}">
						<div class="form-group">
							<label class="control-label col-lg-2" for="lastName">${qualification}</label>
							<div class="col-lg-4">
								<select  name="qualification_id"  class="selectpicker" class="form-control" data-width="100%">
									<c:forEach items="${qualificationList}" var="qualificationEl">
										<option value="${qualificationEl.id }">${qualificationEl.title}</option>
									</c:forEach>
								</select>
							</div>
						</div>
					</c:when>
				</c:choose>
				<div class="form-group">
					<label class="control-label col-lg-2" for="telephone">${telephone }</label>
					<div class="col-lg-4">
						<input type="text" name="telephone" class="form-control" id="telephone" placeholder="${telephone }" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2" for="email">${email }</label>
					<div class="col-lg-4">
						<input type="text" name="email" class="form-control" id="email" placeholder="${email }" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="${email}">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2" for="login">${login }</label>
					<div class="col-lg-4">
						<input type="text" name="login" class="form-control" id="login" placeholder="${login }" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2" for="password">${password }</label>
					<div class="col-lg-4">
						<input type="password" name="password" class="form-control" id="password" placeholder="${password }" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2" for="password">${confirmPassword }</label>
					<div class="col-lg-4">
						<input type="password" name="confirmPassword" class="form-control" id="confirmPassword" placeholder="${confirmPassword }" required>
					</div>
				</div>

				<br>
				<div class="form-group">
					<div class="col-lg-offset-3 col-lg-9">
						<input type="hidden" name="command" value="save_user">
						<input type="hidden" name="role" value="${role}">
						<input type="submit" class="btn btn-primary" value="${addButton}">
						<input type="reset" class="btn btn-default" value="${resetButton}">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>