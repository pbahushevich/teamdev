<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>
<c:set var="page_addr" value="/controller?command=show_customer_list" />
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="customerListLabel" key="customerlist.customerListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="customerlist.numberColumn" />
<fmt:message bundle="${loc }" var="nameColumn" key="customerlist.nameColumn" />
<fmt:message bundle="${loc }" var="companyColumn" key="customerlist.companyColumn" />
<fmt:message bundle="${loc }" var="telephoneColumn" key="customerlist.telephoneColumn" />
<fmt:message bundle="${loc }" var="emailColumn" key="customerlist.emailColumn" />
<fmt:message bundle="${loc }" var="login" key="customerlist.login" />
<fmt:message bundle="${loc }" var="newCustomer" key="customerlist.newCustomer" />


<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/token.js"></script>
<script src="${pageContext.request.contextPath}/js/token.js"></script>
<title>Customer list</title>

</head>
<body>
	<div class="container" style="margin: 30px">

		<div class="row">
			<div class="col-lg-12">
				<td:welcomeMessageTag text="${requestScope.userMessage}" error="${error}" />
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<form name="newCustomer" method="post" action="${pageContext.request.contextPath}/controller">
					<input type="hidden" name="command" value="new_user">
					<input type="hidden" name="role" value="customer">
					<button type="submit" class="btn btn-success ">
						<span class="glyphicon glyphicon-briefcase"></span> ${newCustomer}
					</button>
				</form>
			</div>
			<div class="col-lg-6">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<div class="clearfix visible-md-block"></div>
		<div class="row" style="height: 10px"></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">${customerListLabel }</div>
					<table class="table table-striped">
						<thead>
							<tr>

								<th style="width: 5%">${numberColumn}</th>
								<th>${nameColumn}</th>
								<th>${companyColumn}</th>
								<th>${telephoneColumn}</th>
								<th>${emailColumn}</th>
								<th>${login}</th>
								<th></th>

							</tr>
						</thead>
						<tbody>
							<c:forEach items="${customerList }" var="customer" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
										<form action="${pageContext.request.contextPath}/controller" method="POST">
											<input type="hidden" name="command" value="show_user">
											<input type="hidden" name="id" value="${customer.userId}">
											<a href="#${customer.name}" onclick="$(this).closest('form').submit()">${customer.name}</a>
										</form>
									</td>
									<td>${customer.company }</td>
									<td>${customer.telephone }</td>
									<td>${customer.email }</td>
									<td>${customer.login }</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="delete_user">
											<input type="hidden" name="id" value="${customer.userId }">
											<button type="submit" class="btn btn-danger">X</button>
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>


		</div>

	</div>

</body>




</html>