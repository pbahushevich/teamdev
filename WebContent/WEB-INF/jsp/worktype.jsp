<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>
<c:set var="page_addr" value="/controller?command=show_worktype" />
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="worktypeListLabel" key="worktypelist.worktypeListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="worktypelist.numberColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="worktypelist.workTypeColumn" />
<fmt:message bundle="${loc }" var="newWorkType" key="worktypelist.newWorkType" />
<fmt:message bundle="${loc }" var="addButton" key="worktypelist.addButton" />

<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>
<title>Work type list</title>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row">
			<div class="col-lg-5">
				<td:welcomeMessageTag text="${requestScope.userMessage}" error="${error}" />
			</div>
			<div class="col-lg-3">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<div class="row"></div>
		<div class="clearfix visible-md-block"></div>
		<div class="row" style="height: 10px"></div>
		<div class="row">
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">${worktypeListLabel }</div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th style="width: 5%">${numberColumn}</th>
								<th style="width: 90%">${workTypeColumn}</th>
								<th style="width: 5%"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${worktypeList }" var="workType" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>${workType.title }</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="delete_worktype">
											<input type="hidden" name="id" value="${workType.id }">
											<button type="submit" class="btn btn-danger">X</button>
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<form action="${pageContext.request.contextPath}/controller" name="addWorkType" method="POST">
				<input type="hidden" name="page" value="worktype_list">
				<input type="hidden" name="command" value="save_worktype">
				<div class="row">
					<div class="col-lg-8">
						<div class="input-group" style="margin: 0px 5px 0px 15px">
							<input type="text" name="title" class="form-control" placeholder="${newWorkType}" required>
							<span class="input-group-btn">
								<button type="submit" class="btn btn-success">${addButton}</button>
							</span>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>