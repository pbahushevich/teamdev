<div>
	<c:if test="${user !=null}">
	<div style="display: table-cell; float: right;">
		<form name="homePage" method="POST" action="${pageContext.request.contextPath}/controller">
			<input type="hidden" name="command" value="show_welcome_page" />
			<button type="submit"  class="btn btn-warning"  style="height: 34px"> <span class="glyphicon glyphicon-home"></span></button>
		</form>
	</div>
	<div style="display: table-cell; float: right;">
		<form name="logOut" method="POST" action="${pageContext.request.contextPath}/controller">
			<input type="hidden" name="command" value="log_out" />
			<button type="submit"  class="btn btn-warning" style="height: 34px"> <span class="glyphicon glyphicon-cloud-download"></span></button>
		</form>
	</div>
	</c:if>
	<div style="display: table-cell; float: right;">
		<form name="set_locale_en" method="POST" action="${pageContext.request.contextPath}/controller">
			<input type="hidden" name="command" value="set_locale" />
			<input type="hidden" name="page" value="${page_addr}" />
			<input type="hidden" name="request_line" value="${request_line}" />
			<input type="hidden" name="locale" value="en" />
			<input type="submit" value="EN" class="btn btn-primary" />
		</form>
	</div>
	<div style="display: table-cell; float: right;">
		<form name="set_locale_ru" method="POST" action="${pageContext.request.contextPath}/controller">
			<input type="hidden" name="command" value="set_locale" />
			<input type="hidden" name="page" value="${page_addr}" />
			<input type="hidden" name="request_line" value="${request_line}" />
			<input type="hidden" name="locale" value="ru" />
			<input type="submit" value=RU class="btn btn-primary">
		</form>
	</div>
</div>
