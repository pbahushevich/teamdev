<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:set var="page_addr" value="/controller?command=show_welcome_page" />
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="workListLabel" key="developer.workListLabel" />
<fmt:message bundle="${loc }" var="projectColumn" key="developer.projectColumn" />
<fmt:message bundle="${loc }" var="managerColumn" key="developer.managerColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="developer.workTypeColumn" />
<fmt:message bundle="${loc }" var="dateStartColumn" key="developer.dateStartColumn" />
<fmt:message bundle="${loc }" var="dateEndColumn" key="developer.dateEndColumn" />
<fmt:message bundle="${loc }" var="numberColumn" key="developer.numberColumn" />
<fmt:message bundle="${loc }" var="spentHours" key="developer.spentHours" />
<fmt:message bundle="${loc }" var="addHoursButton" key="developer.addHoursButton" />
<fmt:message bundle="${loc }" var="saveButton" key="developer.saveButton" />


<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/token.js"></script>
<title>Teamdev for developer</title>

</head>
<body>

	<div class="container" style="margin: 30px">

		<div class="row">
			<div class="col-lg-4">
<!-- 
				<div>
					<a href="/teamdev/controller?command=navigation&page=worktype_list_page" class="btn btn-success"> <span class="glyphicon glyphicon-barcode"></span> ${addHoursButton}
					</a>
				</div>
 -->
			</div>
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
				<%@include file="localebuttons.jsp"%>
			</div>
		</div>

		<div class="clearfix visible-md-block"></div>
		<div class="row" style="height: 10px"></div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">${workListLabel }</div>
					<table class="table">
						<thead>
							<tr>
								<th>${numberColumn}</th>
								<th>${projectColumn}</th>
								<th>${managerColumn}</th>
								<th>${workTypeColumn}</th>
								<th>${dateStartColumn}</th>
								<th>${dateEndColumn }</th>
								<th>${spentHours}</th>
								<th></th>
							</tr>
						</thead>

						<tbody>

							<c:forEach items="${workList}" var="projectWork" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_project">
											<input type="hidden" name="id" value="${projectWork.project.id}">
											<a href="#${projectTaskVO.project.title}" onclick="$(this).closest('form').submit()">${projectWork.project.title}</a>
										</form>
									</td>
									<td>${projectWork.project.manager.name}</td>
									<td>${projectWork.taskWork.workType.title}</td>
									<td>
										<fmt:formatDate value="${projectWork.dateStart}" pattern="dd.MM.YYYY" />
									</td>
									<td>
										<fmt:formatDate value="${projectWork.dateEnd}" pattern="dd.MM.YYYY" />
									</td>
									<td>${projectWork.timeSpent}</td>
									<td>
										<div id="timeAdd_${projectWork.id}">
											<button tabindex="-1" class="btn btn-success" type="button" style="height: 33px" value="Add" onclick="insertSpentTime(${projectWork.id})">
												<span class="glyphicon glyphicon-plus"> </span>
											</button>
										</div>

										<div id="timeSave_${projectWork.id}" style="display: none;">
											<form method="post" action="${pageContext.request.contextPath}/controller">
												<input type="hidden" name="command" value="add_time_spent">
												<input type="hidden" name="workId" value="${projectWork.id}">
												<div style="float: left; width: 50px; margin-right: 3px">
													<input type="text" name="timeSpent" id="timeSpent_${projectWork.id}" class="form-control" required pattern="[\d]+(\.[\d]+)?" title="0.0-999">
												</div>
												<div style="display: table-cell;">
													<button class="btn btn-success" type="submit" value="Add">
														<span class="glyphicon glyphicon-floppy-save"> </span> ${saveButton}
													</button>
												</div>
											</form>
										</div>
									</td>
								</tr>
								<input type="hidden" id="rowCount" name="rowCount" value="${workList.size()}">
								<c:set var="totalTime" value="${totalTime+ projectWork.timeSpent}" />
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">

function insertSpentTime(row_number){
	var addEl = document.getElementById("timeAdd_"+row_number);
	addEl.style.display = "none";
//	alert(addEl.style.display);
	var saveEl = document.getElementById("timeSave_"+row_number);
	saveEl.style.display  = "";
}
</script>

</html>