<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:set var="page_addr" value="/controller?command=show_welcome_page" />
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="techTaskLabel" key="customer.techTaskLabel" />
<fmt:message bundle="${loc }" var="techTaskColumn" key="customer.techTaskColumn" />
<fmt:message bundle="${loc }" var="dateStartEndColumn" key="customer.dateStartEndColumn" />
<fmt:message bundle="${loc }" var="invoiceColumn" key="customer.invoiceColumn" />
<fmt:message bundle="${loc }" var="managerColumn" key="customer.managerColumn" />
<fmt:message bundle="${loc }" var="invoicesTitle" key="customer.invoicesTitle" />
<fmt:message bundle="${loc }" var="costColumn" key="customer.costColumn" />
<fmt:message bundle="${loc }" var="newTechTaskButton" key="customer.newTechTaskButton" />
<fmt:message bundle="${loc }" var="numberColumn" key="customer.numberColumn" />


<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<title>Teamdev for customer</title>
</head>
<body>

	<div class="container" style="margin: 30px">


		<td:welcomeMessageTag text="${requestScope.userMessage}" error="${error }" />
		<div class="row">
			<div class="col-lg-4">
				<div>
					<form name="newTechTask" method="POST" action="${pageContext.request.contextPath}/controller">
						<input type="hidden" name="command" value="new_techtask" required/>
						<button type="submit" class="btn btn-success" style="height: 34px">
							<span class="glyphicon glyphicon-list-alt"></span> ${newTechTaskButton}
						</button>
					</form>
				</div>
			</div>
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
				<%@include file="localebuttons.jsp"%>
			</div>
		</div>
		<div class="clearfix visible-md-block"></div>
		<div class="row" style="height: 10px"></div>
		<div class="row">
			<div class="col-lg-9">
				<div class="panel panel-default">
					<div class="panel-heading">${techTaskLabel }</div>
					<table class="table">
						<thead>
							<tr>
								<th>${numberColumn}</th>
								<th>${techTaskColumn}</th>
								<th>${managerColumn}</th>
								<th>${dateStartEndColumn}</th>
								<th>${invoiceColumn}</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${projectTaskList}" var="projectTaskVO" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
										<form action="${pageContext.request.contextPath}/controller" method="POST">
											<input type="hidden" name="command" value="show_techtask">
											<input type="hidden" name="id" value="${projectTaskVO.techTask.id}">
											<a href="#${projectTaskVO.techTask.title}" onclick="$(this).closest('form').submit()">${projectTaskVO.techTask.title}</a>
										</form>
									<td>
										<form action="${pageContext.request.contextPath}/controller" method="POST">
											<input type="hidden" name="command" value="show_user">
											<input type="hidden" name="id" value="${projectTaskVO.manager.userId}">
											<a href="#${projectTaskVO.manager.name}" onclick="$(this).closest('form').submit()">${projectTaskVO.manager.name}</a>
										</form>
									</td>
									<td>
										<c:if test="${!empty projectTaskVO.dateStart}">
											<fmt:formatDate value="${projectTaskVO.dateStart}" pattern="dd.MM.YYYY" /> - 
									<fmt:formatDate value="${projectTaskVO.dateEnd}" pattern="dd.MM.YYYY" />
										</c:if>
									</td>
									<td>
										<td:invoiceRef invoice="${projectTaskVO.invoice}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-lg-3" style="padding-left: 5px">
				<div class="panel panel-default">
					<div class="panel-heading">${invoicesTitle }</div>
					<table class="table">
						<thead>
							<tr>
								<th>${numberColumn}</th>
								<th>${invoiceColumn}</th>
								<th>${costColumn}</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${invoiceList}" var="invoice" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
										<td:invoiceRef invoice="${invoice}" />
									</td>
									<td>${invoice.price}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>

</html>