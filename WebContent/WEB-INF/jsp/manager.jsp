<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:set var="page_addr" value="/controller?command=show_welcome_page" />
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="workTypes" key="manager.workTypes" />
<fmt:message bundle="${loc }" var="customerList" key="manager.customerList" />
<fmt:message bundle="${loc }" var="developerList" key="manager.developerList" />
<fmt:message bundle="${loc }" var="newCustomer" key="manager.newCustomer" />
<fmt:message bundle="${loc }" var="projects_label" key="manager.projectsLabel" />
<fmt:message bundle="${loc }" var="techTaskLabel" key="manager.techTaskLabel" />
<fmt:message bundle="${loc }" var="techTaskColumn" key="manager.techTaskColumn" />
<fmt:message bundle="${loc }" var="projectColumn" key="manager.projectColumn" />
<fmt:message bundle="${loc }" var="dateStartColumn" key="manager.dateStartColumn" />
<fmt:message bundle="${loc }" var="dateEndColumn" key="manager.dateEndColumn" />
<fmt:message bundle="${loc }" var="invoiceColumn" key="manager.invoiceColumn" />
<fmt:message bundle="${loc }" var="costColumn" key="manager.costColumn" />
<fmt:message bundle="${loc }" var="numberColumn" key="manager.numberColumn" />
<fmt:message bundle="${loc }" var="customerColumn" key="manager.customerColumn" />
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<title>Teamdev for manager</title>

</head>
<body>

	<div class="container" style="margin: 30px">

		<div class="row">
			<div class="col-lg-8">
				<td:welcomeMessageTag text="${requestScope.userMessage}" error="${error}" />
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8">
				<div>
				<div style="display: table-cell;">
					<form method="post" action="${pageContext.request.contextPath}/controller">
						<input type="hidden" name="command" value="show_customer_list">
						<a href="#${customerList}" onclick="$(this).closest('form').submit()" class="btn btn-primary"> <span class="glyphicon glyphicon-briefcase"></span> ${customerList}
						</a>
					</form>
				</div>
				<div style="display: table-cell;">
					<form method="post" action="${pageContext.request.contextPath}/controller">
						<input type="hidden" name="command" value="show_devlist">
						<a href="#${developerList}" onclick="$(this).closest('form').submit()" class="btn btn-primary"> <span class="glyphicon glyphicon-user"></span> ${developerList}
						</a>
					</form>
				</div>
				<div style="display: table-cell;">
					<form method="post" action="${pageContext.request.contextPath}/controller">
						<input type="hidden" name="command" value="show_worktype">
						<a href="#${workTypes}" onclick="$(this).closest('form').submit()" class="btn btn-primary"> <span class="glyphicon glyphicon-th-list"></span> ${workTypes}
						</a>
					</form>
				</div>
				</div>
			</div>
			<div class="col-lg-4">

				<%@include file="localebuttons.jsp"%>
			</div>
		</div>
		<div class="clearfix visible-md-block"></div>
		<div class="row" style="height: 10px"></div>
		<div class="row">
			<div class="col-lg-9">
				<div class="panel panel-default">
					<div class="panel-heading">${projects_label }</div>
					<table class="table">
						<thead>
							<tr>
								<th>${numberColumn}</th>
								<th>${customerColumn}</th>
								<th>${projectColumn}</th>
								<th>${dateStartColumn }</th>
								<th>${dateEndColumn}</th>
								<th>${invoiceColumn}</th>
								<th>${costColumn}</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${projectTaskList}" var="projectTaskVO" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_user">
											<input type="hidden" name="id" value="${projectTaskVO.customer.userId}">
											<a href="#${projectTaskVO.customer.name}" onclick="$(this).closest('form').submit()">${projectTaskVO.customer.name}</a>
										</form>
									</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_project">
											<input type="hidden" name="id" value="${projectTaskVO.project.id}">
											<a href="#${projectTaskVO.project.title}" onclick="$(this).closest('form').submit()">${projectTaskVO.project.title}</a>
										</form>
									</td>
									<td>
										<fmt:formatDate value="${projectTaskVO.dateStart}" pattern="dd.MM.YYYY" />
									</td>
									<td>
										<fmt:formatDate value="${projectTaskVO.dateEnd}" pattern="dd.MM.YYYY" />
									</td>
									<td>
									<td:invoiceRef invoice="${projectTaskVO.invoice}"/>
									</td>
									<td>${projectTaskVO.invoice.price}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-lg-3">
				<div class="panel panel-default">
					<div class="panel-heading">${techTaskLabel }</div>
					<table class="table">
						<thead>
							<tr>
								<th>${numberColumn}</th>
								<th>${customerColumn}</th>
								<th>${techTaskColumn}</th>
							</tr>
						</thead>
						<tbody>

							<c:forEach items="${techTaskList }" var="techTask" varStatus="loop">
								<tr>
									<td>${loop.count}</td>
									<td>
									<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_user">
											<input type="hidden" name="id" value="${techTask.customer.userId}">
											<a href="#${techTask.customer.name}" onclick="$(this).closest('form').submit()">${techTask.customer.name}</a>
										</form>
									
								</td>
									<td>
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="show_techtask">
											<input type="hidden" name="id" value="${techTask.id}">
											<a href="#${techTask.title}" onclick="$(this).closest('form').submit()">${techTask.title}</a>
										</form>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>
			</div>


		</div>
	</div>

</body>




</html>