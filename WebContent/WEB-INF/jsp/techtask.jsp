<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>

<%@page import="com.epam.teamdev.bean.Role"%>

<html>
<head>
<fmt:formatDate value="${techTask.date }" pattern="dd.MM.YYYY" var="techTaskDate" />
<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="pageTitle" key="techtask.pageTitle" />
<fmt:message bundle="${loc }" var="techtaskTitle" key="techtask.techtaskTitle" />
<fmt:message bundle="${loc }" var="date" key="techtask.date" />
<fmt:message bundle="${loc }" var="workListLabel" key="techtask.workListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="techtask.numberColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="techtask.workTypeColumn" />
<fmt:message bundle="${loc }" var="qualificationColumn" key="techtask.qualificationColumn" />
<fmt:message bundle="${loc }" var="devcountColumn" key="techtask.devcountColumn" />
<fmt:message bundle="${loc }" var="saveButton" key="techtask.saveButton" />
<fmt:message bundle="${loc }" var="resetButton" key="techtask.resetButton" />
<fmt:message bundle="${loc }" var="deleteButton" key="techtask.deleteButton" />
<fmt:message bundle="${loc }" var="editButton" key="techtask.editButton" />
<fmt:message bundle="${loc }" var="createProjectButton" key="techtask.createProjectButton" />
<fmt:message bundle="${loc }" var="projectExistTitle" key="techtask.projectExistTitle" />
<fmt:message bundle="${loc }" var="dateLocale" key="local.dateLocale" />
<c:set var="page_addr" value="/controller?command=show_techtask&id=${techTask.id}" />
<c:if test="${!readonly}">
	<c:set var="ro_string" value="readonly" />
	<c:set var="page_addr" value="/controller?command=edit_techtask&id=${techTask.id}" />
</c:if>
<c:if test="${newTask}">
	<c:set var="page_addr" value="/controller?command=new_techtask" />
</c:if>

<c:set var="techtaskDate" value="techtaskDate" />
<c:set var="editDate" value="false" />
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<!-- 

 -->

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/daterangepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>


<title>Techtask</title>
<style type="text/css">
</style>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row" style="margin-bottom: 5px">

			<div class="col-lg-9">
				<td:welcomeMessageTag text="${requestScope.userMessage}" error="${error }" />
			</div>
		</div>
		<div class="row" style="margin-bottom: 5px">

			<div class="col-lg-9" style="padding-left: 20px">
				<c:if test="$!{newtask}">
					<form name="taskCustomer" method="POST" action="${pageContext.request.contextPath}/controller">
						<input type="hidden" name="command" value="show_user">
						<input type="hidden" name="id" value="${techTask.customer.userId}">
						<a href="#${techTask.customer.company} - ${techTask.customer.name}" onclick="$(this).closest('form').submit()">${techTask.customer.company} - ${techTask.customer.name}</a>
					</form>
				</c:if>
			</div>

			<div class="col-lg-3">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>

		<form name="techtask" action="${pageContext.request.contextPath}/controller" method="POST">
			<c:choose>
				<c:when test="${newTask}">
					<input type="hidden" name="command" value="save_techtask" />
				</c:when>
				<c:when test="${!readonly}">
					<input type="hidden" name="command" value="update_techtask" />
					<input type="hidden" name="id" value="${techTask.id}" />
				</c:when>
			</c:choose>
			<div class="row">
				<div class="col-lg-9">
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-italic"></span></span>
						<input type="text" name="title" class="form-control" placeholder="${techtaskTitle}" value="${techTask.title}" ${readonly ? "readonly" :""} required>
					</div>
				</div>

				<div class="col-lg-3">
					<div>
						<div class="input-group">
							<td:calendar id="datepicker" name="date" type="single" locale="${sessionScope.locale}" readonly="${readonly}" value="${techTaskDate}" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix visible-md-block"></div>
			<div class="row" style="height: 10px"></div>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">${workListLabel }</div>
						<table id="workList" class="table table-striped">
							<thead>
								<tr>
									<c:if test="${readonly}">
										<th style="width: 5%">${numberColumn}</th>
									</c:if>
									<th style="width: 50%">${workTypeColumn}</th>
									<th>${qualificationColumn}</th>
									<th>${devcountColumn}</th>
									<c:if test="${!readonly}">
										<th></th>
									</c:if>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${workList}" var="work" varStatus="loop">
									<tr>
										<c:if test="${readonly}">
											<td>${loop.count}</td>
										</c:if>
										<td>${work.workType.title}<input type=hidden name="workType" value="${work.workType.id}" />
										</td>
										<td>${work.qualification.title}<input type=hidden name="qualification" value="${work.qualification.id}" />
										</td>
										<td>${work.devCount}<input type=hidden name="devCount" value="${work.devCount}" />
										</td>
										<c:if test="${!readonly}">
											<td>
												<button tabindex="-1" class="btn btn-danger" type="button" style="height: 33px" value="Add" onclick="deleteRow(this)">
													<span class="glyphicon glyphicon-remove"> </span>
												</button>
											</td>
										</c:if>
									</tr>
								</c:forEach>

								<c:if test="${!readonly}">
									<tr>
										<td>
											<select id="newWorkType" data-width=100% class="selectpicker" class="form-control">
												<c:forEach items="${worktypeList}" var="worktypeEl">
													<option value="${worktypeEl.id }">${worktypeEl.title}</option>
												</c:forEach>
											</select>
										</td>
										<td>
											<select id="newQualification" class="selectpicker" class="form-control">
												<c:forEach items="${qualificationList}" var="qualificationEl">
													<option value="${qualificationEl.id }">${qualificationEl.title}</option>
												</c:forEach>
											</select>
										</td>
										<td>
											<div class="form-group">
												<input id="newDevCount" type="text" class="form-control" placeholder="${devcountColumn}">
											</div>
										</td>
										<td>
											<div>
												<button tabindex="-1" class="btn btn-success" type="button" style="height: 33px" value="Add" onclick="addTableRow()">
													<span class="glyphicon glyphicon-plus"> </span>
												</button>
												<input type="hidden" id="rowCount" name="rowCount" value="${workList.size()}">

											</div>

										</td>

									</tr>

								</c:if>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<c:if test="${!readonly}">
				<div class="row">
					<div class="col-lg-1  col-lg-offset-11" style="float: left">
						<div style="display: table-cell; float: right;">
							<input type="submit" class="btn btn-success" value="${saveButton}">
						</div>
					</div>
				</div>
			</c:if>
		</form>
		<div class="row">
			<c:choose>
				<c:when test="${project != null}">
					<div class="col-lg-5  col-lg-offset-7" style="float: left">
						<div style="display: table-cell; float: right;">
							<form method="POST" action="${pageContext.request.contextPath}/controller">
								<input type="hidden" name="command" value="show_project" />
								<input type="hidden" name="id" value="${project.id}" />
								${projectExistTitle} <a href="#${project.title}" onclick="$(this).closest('form').submit()">${project.title} </a>
							</form>
						</div>
					</div>
				</c:when>
				<c:when test="${readonly}">
					<div class="col-lg-3  col-lg-offset-9" style="float: right">
						<div style="display: table-cell; float: right;">
							<form method="post" action="${pageContext.request.contextPath}/controller">
								<input type="hidden" name="command" value="delete_techtask" />
								<input type="hidden" name="id" value="${techTask.id}" />
								<button type="submit" class="btn btn-danger">${deleteButton}</button>
							</form>
						</div>
						<c:if test="${user.role == Role.CUSTOMER }">
							<div style="display: table-cell; float: right;">
								<form method="post" action="${pageContext.request.contextPath}/controller">
									<input type="hidden" name="command" value="edit_techtask" />
									<input type="hidden" name="id" value="${techTask.id}" />
									<button type="submit" class="btn btn-primary">${editButton}</button>
								</form>
							</div>
						</c:if>
						<c:if test="${user.role == Role.MANAGER}">
							<div style="display: table-cell; float: right;">
								<form method="post" action="${pageContext.request.contextPath}/controller">
									<input type="hidden" name="command" value="new_project" />
									<input type="hidden" name="id" value="${techTask.id}" />
									<button type="submit" class="btn btn-primary">${createProjectButton}</button>
								</form>
							</div>
						</c:if>
					</div>
				</c:when>
			</c:choose>
		</div>
	</div>
	<script>
		function deleteRow(r) {
			var i = r.parentNode.parentNode.rowIndex;
			document.getElementById("workList").deleteRow(i);

			var table = document.getElementById("workList");
			document.getElementById("rowCount").value = table.rows.length - 2;
		}
	</script>
	<script>
		function addTableRow() {
		
			var devCountValue = document.getElementById("newDevCount").value;
			devCountValue= devCountValue.replace(/\D*/g, "");
			if (devCountValue ==""){
				return;
			}

			var table = document.getElementById("workList");
			var row = table.insertRow(table.rows.length - 1);
			var cellWorkType = row.insertCell(0);
			var cellQualification = row.insertCell(1);
			var cellDevCount = row.insertCell(2);
			var cellDelete = row.insertCell(3);

			var qualifiactionList = document.getElementById("newQualification");
			var qualificationValue = qualifiactionList.value;
			var qualificationText = qualifiactionList.options[qualifiactionList.selectedIndex].text;

			var workTypeList = document.getElementById("newWorkType");
			var workTypeValue = workTypeList.value;
			var workTypeText = workTypeList.options[workTypeList.selectedIndex].text;
			cellWorkType.innerHTML = "<input type =\"hidden\" name=\"workType"+"\" value=\""+workTypeValue+"\"/>"
					+ workTypeText;
			cellQualification.innerHTML = "<input type =\"hidden\" name=\"qualification"+"\" value=\""+qualificationValue+"\"/>"
					+ qualificationText;
			cellDevCount.innerHTML = "<input type =\"hidden\" name=\"devCount"+"\" value=\""+devCountValue+"\"/>"
					+ devCountValue;

			cellDelete.innerHTML = "<button tabindex=\"-1\" class=\"btn btn-danger\" type=\"button\" style=\"height: 33px\" value=\"Add\" onclick=\"deleteRow(this)\">"
					+ "<span class=\"glyphicon glyphicon-remove\"> </span>"
					+ "</button>";
			document.getElementById("rowCount").value = table.rows.length - 2;

		}
	</script>
</body>




</html>