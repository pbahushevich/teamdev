<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>
<%@ page import="com.epam.teamdev.bean.Role"%>
<html>
<head>

<fmt:setLocale value="${sessionScope.locale}" />
<c:if test="${empty sessionScope.locale}">
	<fmt:setLocale value="en" />
</c:if>
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc }" var="pageTitle" key="invoice.pageTitle" />
<fmt:message bundle="${loc }" var="number" key="invoice.number" />
<fmt:message bundle="${loc }" var="date" key="invoice.date" />
<fmt:message bundle="${loc }" var="price" key="invoice.price" />
<fmt:message bundle="${loc }" var="workListLabel" key="invoice.workListLabel" />
<fmt:message bundle="${loc }" var="numberColumn" key="invoice.numberColumn" />
<fmt:message bundle="${loc }" var="workTypeColumn" key="invoice.workTypeColumn" />
<fmt:message bundle="${loc }" var="qualificationColumn" key="invoice.qualificationColumn" />
<fmt:message bundle="${loc }" var="dateColumn" key="invoice.dateColumn" />
<fmt:message bundle="${loc }" var="developerColumn" key="invoice.developerColumn" />
<fmt:message bundle="${loc }" var="saveButton" key="invoice.saveButton" />
<fmt:message bundle="${loc }" var="resetButton" key="invoice.resetButton" />
<fmt:message bundle="${loc }" var="deleteButton" key="invoice.deleteButton" />
<fmt:message bundle="${loc }" var="editButton" key="invoice.editButton" />
<fmt:message bundle="${loc }" var="projectStartedTitle" key="invoice.projectStartedTitle" />
<fmt:message bundle="${loc }" var="techTaskTitle" key="invoice.techTaskTitle" />
<fmt:message bundle="${loc }" var="timeSpentColumn" key="invoice.timeSpentColumn" />
<fmt:message bundle="${loc }" var="createInvoiceButton" key="invoice.createInvoiceButton" />
<fmt:message bundle="${loc }" var="setPaidButton" key="invoice.setPaidButton" />
<fmt:message bundle="${loc }" var="setUnPaidButton" key="invoice.setUnPaidButton" />
<fmt:message bundle="${loc }" var="dateLocale" key="local.dateLocale" />

<c:choose>
	<c:when test="${newInvoice}">
		<c:set var="page_addr" value="/controller?command=new_invoice&id=${project.id }" />
	</c:when>
	<c:when test="${readonly}">
		<c:set var="page_addr" value="/controller?command=show_invoice&id=${invoice.id }" />
	</c:when>
	<c:when test="${!readonly}">
		<c:set var="page_addr" value="/controller?command=edit_invoice&id=${invoice.id }" />
	</c:when>
</c:choose>
<c:if test="${readonly}">
	<c:set var="ro_string" value="readonly" />
</c:if>
<fmt:formatDate value="${invoice.date}" var="invoiceDate" pattern="dd.MM.YYYY" />
<c:set var="editDate" value="false" />
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/daterangepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap-select.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/token.js"></script>


<title>${pageTitle }</title>
<style type="text/css">
</style>
</head>
<body>
	<div class="container" style="margin: 30px">
		<div class="row" style="margin-bottom: 5px">
			<div class=" col-lg-5" style="height: 33px; padding: 6px; padding-left: 15px">
				<form method="post" action="${pageContext.request.contextPath}/controller">
					<input type="hidden" name="command" value="show_project">
					<input type="hidden" name="id" value="${project.id}">
					<a href="#${project.title}" onclick="$(this).closest('form').submit()">Project: ${project.title}</a>
				</form>
			</div>
			<div class="col-lg-7">
				<div>
					<%@ include file="localebuttons.jsp"%>
				</div>
			</div>
		</div>
		<div>
			<form id="save_invoice" name="save_invoice" action="${pageContext.request.contextPath}/controller">

				<div class="row">
					<c:choose>
						<c:when test="${newInvoice }">
							<input type="hidden" name="command" value="save_invoice" />
						</c:when>
						<c:otherwise>
							<input type="hidden" name="command" value="update_invoice" />
							<input type="hidden" name="id" value="${invoice.id }" />
						</c:otherwise>
					</c:choose>
					<input type="hidden" name="projectId" value="${project.id}" />
					<div class="col-lg-3">
						<div class="input-group">
							<span class="input-group-addon"><span>№</span></span>
							<input type="text" name="number" class="form-control" placeholder="${number}" value="${invoice.number}" ${readonly ? "readonly" :""} required>

						</div>
					</div>
					<div class="col-lg-2">
						<div>
							<div class="input-group">
								<td:calendar id="date" name="date" type="single" locale="${sessionScope.locale}" readonly="${readonly}" value="${invoiceDate}" />
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div>
							<div class="input-group">
								<input type="text" name="price" class="form-control" placeholder="${price}" value="${invoice.price}" ${readonly ? "readonly" :""} required pattern="[\d]+(\.[\d]+)?" title="0.0-999">
								<span class="input-group-addon"><span class="glyphicon glyphicon-usd"></span></span>
							</div>
						</div>
					</div>

				</div>
				<div class="clearfix visible-md-block"></div>
				<div class="row" style="height: 5px"></div>


				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">${workListLabel }</div>
							<table id="workList" class="table table-striped">
								<thead>
									<tr>
										<th style="width: 5%">${numberColumn}</th>
										<th>${workTypeColumn}</th>
										<th>${qualificationColumn}</th>
										<th>${dateColumn}</th>
										<th>${developerColumn}</th>
										<th>${timeSpentColumn}</th>
									</tr>
								</thead>
								<tbody>

									<c:forEach items="${workList}" var="projectWork" varStatus="loop">
										<tr>
											<td>${loop.count}</td>
											<td>${projectWork.taskWork.workType.title}</td>
											<td>${projectWork.taskWork.qualification.title}</td>

											<td>
												<fmt:formatDate value="${projectWork.dateStart}" pattern="dd.MM.YYYY" />
												-
												<fmt:formatDate value="${projectWork.dateEnd}" pattern="dd.MM.YYYY" />
											</td>
											<td>${projectWork.developer.name}</td>
											<td>${projectWork.timeSpent}</td>
										</tr>
										<c:set var="totalTime" value="${totalTime+ projectWork.timeSpent}" />
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<c:if test="${!readonly}">
					<div class="col-lg-6" style="float: right">
						<div style="float: right;">
							<button type="submit" class="btn btn-success">${saveButton}</button>
						</div>
					</div>
				</c:if>
			</form>
			<div class="row">
				<div class="col-lg-6" style="float: left"></div>
				<c:if test="${user.role == Role.MANAGER}">
					<c:choose>
						<c:when test="${readonly}">
							<c:if test="${!invoice.paid}">
								<div class="col-lg-6" style="float: left">
									<div style="display: table-cell; float: right;">
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="delete_invoice" />
											<input type="hidden" name="id" value="${invoice.id}" />
											<button type="submit" class="btn btn-danger">${deleteButton}</button>
										</form>
									</div>
									<div style="display: table-cell; float: right;">
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="edit_invoice" />
											<input type="hidden" name="id" value="${invoice.id}" />
											<button type="submit" class="btn btn-primary">${editButton}</button>
										</form>
									</div>
								</div>
							</c:if>
							<c:if test="${invoice.paid}">
								<div class="col-lg-6" style="float: left">
									<div style="display: table-cell; float: right;">
										<form method="post" action="${pageContext.request.contextPath}/controller">
											<input type="hidden" name="command" value="set_paid_flag" />
											<input type="hidden" name="paid" value="false" />
											<input type="hidden" name="id" value="${invoice.id}" />
											<button type="submit" class="btn btn-danger">${setUnPaidButton}</button>
										</form>
									</div>
								</div>
							</c:if>
						</c:when>
					</c:choose>
				</c:if>
			</div>
		</div>
	<c:if test="${user.role == Role.CUSTOMER && invoice.paid == false}">
		<div class="col-lg-12" style="float: left">
			<div style="display: table-cell; float: right;">
				<form method="post" action="${pageContext.request.contextPath}/controller">
					<input type="hidden" name="command" value="set_paid_flag" />
					<input type="hidden" name="paid" value="true" />
					<input type="hidden" name="id" value="${invoice.id}" />
					<button type="submit" class="btn btn-success">${setPaidButton}</button>
				</form>
			</div>
		</div>
	</c:if>
	</div>


</body>
</html>