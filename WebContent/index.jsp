<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="td" uri="/WEB-INF/tld/teamdev.tld"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<c:set var="page_addr" value="index.jsp" />
<fmt:setLocale value="${sessionScope.locale}" />
<fmt:setBundle basename="com.epam.teamdev.configuration.localization.local" var="loc" />
<fmt:message bundle="${loc}" key="index.welcomeMessage" var="welcomeMessage" />
<fmt:message bundle="${loc}" key="index.invalidLoginMessage" var="invalidLoginMessage" />
<fmt:message bundle="${loc}" key="index.loginButton" var="loginButton" />
<fmt:message bundle="${loc}" key="index.userTitle" var="userTitle" />
<fmt:message bundle="${loc}" key="index.passwordTitle" var="passwordTitle" />

<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style type="text/css">
h2 {
	margin: 0 px;
	padding: 0 10px 1px 0;
	border-bottom: 1px solid #E5E5E5;
}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Developer team</title>
</head>
<body>
	<div>
		<div class="container" style="margin: 40px">
			<div class="row">
				<div class="col-lg-6">
					<h2>${welcomeMessage}</h2>
				</div>
				<div class="col-lg-4">
					<div>
						<%@ include file="WEB-INF/jsp/localebuttons.jsp"%>
					</div>
				</div>
			</div>
			<div class="clearfix visible-md-block"></div>
			<div class="row">
				<div class="col-lg-10">
					<td:welcomeMessageTag error="${error}" text="${userMessage}" />
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<form method="POST" action="controller">
						<input type="hidden" name="page" value="login">
						<input type="hidden" name="command" value="login_user">

						<div class="form-group">
							<label for="inputEmail">${userTitle}</label>
							<input type="user" name="login" class="form-control" id="inputEmail" placeholder="${userTitle}">
						</div>
						<div class="form-group">
							<label for="inputPassword">${passwordTitle}</label>
							<input type="password" name="password" class="form-control" id="inputPassword" placeholder="${passwordTitle}">
						</div>

						<button type="submit" class="btn btn-primary">${loginButton}</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

