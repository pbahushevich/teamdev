window.addEventListener('load',function() {
	for ( var frm in document.forms) {
		addHidden(document.forms[frm]);
	}
},false);

	function addHidden(theForm) {
		
		if(typeof theForm != "object"){
			return;
		}
		var input = document.createElement('input');
		input.type = 'hidden';
		input.name = "token";
		input.value = Math.random();
		theForm.insertBefore(input,theForm.firstChild);
	}